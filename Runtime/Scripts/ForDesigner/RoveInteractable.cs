using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Rove.RoveExporter
{
    [DisallowMultipleComponent]
    public class RoveInteractable : RoveMonoBehaviour, ILocalOnlyChecker
    {
        private IRoveTriggerEventInvoker[] _invokers;

        [SerializeField] private bool _isLocalOnly;
        [SerializeField] private RoveInteractType _interactType;
        [SerializeField] private KeyCode _keyCode;
        [SerializeField] private UnityEvent _onInteractedEvent;

        public RoveInteractType InteractType => _interactType;
        public KeyCode KeyCode => _keyCode;

        public bool IsLocalOnly => _isLocalOnly;

        protected void Awake()
        {
            updateSetterList();
        }

        public void invokeInteractedEvent()
        {
            _onInteractedEvent?.Invoke();
            invokeSetters(RoveTriggerEventType.OnInteracted);
        }
        private void invokeSetters(RoveTriggerEventType eventType)
        {
            foreach (var invoker in _invokers)
            {
                if (invoker != null)
                {
                    if (invoker.EventType == eventType)
                    {
                        invoker.invoke(eventType);
                    }
                }
            }
        }
        private void updateSetterList()
        {
            _invokers = GetComponents<IRoveTriggerEventInvoker>();
        }
    }

    public enum RoveInteractType
    {
        TOUCH,
        KEYBOARD
    }
}
