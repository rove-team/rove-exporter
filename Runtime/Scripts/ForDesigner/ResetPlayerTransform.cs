using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    /// <summary>
    /// Set transform of local player to destination if local player entered/left my trigger
    /// </summary>
    [RequireComponent(typeof(RoveTrigger))]
    public class ResetPlayerTransform : MonoBehaviour
    {
        [Flags]
        public enum ResetOption
        {
            POSITION = 1,
            ROTATION = 1 << 1,
            SCALE = 1 << 2,
        }
        public enum TriggerType
        {
            ENTER,
            EXIT,
        }
        public ResetOption resetOption = ResetOption.POSITION | ResetOption.ROTATION;
        public TriggerType triggerType;
        public Transform destination;
    }
}
