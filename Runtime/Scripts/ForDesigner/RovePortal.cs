using System;
using UnityEngine;

namespace Rove.RoveExporter
{
    [Obsolete("Will be removed later")]
    public class RovePortal : RoveMonoBehaviour
    {
        public bool isReturnPortal;
        public string rockAlias;
        public string textureThumbnailUrl;
        public Renderer environmentThumbnails;
    }
}
