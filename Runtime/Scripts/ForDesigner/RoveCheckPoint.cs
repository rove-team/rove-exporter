using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [DisallowMultipleComponent]
    public class RoveCheckPoint : MonoBehaviour, IRoveTriggerEventInvoker
    {
        public RoveTriggerEventType eventType;
        public int pointNumber;


        public RoveTriggerEventType EventType { get => eventType; set => eventType = value; }

        public event Action<RoveCheckPoint> onPointCheckedEvent;

        public void invoke(RoveTriggerEventType eventType)
        {
            if (eventType == EventType)
            {
                onPointCheckedEvent?.Invoke(this);
            }
        }
        public void clear()
        {
            onPointCheckedEvent = null;
        }
    }
}
