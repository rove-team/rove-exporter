using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Rove.RoveExporter
{
    public class EnvironmentSettings : MonoBehaviour
    {
        public RenderPipelineAsset usingRenderPipelineAsset;
        public RenderPipelineAsset qualityRenderPipelineAsset;
        private void OnValidate()
        {
            if (!usingRenderPipelineAsset && !qualityRenderPipelineAsset)
            {
                usingRenderPipelineAsset = GraphicsSettings.renderPipelineAsset;
                qualityRenderPipelineAsset = QualitySettings.renderPipeline;
            }
        }

        public void updateEnvironmentData()
        {
            usingRenderPipelineAsset = GraphicsSettings.renderPipelineAsset;
            qualityRenderPipelineAsset = QualitySettings.renderPipeline;
        }
    }
}
