using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class PlayerSpawnPoint : RoveMonoBehaviour, IVisibleInThumbnail
    {
        public enum SpawnTag
        {
            START,
            FALL_OR_KILLED,
            CHECK_POINT,
        }

        public SpawnTag[] spawnTags = new SpawnTag[1] { SpawnTag.START };
        public Vector3 positionOnGround;
        public bool canOnGround;
        protected void Awake()
        {
            if (Application.isPlaying)
            {
                gameObject.SetActive(false);
            }
        }
        public bool isCanBeOnGround()
        {
            var ray = new Ray(transform.position, Vector3.down);
            if (Physics.Raycast(ray, out var hitInfo, 1000, LayerMask.GetMask("Default")))
            {
                positionOnGround = hitInfo.point;
                canOnGround = true;
                return true;
            }
            else
            {
                positionOnGround = transform.position;
                canOnGround = false;
                return false;
            }
        }

        void OnDrawGizmos()
        {
            isCanBeOnGround();
        }
    }
}
