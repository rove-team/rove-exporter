using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Events;

namespace Rove.RoveExporter
{
    [DisallowMultipleComponent]
    public class RoveTrigger : RoveMonoBehaviour, ILocalOnlyChecker
    {
        [SerializeField] private bool _isLocalOnly;
        [SerializeField] protected RoveTriggerType _TriggerType;
        [SerializeField] protected RoveTriggerTarget _TriggerTarget = RoveTriggerTarget.ROVE_TRIGGERER;
        [SerializeField] protected RoveTriggererType _TriggererType = getAllTriggererType();

        [Tooltip("Number of objects enter RoveTrigger area to trigger enter event")]
        [SerializeField] protected int _NumberToTrigger = 1;
        [Tooltip("Minimum time that objects stay in RoveTrigger area to trigger enter event")]
        [SerializeField] protected float _minTimeToTrigger = 0.2f;

        [Tooltip("0 is unlimited")]
        [SerializeField] protected uint _LimitTriggerCount = 0;

        [SerializeField] protected LayerMask _triggerMask = ~(1 | 2 | 4 | 16 | 32);
        [SerializeField] protected List<GameObject> _Triggerers;

        [SerializeField] protected RoveTriggerEvent _TriggerEvents = new RoveTriggerEvent();


        [SerializeField] protected BoxCollider _BoxCollider;
        [SerializeField] protected SphereCollider _SphereCollider;

        [SerializeField] protected bool _IsEnableLog;
        [SerializeField] protected bool _IsEnableGizmos;

        private IRoveTriggerEventInvoker[] _invokers;
        private List<Collider> _ColliderStaying = new List<Collider>();

        public RoveTriggerEvent TriggerEvent => _TriggerEvents;

        protected bool _IsTriggerred;
        protected float _triggerEnterTime;

        public bool IsLocalOnly => _isLocalOnly;
        public MonoBehaviour getMonoBehaviour()
        {
            return this;
        }

        protected static RoveTriggererType getAllTriggererType()
        {
            return (RoveTriggererType)(-1);
        }

        protected void Awake()
        {
            updateSetterList();
#if UNITY_EDITOR
            Collider collider = GetComponent<SphereCollider>();
            if (collider)
            {
                _TriggerType = RoveTriggerType.OVERLAP_SPHERE;
                _SphereCollider = collider as SphereCollider;
            }
            else
            {
                collider = GetComponent<BoxCollider>();
                if (collider)
                {
                    _TriggerType = RoveTriggerType.OVERLAP_BOX;
                    _BoxCollider = collider as BoxCollider;
                }
            }
#endif
            if (_BoxCollider)
            {
                _BoxCollider.isTrigger = true;
            }
            if (_SphereCollider)
            {
                _SphereCollider.isTrigger = true;
            }
            gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
        }

        private void FixedUpdate()
        {
            if (_triggerMask.value == 0)
            {
                return;
            }
            if (_TriggerTarget == RoveTriggerTarget.GAMEOBJECTS)
            {
                _triggerMask = 0;
                foreach (var t in _Triggerers)
                {
                    if (t)
                    {
                        _triggerMask |= 1 << t.layer;
                    }
                }
            }
            var layerMask = _triggerMask;
            Collider[] colliders = null;
            switch (_TriggerType)
            {
                case RoveTriggerType.OVERLAP_BOX:
                    if (_BoxCollider)
                    {
                        var size = _BoxCollider.size;
                        size.x *= _BoxCollider.transform.lossyScale.x;
                        size.y *= _BoxCollider.transform.lossyScale.y;
                        size.z *= _BoxCollider.transform.lossyScale.z;
                        colliders = Physics.OverlapBox(_BoxCollider.transform.TransformPoint(_BoxCollider.center), size / 2, _BoxCollider.transform.rotation, layerMask);
                    }
                    break;
                case RoveTriggerType.OVERLAP_SPHERE:
                    if (_SphereCollider)
                    {
                        var radius = _SphereCollider.radius;
                        radius *= _SphereCollider.transform.lossyScale.x;
                        colliders = Physics.OverlapSphere(_SphereCollider.transform.TransformPoint(_SphereCollider.center), radius, layerMask);
                    }
                    break;
            }
            for (int i = _ColliderStaying.Count - 1; i >= 0; i--)
            {
                if (_ColliderStaying[i] == null)
                {
                    _ColliderStaying.RemoveAt(i);
                }
            }
            if (colliders != null)
            {
                var tempList = new List<Collider>(_ColliderStaying);
                foreach (var c in colliders)
                {
                    if (c == null)
                    {
                        continue;
                    }

                    if (_LimitTriggerCount != 0)
                    {
                        if (_ColliderStaying.Count >= _LimitTriggerCount)
                        {
                            if (!_ColliderStaying.Contains(c))
                            {
                                continue;
                            }
                        }
                    }

                    switch (_TriggerTarget)
                    {
                        case RoveTriggerTarget.GAMEOBJECTS:
                            if (!_Triggerers.Contains(c.gameObject))
                            {
                                continue;
                            }
                            break;
                        case RoveTriggerTarget.ROVE_TRIGGERER:
                            if (!isTriggerer(c))
                            {
                                continue;
                            }
                            break;
                    }
                    if (c.gameObject != gameObject)
                    {
                        if (_ColliderStaying.Contains(c))
                        {
                            hasObjectStay(c);
                            tempList.Remove(c);
                            //log($"{c.gameObject.name} stay");
                        }
                        else
                        {
                            _ColliderStaying.Add(c);
                            haveObjectEnter(c);
                            log($"{c.gameObject.name} enter");
                        }
                    }
                }

                foreach (var c in tempList)
                {
                    if (c != null)
                    {
                        haveObjectExit(c);
                        log($"{c.gameObject.name} exit");
                        _ColliderStaying.Remove(c);
                    }
                }
            }

            if (_IsTriggerred)
            {
                if (_ColliderStaying.Count < _NumberToTrigger)
                {
                    _IsTriggerred = false;
                    invokeExit();
                    log("RoveTrigger Exit");
                    _triggerEnterTime = 0;
                }
            }
            else
            {
                if (_ColliderStaying.Count >= _NumberToTrigger)
                {
                    _triggerEnterTime += Time.fixedDeltaTime;
                    if (_triggerEnterTime > _minTimeToTrigger)
                    {
                        _IsTriggerred = true;
                        invokeEnter();
                        log("RoveTrigger Enter");
                    }
                }
            }
        }

        private bool isTriggerer(Collider c)
        {
            if (!c)
            {
                return false;
            }
            IPlayerTriggerer playerTrigger = null;
            if ((_TriggererType & RoveTriggererType.LOCAL_PLAYER) != 0)
            {
                if (playerTrigger == null)
                {
                    playerTrigger = c.GetComponent<IPlayerTriggerer>();
                }
                if (playerTrigger != null && playerTrigger.IsLocalPlayer)
                {
                    return true;
                }
            }
            if ((_TriggererType & RoveTriggererType.OTHER_PLAYER) != 0)
            {
                if (playerTrigger == null)
                {
                    playerTrigger = c.GetComponent<IPlayerTriggerer>();
                }
                if (playerTrigger != null && !playerTrigger.IsLocalPlayer)
                {
                    return true;
                }
            }
            if ((_TriggererType & RoveTriggererType.NPC) != 0)
            {
                var triggerer = c.GetComponent<INPCTriggerer>();
                if (triggerer != null)
                {
                    return true;
                }
            }
            return false;
        }

        void OnDrawGizmos()
        {
            if (_IsEnableGizmos)
            {
                Gizmos.color = Color.red;
                if (_TriggerType == RoveTriggerType.OVERLAP_BOX)
                {
                    if (_BoxCollider)
                    {
                        var size = _BoxCollider.size;
                        size.x *= _BoxCollider.transform.lossyScale.x;
                        size.y *= _BoxCollider.transform.lossyScale.y;
                        size.z *= _BoxCollider.transform.lossyScale.z;
                        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
                        Gizmos.DrawWireCube(_BoxCollider.center, size);
                    }
                }
                else if (_TriggerType == RoveTriggerType.OVERLAP_SPHERE)
                {
                    if (_SphereCollider)
                    {
                        var radius = _SphereCollider.radius;
                        radius *= _SphereCollider.transform.lossyScale.x;
                        Gizmos.DrawSphere(_SphereCollider.transform.TransformPoint(_SphereCollider.center), radius);
                    }
                }
            }
        }

        private void haveObjectEnter(Collider c)
        {
            _TriggerEvents?.invokeEnter(c);
        }
        private void haveObjectExit(Collider c)
        {
            _TriggerEvents?.invokeExit(c);
        }
        private void hasObjectStay(Collider c)
        {
            _TriggerEvents?.invokeStay(c);
        }
        private void invokeExit()
        {
            _TriggerEvents?.onExitTriggerEvent?.Invoke();
            invokeSetters(RoveTriggerEventType.OnExit);
        }
        private void invokeEnter()
        {
            _TriggerEvents?.onEnterTriggerEvent?.Invoke();
            invokeSetters(RoveTriggerEventType.OnEnter);
        }
        private void invokeSetters(RoveTriggerEventType eventType)
        {
            foreach (var invoker in _invokers)
            {
                if (invoker != null)
                {
                    if (invoker.EventType == eventType)
                    {
                        invoker.invoke(eventType);
                    }
                }
            }
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void log(string msg)
        {
            if (_IsEnableLog)
            {
                Debug.Log($"[RoveTrigger - {gameObject.name}] " + msg);
            }
        }
        public void updateSetterList()
        {
            _invokers = GetComponents<IRoveTriggerEventInvoker>();
        }
        public void clearCache()
        {
            _ColliderStaying.Clear();
        }
        void OnEnable()
        {
            clearCache();
        }
    }

    public enum RoveTriggerType
    {
        OVERLAP_SPHERE,
        OVERLAP_BOX,
    }
    public enum RoveTriggerTarget
    {
        LAYER_MASK,
        GAMEOBJECTS,
        ROVE_TRIGGERER,
    }
    [Serializable]
    public class RoveTriggerEvent
    {
        public UnityEvent onEnterTriggerEvent;
        public UnityEvent onExitTriggerEvent;

        public event Action<Collider> onEnterEvent;
        public event Action<Collider> onExitEvent;
        public event Action<Collider> onStayEvent;

        public void invokeEnter(Collider c)
        {
            onEnterEvent?.Invoke(c);
        }
        public void invokeExit(Collider c)
        {
            onExitEvent?.Invoke(c);
        }
        public void invokeStay(Collider c)
        {
            onStayEvent?.Invoke(c);
        }
    }
}
