using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.CompilerServices;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Rove.RoveExporter
{
    public class RovePhotoFrame : RoveMonoBehaviour, IVisibleInThumbnail
    {
        public enum DefaultMode
        {
            URL,
            TEXTURE,
        }
        [SerializeField] private MeshRenderer _photoRenderer;
        [SerializeField] private MeshRenderer _frame;
        [SerializeField] private MeshRenderer _backFrame;
        [SerializeField] private DefaultMode _defaultMode = DefaultMode.TEXTURE;
        [SerializeField] private string _defaultUrl;
        [SerializeField] private Texture2D _defaultTexture;
        [SerializeField] private Texture2D _emptyTexture;
        [SerializeField] private GameObject _loadingOverlay;
        [SerializeField] private int _sortIndex;

#if UNITY_EDITOR
        [SerializeField] private bool _IsShowUidOnSceneEditor = true;
        private void OnDrawGizmos()
        {
            if (_IsShowUidOnSceneEditor)
            {
                //old code, draw uid on scene view
                // drawLabelGizmos(Uid.ToString());

                //new code, draw sort index on scene view
                drawLabelGizmos(SortIndex.ToString());
            }
        }

        private void drawLabelGizmos(string text)
        {
            GUIStyle style = new GUIStyle(GUI.skin.label);
            style.fontSize = 50;
            style.alignment = TextAnchor.MiddleCenter;
            style.normal.textColor = Color.black;
            style.fontStyle = FontStyle.Bold;
            Handles.Label(transform.position, text, style);

            style.fontSize = 48;
            style.normal.textColor = Color.white;
            style.fontStyle = FontStyle.Normal;
            Handles.Label(transform.position, text, style);
        }
#endif

        public DefaultMode Mode => _defaultMode;
        public string Url => _defaultUrl;
        public Texture2D DefaultTexture => _defaultTexture;
        public MeshRenderer PhotoRenderer => _photoRenderer;
        public MeshRenderer Frame => _frame;
        public MeshRenderer BackFrame => _backFrame;
        public int SortIndex => _sortIndex;
        public GameObject LoadingOverlay => _loadingOverlay;

        private MeshRenderer _loadingIcon;
        private MeshRenderer _errorIcon;
        public MeshRenderer LoadingIcon
        {
            get
            {
                if (!_loadingIcon)
                {
                    _loadingIcon = _loadingOverlay.transform.GetChild(0).GetComponent<MeshRenderer>();
                }
                return _loadingIcon;
            }
        }
        public MeshRenderer ErrorIcon
        {
            get
            {
                if (!_errorIcon)
                {
                    _errorIcon = Instantiate(LoadingIcon, LoadingIcon.transform.parent);
                }
                return _errorIcon;
            }
        }

        private void Awake()
        {
            if (!_errorIcon)
            {
                _errorIcon = Instantiate(LoadingIcon, LoadingIcon.transform.parent);
            }
        }

        private void Start()
        {
            showError(false);
            showLoading(false);

            viewEmpty();
        }
        private void Update()
        {
            if (LoadingIcon.gameObject.activeInHierarchy)
            {
                var angles = LoadingIcon.transform.localEulerAngles;
                angles.z = Mathf.SmoothStep(0, -360, Time.time % 1);
                LoadingIcon.transform.localEulerAngles = angles;
            }
        }

        public void setDefaultTexture(Texture2D texture2D)
        {
            _defaultTexture = texture2D;
            _defaultMode = DefaultMode.TEXTURE;
        }
        public void setDefaultTexture(string url)
        {
            _defaultUrl = url;
            _defaultMode = DefaultMode.URL;
        }
        public void showLoading(bool isShow)
        {
            setActive(LoadingIcon.gameObject, isShow);
            updateOverlay();
        }
        public void showError(bool isShow)
        {
            setActive(ErrorIcon.gameObject, isShow);
            updateOverlay();
        }
        private void updateOverlay()
        {
            var isActive = LoadingIcon.gameObject.activeSelf || ErrorIcon.gameObject.activeSelf;
            setActive(_loadingOverlay, isActive);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void setActive(GameObject go, bool isActive)
        {
            if (go.activeSelf != isActive)
            {
                go.SetActive(isActive);
            }
        }

        public void view(Texture tex)
        {
            var photoTransform = _photoRenderer.transform;
            _photoRenderer.material.mainTexture = tex;
            if (tex == null || tex.width == 0 || tex.height == 0)
            {
                photoTransform.localScale = Vector3.zero;
                return;
            }
            var ratio = tex.width * 1f / tex.height;
            if (ratio > 1)
            {
                photoTransform.localScale = new Vector3(1, 1 / ratio, 1);
            }
            else if (ratio < 1)
            {
                photoTransform.localScale = new Vector3(ratio, 1, 1);
            }
            else
            {
                photoTransform.localScale = Vector3.one;
            }
        }
        public void viewDefault()
        {
            view(DefaultTexture);
        }
        public void viewEmpty()
        {
            view(_emptyTexture);
        }
    }
}
