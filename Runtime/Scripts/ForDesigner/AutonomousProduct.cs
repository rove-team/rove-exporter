using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class AutonomousProduct : RoveMonoBehaviour, IRoveTriggerEventInvoker
    {
        [SerializeField] private RoveTriggerEventType eventType;
        public string autonomousProductId;
        public RoveTriggerEventType EventType { get => eventType; set => eventType = value; }

        public event Action<AutonomousProduct> onAutonomousProductSelectedEvent;

        public void clearEvent()
        {
            onAutonomousProductSelectedEvent = null;
        }

        public void invoke(RoveTriggerEventType eventType)
        {
            if (eventType == EventType)
            {
                onAutonomousProductSelectedEvent?.Invoke(this);
            }
        }
    }
}
