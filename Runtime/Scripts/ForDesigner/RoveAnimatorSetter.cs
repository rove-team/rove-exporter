using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [ExecuteInEditMode]
    public class RoveAnimatorSetter : MonoBehaviour, IRoveTriggerEventInvoker
    {
        public enum RoveSetterType
        {
            Integer,
            Float,
            Boolean,
        }

        public Animator animator;
        public RoveTriggerEventType eventType;
        public RoveSetterType setterType;
        public string parameterName;
        public int intValue;
        public float floatValue;
        public bool boolValue;

        public RoveTriggerEventType EventType { get => eventType; set => eventType = value; }

        public bool IsLocalOnly
        {
            get
            {
                var checkers = GetComponents<ILocalOnlyChecker>();
                foreach (var checker in checkers)
                {
                    if (!checker.IsLocalOnly)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private void setNow(RoveTriggerEventType eventType)
        {
            if (string.IsNullOrEmpty(parameterName) || animator == null)
            {
                return;
            }
            if (eventType == this.eventType)
            {
                if (animator)
                {
                    switch (setterType)
                    {
                        case RoveSetterType.Integer:
                            animator.SetInteger(parameterName, intValue);
                            break;
                        case RoveSetterType.Float:
                            animator.SetFloat(parameterName, floatValue);
                            break;
                        case RoveSetterType.Boolean:
                            animator.SetBool(parameterName, boolValue);
                            break;
                    }
                }
            }
        }

        private void Update()
        {
            if (animator != null)
            {
                var animationSync = animator.GetComponent<RoveAnimatorSync>();
                if (IsLocalOnly)
                {
                    if (animationSync)
                    {
                        RoveUtils.destroy(animationSync);
                    }
                }
                else
                {
                    if (animationSync == null)
                    {
                        animationSync = animator.gameObject.AddComponent<RoveAnimatorSync>();
                    }
                }
            }
        }

        public void invoke(RoveTriggerEventType eventType)
        {
            if (eventType == EventType)
            {
                setNow(eventType);
            }
        }
    }
}
