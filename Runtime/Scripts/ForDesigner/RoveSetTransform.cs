using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class RoveSetTransform : MonoBehaviour
    {
        [SerializeField] private Vector3 _position;
        [SerializeField] private Vector3 _rotation;
        [SerializeField] private Vector3 _scale;

        [ContextMenu("Save current transform")]
        public void saveState()
        {
            _position = transform.position;
            _rotation = transform.eulerAngles;
            _scale = transform.localScale;
        }

        public void resetPosition()
        {
            transform.position = _position;
        }
        public void resetRotation()
        {
            transform.eulerAngles = _rotation;
        }
        public void resetScale()
        {
            transform.localScale = _scale;
        }

        [ContextMenu("Reset transform state")]
        public void resetTransform()
        {
            resetPosition();
            resetRotation();
            resetScale();
        }
        public void setPosition(Transform from)
        {
            transform.position = from.position;
        }
        public void setRotation(Transform from)
        {
            transform.rotation = from.rotation;
        }
        public void setLocalScale(Transform from)
        {
            transform.localScale = from.localScale;
        }
        public void setTransformValue(Transform from)
        {
            setPosition(from);
            setRotation(from);
            setLocalScale(from);
        }
    }
}
