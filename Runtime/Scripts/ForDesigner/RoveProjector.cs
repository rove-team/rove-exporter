using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class RoveProjector : RoveMonoBehaviour
    {
        [SerializeField] private RoveProjectorScreen _ProjectorScreen;
        [SerializeField] private bool _isAutoCreateProjectorScreen = true;

        private void OnValidate()
        {
            if (_isAutoCreateProjectorScreen)
            {
                if (!_ProjectorScreen)
                {
                    var screenPrefab = Resources.Load<RoveProjectorScreen>("RoveProjectorScreen");
                    var screen = Instantiate(screenPrefab);
                    screen.transform.position = transform.position + transform.forward;
                    screen.transform.rotation = transform.rotation;
                    _ProjectorScreen = screen;
                    _ProjectorScreen.name = $"RoveProjectorScreen of {gameObject.name}";
                    _ProjectorScreen.transform.SetParent(transform, true);
                }
            }
        }

        private void OnDrawGizmos()
        {
            if (_ProjectorScreen)
            {
                var renderer = _ProjectorScreen.GetComponent<Renderer>();
                if (renderer != null)
                {
                    var bounds = renderer.bounds;
                    Gizmos.color = Color.green;
                    Gizmos.DrawLine(transform.position, bounds.center);
                }
            }
        }
        private void OnEnable()
        {
            if (_ProjectorScreen)
            {
                _ProjectorScreen.gameObject.SetActive(true);
            }
        }
        private void OnDisable()
        {
            if (_ProjectorScreen)
            {
                _ProjectorScreen.gameObject.SetActive(false);
            }
        }
        private void OnDestroy()
        {
            if (_ProjectorScreen)
            {
                Destroy(_ProjectorScreen.gameObject);
            }
        }
    }
}
