using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public interface IPlayerTriggerer
    {
        bool IsLocalPlayer { get; }
    }

    public interface INPCTriggerer
    {

    }

    [Flags]
    public enum RoveTriggererType
    {
        LOCAL_PLAYER = 1,
        OTHER_PLAYER = 1 << 1,
        NPC = 1 << 2,
    }
}
