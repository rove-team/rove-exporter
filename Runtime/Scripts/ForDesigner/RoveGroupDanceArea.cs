using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [RequireComponent(typeof(RoveTrigger))]
    public class RoveGroupDanceArea : RoveMonoBehaviour
    {
        [SerializeField] private AnimationClip[] _animationClips;
        public AnimationClip[] AnimationClips => _animationClips;
        [SerializeField] private bool _isRandomDance;
        [SerializeField] private float _delayStartDanceTime = 3;
        [SerializeField] Transform _danceDirection;
        [SerializeField] private bool _isHideDirectionOnRuntime;
    
        public Transform DanceDirection => _danceDirection;
        public bool IsRandomDance => _isRandomDance;
        public float DelayStartDanceTime => _delayStartDanceTime;

        private void Start()
        {
            if (_isHideDirectionOnRuntime && _danceDirection)
            {
                _danceDirection.gameObject.SetActive(false);
            }
        }
    }
}
