using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;

namespace Rove.RoveExporter
{
    [RequireComponent(typeof(VideoPlayer))]
    public class RoveVideoPlayerOutput : RoveMonoBehaviour
    {
        private VideoPlayer _videoPlayer;
        [SerializeField] private UnityEvent<double> onOutputPlayedTime;
        [SerializeField] private UnityEvent onVideoStarted;
        [SerializeField] private UnityEvent onVideoStopped;

        [SerializeField] private float _outputFps = 1;
        [SerializeField] private bool _isEnableLog;
        public event Action<double> onOutputPlayedTimeEvent;

        private float _lastOutTime;
        private double _startTime;
        public bool IsPlaying { get; private set; }

        protected void Awake()
        {
            _videoPlayer = GetComponent<VideoPlayer>();
            if (_outputFps < 0)
            {
                _outputFps = 1;
            }
        }

        private void Update()
        {
            checkVideoPlaying();
            checkVideoState();
            //checkDelayVideo();
        }
        private void checkVideoPlaying()
        {
            if (_videoPlayer.isPlaying)
            {
                var dtTime = 1f / _outputFps;
                if (Time.time > _lastOutTime + dtTime)
                {
                    _lastOutTime = Time.time;
                    callOutputEvent();
                }
                else if (_videoPlayer.time == 0)
                {
                    _lastOutTime = Time.time;
                    callOutputEvent();
                }
            }
        }
        private void checkVideoState()
        {
            if (IsPlaying != _videoPlayer.isPlaying)
            {
                IsPlaying = _videoPlayer.isPlaying;
                if (IsPlaying)
                {
                    onVideoStarted?.Invoke();
                    if (_isEnableLog)
                    {
                        Debug.Log("Start video");
                    }
                    _startTime = Time.time - _videoPlayer.time;
                }
                else
                {
                    onVideoStopped?.Invoke();
                    if (_isEnableLog)
                    {
                        Debug.Log("Stop video");
                    }
                }
            }
        }
        private void checkDelayVideo()
        {
            if (!_videoPlayer.isPlaying)
            {
                return;
            }
            var playedTime = Time.time - _startTime;
            var deltaWithRealtime = Mathf.Abs((float)(playedTime - _videoPlayer.time));
            if (deltaWithRealtime < 1)
            {
                _videoPlayer.time = playedTime;
                callOutputEvent();
            }
        }
        private void callOutputEvent()
        {
            onOutputPlayedTime?.Invoke(_videoPlayer.time);
            onOutputPlayedTimeEvent?.Invoke(_videoPlayer.time);
            if (_isEnableLog)
            {
                Debug.Log($"Audio played time: {_videoPlayer.time}");
            }
        }

    }
}
