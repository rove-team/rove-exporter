using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Rove.RoveExporter
{
    [DisallowMultipleComponent]
    public class RovePoint : RoveMonoBehaviour
    {
        [SerializeField, HideInInspector] private string _RoveId;
        [SerializeField, HideInInspector] private string[] _roveTags;
        [SerializeField, HideInInspector] private RoveParameter[] _roveParameters;
        [SerializeField] private bool _IsDisableOnRuntime;
        
        void OnValidate()
        {
            if (_roveTags != null && _roveTags.Length > 0)
            {
                _RoveId = _roveTags[0];
            }
        }

        [Obsolete("Will be removed later, using isContainTag instead")]
        public string RoveId
        {
            get
            {
                if (!string.IsNullOrEmpty(_RoveId))
                {
                    return _RoveId;
                }
                if (_roveTags != null && _roveTags.Length > 0)
                {
                    return _roveTags[0];
                }
                return null;
            }
            set
            {
                _RoveId = value;
            }
        }


        protected void Awake()
        {
            if (_IsDisableOnRuntime)
            {
                gameObject.SetActive(false);
            }
        }

        public bool isContainTag(string tagName)
        {
            if (string.IsNullOrEmpty(tagName))
            {
                return false;
            }
            if (_RoveId == tagName)
            {
                return true;
            }
            if (_roveTags == null)
            {
                return false;
            }
            foreach (var t in _roveTags)
            {
                if (t == tagName)
                {
                    return true;
                }
            }
            return false;
        }
        public bool tryGetParameter(string paramName, out RoveParameter resultParameter)
        {
            if (string.IsNullOrEmpty(paramName))
            {
                resultParameter = default;
                return false;
            }
            if (_roveParameters == null)
            {
                resultParameter = default;
                return false;
            }
            foreach (var param in _roveParameters)
            {
                if (param.Key == paramName)
                {
                    resultParameter = param;
                    return true;
                }
            }
            resultParameter = default;
            return false;
        }

        public T getParameter<T>(string paramName)
        {
            var result = getParameter(paramName, typeof(T));
            return (T)result;
        }
        public bool tryGetParameter<T>(string paramName, out T result)
        {
            try
            {
                result = getParameter<T>(paramName);
                return true;
            }
            catch (Exception ex)
            {
                Debug.LogWarning(ex);
                result = default;
                return false;
            }
        }
        public object getParameter(string paramName, Type type)
        {
            if (tryGetParameter(paramName, out var roveParameter))
            {
                if (type == roveParameter.Value.GetType())
                {
                    return roveParameter.Value;
                }

                if (roveParameter.RoveParameterType == RoveParameterType.Component && !type.IsPrimitive && type != typeof(string))
                {
                    if (roveParameter.ComponentValue is Component)
                    {
                        var component = (Component)roveParameter.ComponentValue;
                        if (component == null)
                        {
                            return null;
                        }

                        if (type == typeof(GameObject))
                        {
                            return component.gameObject;
                        }
                        return component.GetComponent(type);
                    }
                    else if (roveParameter.ComponentValue is GameObject)
                    {
                        var go = roveParameter.ComponentValue as GameObject;
                        return go.GetComponent(type);
                    }
                    else
                    {
                        return roveParameter.ComponentValue;
                    }
                }
                throw new Exception($"Param `{paramName}`, Type: `{roveParameter.Value.GetType()}` not match with input type: `{type}`");
            }
            return null;
        }

        /// <summary>
        /// callback only run when this has parameter
        /// </summary>
        public void doParameter<T>(string paramName, Action<T> callback)
        {
            try
            {
                var result = getParameter<T>(paramName);
                if (result != null)
                {
                    callback?.Invoke(result);
                }
                else
                {
                    Debug.LogWarning($"Can't found `{typeof(T)}` with param `{paramName}`");
                }
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Can't get parameter: {paramName}, type: {typeof(T)}. Exception: " + e);
            }
        }

#if UNITY_EDITOR
        [SerializeField] private bool _IsShowRoveIdOnSceneEditor = true;
        [SerializeField] private bool _IsShowUidOnSceneEditor = true;
        private void OnDrawGizmos()
        {
            var content = "";
            if (_IsShowRoveIdOnSceneEditor)
            {
                if (_roveTags != null && _roveTags.Length > 0)
                {
                    content += _roveTags[0].ToString();
                }
            }
            if (_IsShowUidOnSceneEditor)
            {
                content += $"\n[Id-{Uid}]";
            }
            if (!string.IsNullOrEmpty(content))
            {
                GUIStyle style = new GUIStyle(GUI.skin.label);
                style.alignment = TextAnchor.MiddleCenter;
                style.fontSize = 30;
                style.normal.textColor = Color.white;
                Handles.Label(transform.position, content, style);
            }
        }
#endif
    }
}
