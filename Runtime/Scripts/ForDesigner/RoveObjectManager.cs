using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Rove.RoveExporter
{
    [ExecuteInEditMode]
    public class RoveObjectManager : MonoBehaviour
    {
        public class RoveUidDataList : AbsJsonable
        {
            public int buildNumber;
            public List<RoveUidData> data = new List<RoveUidData>();
            [Serializable]
            public class RoveUidData
            {
                public string uniqueCode;
                public int buildNumber;
                public string lastedExportVersion;
            }
        }
        [SerializeField] private List<RoveUid> _roveUidList = new List<RoveUid>();
        [SerializeField] private string _exportVersion;
        [SerializeField] private string _roveUidJson;

        public List<RoveUid> RoveUidList => _roveUidList;
        public string ExportVersion => _exportVersion;

        private void Awake()
        {
            gameObject.hideFlags = HideFlags.NotEditable;
            transform.SetAsFirstSibling();
            RoveUid.clearEvent();
            addEvents();
        }

        private void onRoveUid_onHaveNewRoveUidEvent(RoveUid obj)
        {
            if (obj == null) return;
            if (obj.gameObject.scene == null || string.IsNullOrEmpty(obj.gameObject.scene.name)) return;

            if (!_roveUidList.Contains(obj))
            {
                obj.regenerateUid();
                _roveUidList.Add(obj);
            }
            else
            {
                if (obj.Uid == 0)
                {
                    obj.regenerateUid();
                }
            }
        }
        private void onRoveUid_onDestroyRoveUidEvent(RoveUid obj)
        {
            for (int i = _roveUidList.Count - 1; i >= 0; i--)
            {
                if (_roveUidList[i] == null)
                {
                    _roveUidList.RemoveAt(i);
                }
            }
            if (obj.gameObject.scene == null || string.IsNullOrEmpty(obj.gameObject.scene.name)) return;
            var index = _roveUidList.IndexOf(obj);
            if (index >= 0)
            {
                _roveUidList.RemoveAt(index);
            }
        }

        public void addEvents()
        {
            RoveUid.onHaveNewRoveUidEvent -= onRoveUid_onHaveNewRoveUidEvent;
            RoveUid.onDestroyRoveUidEvent -= onRoveUid_onDestroyRoveUidEvent;

            RoveUid.onHaveNewRoveUidEvent += onRoveUid_onHaveNewRoveUidEvent;
            RoveUid.onDestroyRoveUidEvent += onRoveUid_onDestroyRoveUidEvent;

        }
        public void findReferences()
        {
            updateRoveUidList();

            var roveUids = FindObjectsOfType<RoveUid>(true);
            foreach (var r in roveUids)
            {
                if (r != null)
                {
                    onRoveUid_onHaveNewRoveUidEvent(r);
                }
            }
        }

        public void updateRoveUidList()
        {
            for (int i = _roveUidList.Count-1; i>=0; i--)
            {
                if (!_roveUidList[i])
                {
                    _roveUidList.RemoveAt(i);
                }
            }
        }

#if UNITY_EDITOR
        public void buildJson()
        {
            RoveUidDataList dataList = JsonUtility.FromJson<RoveUidDataList>(_roveUidJson);
            _exportVersion = RoveExporterInfo.EXPORT_VERSION;
            if (dataList == null)
            {
                dataList = new RoveUidDataList();
            }
            dataList.buildNumber++;
            foreach (var roveUid in RoveUidList)
            {
                if (roveUid)
                {
                    RoveUidDataList.RoveUidData fData = dataList.data.Find((d) => d.uniqueCode == roveUid.UniqueCode);
                    if (fData == null)
                    {
                        dataList.data.Add(new RoveUidDataList.RoveUidData()
                        {
                            uniqueCode = roveUid.UniqueCode,
                            buildNumber = dataList.buildNumber,
                            lastedExportVersion = RoveExporterInfo.EXPORT_VERSION,
                        });
                    }
                    else
                    {
                        fData.buildNumber = dataList.buildNumber;
                        fData.lastedExportVersion = RoveExporterInfo.EXPORT_VERSION;
                    }
                }
            }
            _roveUidJson = dataList.toJson();
        }
#endif
    }
}
