using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class RoveTransformSync : RoveMonoBehaviour
    {
        public bool isSyncPosition = true;
        public bool isSyncRotation = true;
        public bool isSyncLocalScale = false;
    }
}
