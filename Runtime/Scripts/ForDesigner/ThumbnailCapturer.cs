using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEngine;

namespace Rove.RoveExporter
{
    [RequireComponent(typeof(Camera)), DisallowMultipleComponent, ExecuteInEditMode]
    public class ThumbnailCapturer : RoveMonoBehaviour, IVisibleInThumbnail
    {
        [SerializeField] private bool _IsDestroyOnRuntime = true;

        protected void Awake()
        {
#if UNITY_EDITOR
            var listener = GetComponent<AudioListener>();
            if (listener != null)
            {
                DestroyImmediate(listener);
            }
#endif
            if (_IsDestroyOnRuntime && Application.isPlaying)
            {
#if !UNITY_EDITOR
                Destroy(gameObject);
#else
                gameObject.SetActive(false);
#endif
            }
        }

#if UNITY_EDITOR
        const string PREVIEW_EXTENSION = ".preview";
        [SerializeField] private Vector2Int _Resolution = new Vector2Int(4096, 4096);
        [SerializeField] private bool _IsHideAllCanvasWhenCapture = false;
        [SerializeField] private bool _IsFirst = true;
        public string _UrlPath;
        private void OnGUI()
        {
            if (_IsFirst)
            {
                _IsFirst = false;
                captureNow();
            }
        }

        [HideInInspector] public bool _IsMarkClearCache = false;
        [ContextMenu("Clear captured directory cache")]
        public void clearCache()
        {
            var path = generatePath();
            Directory.Delete(Path.GetDirectoryName(path), true);
            _IsMarkClearCache = true;
            _UrlPath = null;
            Resources.UnloadUnusedAssets();
        }
        public void captureNow()
        {
            capture(_Resolution.x, _Resolution.y);
        }
        public void capture(int w, int h)
        {
            if (File.Exists(_UrlPath + PREVIEW_EXTENSION))
            {
                File.Delete(_UrlPath + PREVIEW_EXTENSION);
            }
            if (File.Exists(_UrlPath))
            {
                File.Delete(_UrlPath);
                _IsMarkClearCache = true;
                _UrlPath = null;
            }

            List<GameObject> hideObjects = new List<GameObject>();
            if (_IsHideAllCanvasWhenCapture)
            {
                var canvases = GameObject.FindObjectsOfType<Canvas>();
                foreach (Canvas c in canvases)
                {
                    hideObjects.Add(c.gameObject);
                }
            }
            var iHideInThumbnails = GameObject.FindObjectsOfType<Component>();
            foreach (var iObject in iHideInThumbnails)
            {
                if (iObject.GetComponent<IVisibleInThumbnail>() != null)
                {
                    if (!hideObjects.Contains(iObject.gameObject))
                    {
                        hideObjects.Add(iObject.gameObject);
                    }
                }
            }
            
            for (int i = 0; i < hideObjects.Count; i++)
            {
                hideObjects[i].gameObject.SetActive(false);
            }

            var camera = GetComponent<Camera>();
            var texture = TextureUtils.getFromCamera(camera, w, h, true);

            for (int i = 0; i < hideObjects.Count; i++)
            {
                hideObjects[i].gameObject.SetActive(true);
            }

            var tData = texture.EncodeToJPG();
            texture.Apply();
            var texturePreview = TextureUtils.getFromTexture(texture, 256, 256);
            var previewData = texturePreview.EncodeToJPG();
            DestroyImmediate(texture);
            DestroyImmediate(texturePreview);
            var imagePath = generatePath();
            File.WriteAllBytes(imagePath, tData);
            File.WriteAllBytes(imagePath + PREVIEW_EXTENSION, previewData);
            _UrlPath = imagePath;
        }
        public string generatePath()
        {
            var projectPath = Path.GetDirectoryName(Application.dataPath);
            var sceneName = UnityEngine.SceneManagement.SceneManager.GetActiveScene().name;
            var sceneMd5 = sceneName + "_" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().path.createMD5();
            var tempPath = Path.Combine(projectPath, "Library", "RoveCache", "ThumbnailCapture", sceneMd5);
            Path.GetFileNameWithoutExtension(sceneName);
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            };
            return Path.Combine(tempPath, $"{gameObject.GetInstanceID()}.jpg");
        }
        public void saveScene()
        {
            //if (!Application.isPlaying)
            //{
            //    UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
            //    UnityEditor.SceneManagement.EditorSceneManager.SaveOpenScenes();
            //}
        }
#endif
    }
    public interface IVisibleInThumbnail
    {

    }
}