using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [RequireComponent(typeof(RoveUid))]
    public abstract class RoveMonoBehaviour : MonoBehaviour
    {
        [HideInInspector, SerializeField] private RoveUid _roveUid;

        public RoveUid RoveUid
        {
            get
            {
                if (_roveUid == null)
                {
                    _roveUid = GetComponent<RoveUid>();
                    if (_roveUid == null)
                    {
                        _roveUid = gameObject.AddComponent<RoveUid>();
                    }
                }
                return _roveUid;
            }
        }

        public int Uid
        {
            get
            {
                if (RoveUid)
                {
                    return RoveUid.Uid;
                }
                return int.MinValue;
            }
        }
        public string UniqueCode
        {
            get
            {
                if (RoveUid)
                {
                    return RoveUid.UniqueCode;
                }
                return "";
            }
        }
        public bool IsRuntimeCreated => RoveUid.IsRuntimeCreated;
    }
}
