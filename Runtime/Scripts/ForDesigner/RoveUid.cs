using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
using UnityEditor;
#endif

namespace Rove.RoveExporter
{
    [ExecuteInEditMode, DisallowMultipleComponent]
    public class RoveUid : MonoBehaviour
    {
        public static event Action<RoveUid> onHaveNewRoveUidEvent;
        public static event Action<RoveUid> onDestroyRoveUidEvent;
        public static List<RoveUid> RoveUidTable { get; private set; } = new List<RoveUid>();
        [SerializeField] private int _uid;
        /// <summary>
        /// Will is empty while generate on runtime
        /// </summary>
        [SerializeField] private string _uniqueCode;
        
        public bool IsRuntimeCreated { get; private set; }

        public int Uid
        {
            get
            {
                if (!_isInvokeNewEvent)
                {
                    invokeNewRoveUidEvent();
                }
                return _uid;
            }
        }
        public string UniqueCode
        {
            get
            {
                if (!_isInvokeNewEvent)
                {
                    invokeNewRoveUidEvent();
                }
                return _uniqueCode;
            }
        }

        public void setId(int uid, string uniqueCode)
        {
            _uid = uid;
            _uniqueCode = uniqueCode; 
        }

        private bool _isInvokeNewEvent;

        private void Start()
        {
            if (!_isInvokeNewEvent)
            {
                invokeNewRoveUidEvent();
            }
        }

        private void OnDestroy()
        {
            if (Application.isPlaying)
            {
                var index = RoveUidTable.IndexOf(this);
                if (index >= 0)
                {
                    RoveUidTable.RemoveAt(index);
                }
            }
            onDestroyRoveUidEvent?.Invoke(this);
        }

        public void invokeNewRoveUidEvent()
        {
            _isInvokeNewEvent = true;
            onHaveNewRoveUidEvent?.Invoke(this);
            if (Application.isPlaying)
            {
                if (!RoveUidTable.Contains(this))
                {
                    RoveUidTable.Add(this);
                }
            }
        }
        public void clearUid()
        {
            _uid = 0;
            _uniqueCode = null;
        }
        public void regenerateUid()
        {
            if (gameObject.isInScene())
            {
                clearUid();
                generateId();
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    var modifications = PrefabUtility.GetPropertyModifications(this);
                    if (modifications == null)
                    {
                        return;
                    }
                    var propertyModifications = new List<PropertyModification>(modifications);

                    var propertyUid = propertyModifications.Find((p) => p.propertyPath == "_uid");
                    if (propertyUid == null && propertyUid == null)
                    {
                        propertyUid = new PropertyModification();
                        propertyUid.target = this;
                        propertyUid.propertyPath = "_uid";
                        propertyUid.value = _uid.ToString();
                        propertyModifications.Add(propertyUid);
                    }
                    else
                    {
                        propertyUid.value = _uid.ToString();
                    }

                    var propertyUniCode = propertyModifications.Find((p) => p.propertyPath == "_uniqueCode");
                    if (propertyUniCode == null)
                    {
                        propertyUniCode = new PropertyModification();
                        propertyUniCode.target = this;
                        propertyUniCode.propertyPath = "_uniqueCode";
                        propertyUniCode.value = _uniqueCode;
                        propertyModifications.Add(propertyUniCode);
                    }
                    else
                    {
                        propertyUniCode.value = _uniqueCode;
                    }

                    PrefabUtility.RecordPrefabInstancePropertyModifications(this);
                }
#endif
            }
        }

        private void generateId()
        {
            if (Application.isPlaying)
            {
                _uid = generateIdRunTime();
                generateUniqueCode(true);
                IsRuntimeCreated = true;
            }
            else
            {
                var type = this.GetType();
                _uid = RoveUtils.generateNextUid(type);
                generateUniqueCode(false);
            }
        }
        private int generateIdRunTime()
        {
            RoveUidTable.Sort((a, b) => -a._uid.CompareTo(b._uid));
            int id = 10000;
            for (int i = RoveUidTable.Count - 1; i >= 0 ; i--)
            {
                if (RoveUidTable[i] == null)
                {
                    RoveUidTable.RemoveAt(i);
                    continue;
                }
                if (RoveUidTable[i]._uid < id)
                {
                    continue;
                }
                if (RoveUidTable[i]._uid == id)
                {
                    id++;
                }
                else
                {
                    return id;
                }
            }
            return id;
        }
        private void generateUniqueCode(bool isRuntime)
        {
            _uniqueCode = Guid.NewGuid().ToString("n");
            if (isRuntime)
            {
                _uniqueCode += "_R";
            }
        }

        public static void clearEvent()
        {
            RoveUidTable.Clear();
            onHaveNewRoveUidEvent = null;
        }
        public void setUid(int uid)
        {
            _uid = uid;
        }
        public void setUniqueCode(string code)
        {
            _uniqueCode = code;
        }
    }
}
