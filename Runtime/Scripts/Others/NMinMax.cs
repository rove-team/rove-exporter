using System;

namespace Rove.RoveExporter
{
    [Serializable]
    public struct NMinMax01
    {
        public float min;
        public float max;
    }
}
