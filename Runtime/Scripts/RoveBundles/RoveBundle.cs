using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Networking;
using static Rove.RoveExporter.RoveBundle;

namespace Rove.RoveExporter
{
    public class RoveBundle
    {
        internal class AssetBundleUsing
        {
            public AssetBundle assetBundle;
            public int usingCount;
            public string md5;
            public void dispose()
            {
                assetBundle.Unload(true);
            }
        }
        internal static Dictionary<string, AssetBundleUsing> LoadedBundles { get; private set; } = new Dictionary<string, AssetBundleUsing>();

        public static int CacheCount => LoadedBundles.Count;
        public static void cleanCache()
        {
            foreach (var b in LoadedBundles.Values)
            {
                if (b != null && b.assetBundle)
                {
                    b.assetBundle.Unload(true);
                }
            }
            LoadedBundles.Clear();
        }
        public string Url { get; private set; }
        public string Md5 { get; private set; }
        public AssetBundle AssetBundle
        {
            get
            {
                if (LoadedBundles.ContainsKey(Md5))
                {
                    var bundleUsing = LoadedBundles[Md5];
                    return bundleUsing.assetBundle;
                }
                return null;
            }
        }
        public bool IsLoaded { get; private set; }
        public string[] AllScenePaths { get; private set; }
        public string[] AllAssetPaths { get; private set; }

        public void unload(bool unloadAllLoadedObjects)
        {
            try
            {
                if (LoadedBundles.ContainsKey(Md5))
                {
                    var bundleUsing = LoadedBundles[Md5];
                    bundleUsing.usingCount--;
                    if (bundleUsing.usingCount <= 0)
                    {
                        bundleUsing.assetBundle.Unload(unloadAllLoadedObjects);
                        LoadedBundles.Remove(Md5);
                    }
                }
                else
                {
                    if (AssetBundle != null)
                    {
                        AssetBundle.Unload(unloadAllLoadedObjects);
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        }

        public RoveBundle(byte[] inData)
        {
            try
            {
                Md5 = createRoveMD5(inData);
                if (!LoadedBundles.ContainsKey(Md5))
                {
                    var bundle = AssetBundle.LoadFromMemory(inData);
                    LoadedBundles.Add(Md5, new AssetBundleUsing()
                    {
                        assetBundle = bundle,
                        md5 = Md5,
                    });
                }
                LoadedBundles[Md5].usingCount++;
            }
            catch (Exception e)
            {
                Debug.LogError("[RoveBundle] " + e);
            }
        }
        public RoveBundle(RoveBundleDownloadHandle downloadHandle)
        {
            try
            {
                Url = downloadHandle.Url;
                Md5 = createRoveMD5(Url);

                if (!LoadedBundles.ContainsKey(Md5))
                {
                    LoadedBundles.Add(Md5, new AssetBundleUsing()
                    {
                        assetBundle = downloadHandle.DownloadHandlerAssetBundle.assetBundle,
                        md5 = Md5,
                    });
                }
                else
                {
                    if (LoadedBundles[Md5].assetBundle != null)
                    {
                        
                    }
                    else
                    {
                        LoadedBundles[Md5] = new AssetBundleUsing()
                        {
                            assetBundle = downloadHandle.DownloadHandlerAssetBundle.assetBundle,
                            md5 = Md5,
                        };
                    }
                }
                LoadedBundles[Md5].usingCount++;
            }
            catch (Exception e)
            {
                Debug.LogError("[RoveBundle] " + e);
            }
        }
        internal RoveBundle(AssetBundleUsing bundleUsing)
        {
            Md5 = bundleUsing.md5;
            bundleUsing.usingCount++;
        }
        internal static string createRoveMD5(string url)
        {
            return RoveUtils.createMD5(url);
        }
        internal static string createRoveMD5(byte[] inData)
        {
            var src = inData;
            var length = src.Length < 256 ? src.Length : 256;
            var tempData = new byte[length + 4];
            Buffer.BlockCopy(src, src.Length - length, tempData, 0, length);
            Buffer.BlockCopy(BitConverter.GetBytes(inData.Length), 0, tempData, length, 4);
            return RoveUtils.createMD5(tempData);
        }

        public void loadAll()
        {
            try
            {
                if (!IsLoaded)
                {
                    IsLoaded = true;
                    if (AssetBundle == null)
                    {
                        throw new Exception($"Have error when load assetBundles, Url=?[{Url}], RoveBundleMd5={Md5}");
                    }
                    AllScenePaths = AssetBundle.GetAllScenePaths();
                    AllAssetPaths = AssetBundle.GetAllAssetNames();
                    if (AllScenePaths.Length == 0 && AllAssetPaths.Length == 0)
                    {
                        AssetBundle.LoadAllAssets();
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[RoveBundle] " + e);
                throw e;
            }
        }
    }
    public class RoveBundleDownloadHandle
    {
        public DownloadHandlerAssetBundle DownloadHandlerAssetBundle { get; private set; }
        public string Url { get; private set; }
        public RoveBundleDownloadHandle(string url, DownloadHandlerAssetBundle downloadHandler)
        {
            Url = url;
            DownloadHandlerAssetBundle = downloadHandler;
        }
        public RoveBundle getRoveBundle()
        {
            return new RoveBundle(this);
        }
    }

    public class RoveBundleCreation : RoveOperation
    {
        public static RoveBundleCreation create(byte[] inData)
        {
            return new RoveBundleCreation(inData);
        }
        public static RoveBundleCreation create(string path)
        {
            return new RoveBundleCreation(path);
        }
        private string _fromPath;
        private long _fileSize = -1;
        public RoveBundle RoveBundle { get; private set; }

        private RoveBundleCreation(byte[] inData)
        {
            try
            {
                _fileSize = inData.Length;
                var md5 = createRoveMD5(inData);
                if (!LoadedBundles.ContainsKey(md5))
                {
                    var request = AssetBundle.LoadFromMemoryAsync(inData);
                    setRequestEvent(md5, request);
                }
                else
                {
                    RoveBundle = new RoveBundle(LoadedBundles[md5]);
                    if (RoveBundle.AssetBundle == null)
                    {
                        RoveBundle = null;
                        LoadedBundles.Remove(md5);
                        var request = AssetBundle.LoadFromMemoryAsync(inData);
                        setRequestEvent(md5, request);
                    }
                    else
                    {
                        inheritedInvokeComplete();
                    }
                }

                
            }
            catch (Exception e)
            {
                invokeErrorAndComplete(e.Message);
            }
        }

        private void invokeErrorAndComplete(string errorMsg, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            inheritedSetErrorAndComplete(errorMsg, memberName, lineNumber, "RoveBundle.cs");
        }

        private static Dictionary<string, AssetBundleCreateRequest> _requesting = new Dictionary<string, AssetBundleCreateRequest>();

        private RoveBundleCreation(string path)
        {
            try
            {
                _fromPath = path;
                var fileInfo = new FileInfo(_fromPath);
                _fileSize = fileInfo.Length;

                var md5 = createRoveMD5(path);
                if (!LoadedBundles.ContainsKey(md5))
                {
                    AssetBundleCreateRequest request;
                    if (_requesting.ContainsKey(md5))
                    {
                        request = _requesting[md5];
                    }
                    else
                    {
                        request = AssetBundle.LoadFromFileAsync(path);
                        _requesting.Add(md5, request);
                    }
                    setRequestEvent(md5, request);
                }
                else
                {
                    RoveBundle = new RoveBundle(LoadedBundles[md5]);
                    if (RoveBundle.AssetBundle == null)
                    {
                        RoveBundle = null;
                        LoadedBundles.Remove(md5);
                        var request = AssetBundle.LoadFromFileAsync(path);
                        _requesting.Add(md5, request);
                        setRequestEvent(md5, request);
                    }
                    else
                    {
                        inheritedInvokeComplete();
                    }
                }


            }
            catch (Exception e)
            {
                invokeErrorAndComplete(e.Message);
            }
        }

        private void setRequestEvent(string md5, AssetBundleCreateRequest createRequest)
        {
            createRequest.completed += (operation) =>
            {
                _requesting.Remove(md5);
                if (!createRequest.assetBundle)
                {
                    inheritedSetErrorAndComplete($"Failed to load AssetBundle: fromPath?=[{_fromPath}], RoveBundleMd5=[{md5}], FileSize={_fileSize}");
                }
                else
                {
                    var bundleUsing = new AssetBundleUsing()
                    {
                        assetBundle = createRequest.assetBundle,
                        md5 = md5,
                    };
                    if (!LoadedBundles.ContainsKey(md5))
                    {
                        LoadedBundles.Add(md5, bundleUsing);
                    }
                    else
                    {
                        bundleUsing.dispose();
                    }
                    RoveBundle = new RoveBundle(LoadedBundles[md5]);
                    inheritedInvokeComplete();
                }
            };
        }
    }
}
