using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class RoveLinkObject : RoveMonoBehaviour
    {
        [SerializeField, HideInInspector] private GameObject _completeCloneRoot;
        [SerializeField, HideInInspector] private GameObject _linkedPattern;
        [SerializeField, HideInInspector] private OverrideLinkData _linkedData;

        public OverrideLinkData LinkedData => _linkedData;
        public GameObject LinkedPattern => _linkedPattern;

#if UNITY_EDITOR
        [Header("Create link data")]
        [SerializeField] private GameObject _completeObject;
        [SerializeField] private GameObject _originalObject;
        [HideInInspector] public GameObject PrefabInProject;

        public GameObject CompleteObject => _completeObject;
        public GameObject OriginalObject => _originalObject;

        public void createLinkData(bool isSaveAsPrefab)
        {
            if (_completeObject.transform.parent != null)
            {
                Debug.LogError("CompleteObject must be root object");
                return;
            }
            if (_originalObject.transform.parent != null)
            {
                Debug.LogError("OriginalObject must be root object");
                return;
            }
            if (_originalObject.isInScene())
            {
                Debug.LogError("OriginalObject must be prefab");
                return;
            }
            if (_completeObject.transform.childCount == 0)
            {
                Debug.LogError("CompleteObject child count must > 0");
                return;
            }
            if (_originalObject.transform.childCount == 0)
            {
                Debug.LogError("OriginalObject child count must > 0");
                return;
            }
            var pObject = RoveUtils.findObjectPattern(_completeObject, _originalObject, true);
            if (!pObject)
            {
                Debug.LogError("Not found original object pattern in CompleteObject");
                return;
            }

            if (_completeCloneRoot)
            {
                RoveUtils.destroy(_completeCloneRoot);
            }

            _completeCloneRoot = Instantiate(_completeObject, transform);
            _completeCloneRoot.name = CompleteObject.name + "_Linked";

            _linkedPattern = RoveUtils.findObjectPattern(_completeCloneRoot, _originalObject, true);
            _linkedData = new OverrideLinkData();

            var renderers = LinkedPattern.getComponentInPattern<Renderer>();
            int rendererIndex = 0;
            foreach (var r in renderers)
            {
                var linkMat = new OverrideLinkMaterial(rendererIndex);
                for (int i = 0; i < r.sharedMaterials.Length; i++)
                {
                    var mat = r.sharedMaterials[i];
                    var assetPath = AssetDatabase.GetAssetPath(mat);
                    bool isCustomMat = !string.IsNullOrEmpty(assetPath) && assetPath.EndsWith(".mat");
                    if (isCustomMat)
                    {
                        // add overrideLinkMat with key is index of materials in the array
                        linkMat.add(i, mat);
                    }
                }
                if (linkMat.Count > 0)
                {
                    _linkedData.add(linkMat);
                }
                rendererIndex++;
                RoveUtils.destroy(r);
            }

            var meshFilters = LinkedPattern.getComponentInPattern<MeshFilter>();
            foreach (var m in meshFilters)
            {
                RoveUtils.destroy(m);
            }

            if (isSaveAsPrefab)
            {
                var folderPath = Path.Combine(Application.dataPath, "RoveLinkObjectOutput", "Prefab");
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                var uniqueCode = GetComponent<RoveUid>().UniqueCode.Substring(0, 6);
                var savePath = Path.Combine(folderPath, CompleteObject.name + "_" + uniqueCode + ".prefab");
                if (File.Exists(savePath))
                {
                    File.Delete(savePath);
                }
                PrefabInProject = PrefabUtility.SaveAsPrefabAsset(gameObject, savePath);

                Debug.Log($"Created prefab at <color=lime>{savePath}</color>", PrefabInProject);
            }
        }

        [Header("Apply link data")]
        [SerializeField] private GameObject _editorImportedObject;
        public GameObject EditorImportedObject => _editorImportedObject;
        public void applyLinkedObjectEditor()
        {
            applyLinkData(EditorImportedObject);
            EditorImportedObject?.SetActive(false);
        }
#endif

        public void applyLinkData(GameObject importedObject)
        {
            var pObject = RoveUtils.findObjectPattern(importedObject, LinkedPattern, true);
            if (!pObject)
            {
                Debug.Log("Can't not find linked object in dynamic object");
                return;
            }

            var importedTrans = pObject.transform.getTransformInPattern();
            var linkedPatternTrans = LinkedPattern.transform.getTransformInPattern();

            for (int i = 1; i < importedTrans.Length; i++)
            {
                var t = importedTrans[i];
                var l = linkedPatternTrans[i];
                RoveUtils.copyAllComponents(t.gameObject, l.gameObject, typeof(MeshRenderer), typeof(MeshFilter));
            }

            applyOverrideMaterial(LinkedPattern, LinkedData);
        }

        private void applyOverrideMaterial(GameObject target, OverrideLinkData linkData)
        {
            var renderers = target.getComponentInPattern<Renderer>();
            foreach (var linkMat in linkData.LinkMaterials)
            {
                var renderer = renderers[linkMat.OrderInRoot];
                var materials = renderer.materials;
                foreach (var mat in linkMat.Materials)
                {
                    materials[mat.key] = mat.value;
                }
                renderer.materials = materials;
            }
        }
    }
}
