using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [Serializable]
    public abstract class AbsJsonable
    {
        public virtual string toJson()
        {
            return JsonUtility.ToJson(this);
        }
    }
    public static class RovePlatform
    {
        public const string Android = "Android";
        public const string iOS = "iOS";
        public const string StandaloneOSX = "StandaloneOSX";
        public const string StandaloneWindows64 = "StandaloneWindows64";
        public const string WebGL = "WebGL";
    }
    [Serializable]
    public class AddressableBuiltData
    {
        public string name;
        public string alias;
        public string unity_version;
        public string info;
        public List<AddressablePlatform> allPlatforms = new List<AddressablePlatform>();
        public string checksum;
        public string ThumnbnailUrl { get; set; }
        public bool IsUploaded
        {
            get
            {
                foreach (var p in allPlatforms)
                {
                    if (!p.IsUploaded)
                    {
                        return false;
                    }
                }
                return true;
            }
        }
        public AddressablePlatform add(string platform, string[] files)
        {
            var f = new AddressablePlatform()
            {
                platform = platform,
                all_files = files
            };
            allPlatforms.Add(f);
            return f;
        }
        public void initForUpload()
        {
            foreach (var p in allPlatforms)
            {
                p.initForUpload();
            }
        }
        public void resetUrls()
        {
            ThumnbnailUrl = "";
            foreach (var p in allPlatforms)
            {
                p.resetUrls();
            }
        }
        public bool setData(string fileName, byte[] data)
        {
            foreach (var p in allPlatforms)
            {
                var isSet = p.setData(fileName, data);
                if (isSet)
                {
                    return true;
                }
            }
            return false;
        }
        public bool setUrl(string fileName, string url)
        {
            foreach (var p in allPlatforms)
            {
                var isSet = p.setUrl(fileName, url);
                if (isSet)
                {
                    return true;
                }
            }
            return false;
        }
        public string[] getUrls(string platform)
        {
            foreach (var p in allPlatforms)
            {
                if (p.platform == platform)
                {
                    return p.Urls;
                }
            }
            return null;
        }
        public string toJson()
        {
            return JsonUtility.ToJson(this);
        }
        [Serializable]
        public class AddressablePlatform
        {
            public string platform;
            public string[] all_files;
            public string checksum;
            public string[] Urls { get; set; }
            public byte[][] All_Data { get; set; }
            public bool IsUploaded
            {
                get
                {
                    if (Urls == null) return false;
                    if (Urls.Length != all_files.Length) return false;
                    foreach (string url in Urls)
                    {
                        if (string.IsNullOrEmpty(url)) return false;
                    }
                    return true;
                }
            }
            public void initForUpload()
            {
                Urls = new string[all_files.Length];
                All_Data = new byte[all_files.Length][];
            }
            public bool setData(string fileName, byte[] data)
            {
                for (int i = 0; i < all_files.Length; i++)
                {
                    if (fileName == all_files[i])
                    {
                        All_Data[i] = data;
                        return true;
                    }
                }
                return false;
            }
            public bool setUrl(string fileName, string url)
            {
                for (int i = 0; i < all_files.Length; i++)
                {
                    if (fileName == all_files[i])
                    {
                        Urls[i] = url;
                        return true;
                    }
                }
                return false;
            }
            public void resetUrls()
            {
                Urls = new string[all_files.Length];
            }
        }
    }
    [Serializable]
    public class AddressableBuildInfo : AbsJsonable
    {
        public string unityVersion = Application.unityVersion;
        public string exportVersion;
        public string buildGuid;
    }
}
