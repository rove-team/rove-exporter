using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [Serializable]
    public class OverrideLinkData
    {
        [SerializeField] private List<OverrideLinkMaterial> linkMaterials = new List<OverrideLinkMaterial>();

        public void add(OverrideLinkMaterial linkMaterial)
        {
            this.linkMaterials.Add(linkMaterial);
        }
        public List<OverrideLinkMaterial> LinkMaterials => linkMaterials;
        public int Count => this.linkMaterials.Count;
    }
    [Serializable]
    public class OverrideLinkMaterial
    {
        [SerializeField] private int orderInRoot;
        [SerializeField] private List<MaterialKeyValue> materials = new List<MaterialKeyValue>();

        public int OrderInRoot => orderInRoot;
        public List<MaterialKeyValue> Materials => materials;

        public OverrideLinkMaterial(int orderInRoot)
        {
            this.orderInRoot = orderInRoot;
        }

        public void add(int key, Material material)
        {
            materials.Add(new MaterialKeyValue(key, material));
        }
        public int Count => materials.Count;
    }
    [Serializable]
    public struct MaterialKeyValue
    {
        public int key;
        public Material value;
        public MaterialKeyValue(int k, Material v)
        {
            key = k;
            value = v;
        }
    }
}
