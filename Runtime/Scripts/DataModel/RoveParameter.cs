using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Rove.RoveExporter
{
    [Serializable]
    public struct RoveParameter
    {
        [SerializeField] private string key;
        [SerializeField] private RoveParameterType type;
        [SerializeField] private int intValue;
        [SerializeField] private float floatValue;
        [SerializeField] private bool boolValue;
        [SerializeField] private string stringValue;
        [SerializeField] private Object componentValue;

        public string Key => key;
        public RoveParameterType RoveParameterType => type;
        public object Value
        {
            get
            {
                switch (type)
                {
                    case RoveParameterType.Bool:
                        return boolValue;
                    case RoveParameterType.Float:
                        return floatValue;
                    case RoveParameterType.Integer:
                        return intValue;
                    case RoveParameterType.String:
                        return stringValue;
                    case RoveParameterType.Component:
                        return componentValue;
                    default:
                        return null;
                }
            }
        }
        public int IntValue
        {
            get
            {
                if (type != RoveParameterType.Integer)
                {
                    throw new ArgumentException("RoveParameterType is not Integer");
                }
                return intValue;
            }
        }
        public bool BoolValue
        {
            get
            {
                if (type != RoveParameterType.Bool)
                {
                    throw new ArgumentException("RoveParameterType is not Boolean");
                }
                return boolValue;
            }
        }
        public float FloatValue
        {
            get
            {
                if (type != RoveParameterType.Float)
                {
                    throw new ArgumentException("RoveParameterType is not Float");
                }
                return floatValue;
            }
        }
        public string StringValue
        {
            get
            {
                if (type != RoveParameterType.String)
                {
                    throw new ArgumentException("RoveParameterType is not String");
                }
                return stringValue;
            }
        }

        public Object ComponentValue
        {
            get
            {
                if (type != RoveParameterType.Component)
                {
                    throw new ArgumentException("RoveParameterType is not Component");
                }
                return componentValue;
            }
        }
    }
    public enum RoveParameterType
    {
        Bool,
        Integer,
        Float,
        String,
        Component
    }
}
