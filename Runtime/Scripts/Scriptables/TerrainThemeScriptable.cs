using System;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    [CreateAssetMenu(fileName = "FileName", menuName = "ScriptableObject/TerrainThemeScriptable", order = 1)]
    public class TerrainThemeScriptable : ScriptableObject
    {
        public TerrainAlphaLayerData[] alphaLayers;
        public TerrainTreeData[] treeData;
    }

    [Serializable]
    public class TerrainAlphaLayerData
    {
        public TerrainAlphaType layer;
        public TerrainLayer terrainLayer;
    }

    [Serializable]
    public class TerrainTreeData
    {
        public TerrainAlphaType layer;
        public float density = 1;
        public TreeSet[] treeSets;
        [Serializable]
        public class TreeSet
        {
            public GameObject treePrefab;
            [Range(0, 1)] public float ratio;
        }
    }

    public enum TerrainAlphaType
    {
        UNDER_WATER = 0,
        DESERT = 1,
        SUBTROPICAL_DESERT = 2,
        TROPICAL_FOREST = 3,
        TROPICAL_RAIN_FOREST = 4,
        SAVANNA = 5,
        GRASS_LAND = 6,
        TEMPERATE_FOREST = 7,
        BARE_ROCK = 8,
        TUNDRA = 9,
        SNOW = 10,
    }
}
