using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Networking;

namespace Rove.RoveExporter
{
    public class RoveAddressableDownloader : MonoBehaviour
    {
        public void predownload(params string[] urls)
        {
            foreach (var u in urls)
            {
                AddressableDownloadCallback callback = new AddressableDownloadCallback();

                callback.onProgressEvent.AddListener((progress) =>
                {
                    Debug.Log($"[RoveAddressableCache] Pre-download - Downloading... - {u} - {progress * 100}%");
                });
                callback.onErrorEvent.AddListener((eCode, eMsg) =>
                {
                    Debug.Log($"[RoveAddressableCache] Pre-download - Error - {u} - {eMsg}");
                });
                callback.onSuccessEvent.AddListener((success) =>
                {
                    Debug.Log($"[RoveAddressableCache] Pre-download - Success - {u} - {success.Length}");
                });

                StartCoroutine(AddressableLoader.downloadBundles(new string[] { u }, callback));
            }
        }
    }
}
