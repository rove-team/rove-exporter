using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public abstract class AbsLoadedAddressableData
    {
        public RoveBundle[] RoveBundles { get; protected set; }
        public AbsLoadedAddressableData(RoveBundle[] bundles)
        {
            RoveBundles = bundles;
            foreach (var bundle in bundles)
            {
                bundle.loadAll();
            }
        }
        public void unload(bool isUnLoadedAllLoadedObjects)
        {
            if (RoveBundles != null)
            {
                foreach (var bundle in RoveBundles)
                {
                    if (bundle != null)
                    {
                        bundle.unload(isUnLoadedAllLoadedObjects);
                    }
                }
                RoveBundles = null;
            }
        }
    }
    public class LoadedAddressableData : AbsLoadedAddressableData
    {
        public LoadedAddressableData(RoveBundle[] bundles) : base(bundles)
        {

        }
        public string[] getAllScenePaths()
        {
            List<string> paths = new List<string>();
            foreach (var b in RoveBundles)
            {
                paths.AddRange(b.AllScenePaths);
            }
            return paths.ToArray();
        }
        public string[] getAllAssetPaths()
        {
            List<string> paths = new List<string>();
            foreach (var b in RoveBundles)
            {
                paths.AddRange(b.AllAssetPaths);
            }
            return paths.ToArray();
        }
        public T loadAsset<T>(string name) where T : Object
        {
            foreach (var rovebundle in RoveBundles)
            {
                if (rovebundle.AssetBundle.Contains(name))
                {
                    return rovebundle.AssetBundle.LoadAsset<T>(name);
                }
            }
            return null;
        }
        public AssetBundleRequest loadAssetAsync<T>(string name)
        {
            foreach (var rovebundle in RoveBundles)
            {
                if (rovebundle.AssetBundle.Contains(name))
                {
                    return rovebundle.AssetBundle.LoadAssetAsync<T>(name);
                }
            }
            return null;
        }
    }
}
