using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class AddressableLoaderOperation : RoveOperation
    {
        public static AddressableLoaderOperation startLoad(byte[][] inData)
        {
            List<RoveBundleCreation> creationAsyncs = new List<RoveBundleCreation>();
            foreach (var data in inData)
            {
                if (data == null)
                {
                    throw new Exception("Bundle is null");
                }
                var creation = RoveBundleCreation.create(data);
                creationAsyncs.Add(creation);
            }
            return new AddressableLoaderOperation(creationAsyncs.ToArray());
        }
        public static AddressableLoaderOperation startLoad(string[] inPath)
        {
            List<RoveBundleCreation> creationAsyncs = new List<RoveBundleCreation>();
            foreach (var path in inPath)
            {
                if (path == null)
                {
                    throw new Exception("Bundle is null");
                }
                var creation = RoveBundleCreation.create(path);
                creationAsyncs.Add(creation);
            }
            return new AddressableLoaderOperation(creationAsyncs.ToArray());
        }
        internal static AddressableLoaderOperation createErrorOperation(string errMsg)
        {
            var operation = new AddressableLoaderOperation();
            operation.inheritedSetErrorAndComplete(errMsg, "createErrorOperation", 42, "AddressableLoaderOperation.cs");
            return operation;
        }


        public RoveBundleCreation[] RoveBundleCreations { get; private set; } 
        public LoadedAddressableData LoadedData { get; private set; }
        private AddressableLoaderOperation() { }
        private AddressableLoaderOperation(RoveBundleCreation[] creations)
        {
            RoveBundleCreations = creations;

            if (RoveBundleCreations == null || RoveBundleCreations.Length <= 0)
            {
                invokeErrorAndComplete($"Create bundle error: RoveBundleCreations is null or empty: isNull={RoveBundleCreations == null}");
                return;
            }

            foreach (var creation in RoveBundleCreations)
            {
                creation.addCompleteCallback(checkComplete);
            }
            if (isComplete())
            {
                inheritedInvokeComplete();
            }
        }

        private void getCompleteData()
        {
            if (IsDone)
            {
                return;
            }
            try
            {
                List<RoveBundle> roveBundles = new List<RoveBundle>();
                foreach (var creation in RoveBundleCreations)
                {
                    roveBundles.Add(creation.RoveBundle);
                }
                LoadedData = new LoadedAddressableData(roveBundles.ToArray());
                inheritedInvokeComplete();
            }
            catch (Exception e)
            {
                invokeErrorAndComplete("Create bundle error: " + e.Message);
            }
        }
        private bool isComplete()
        {
            foreach (var creation in RoveBundleCreations)
            {
                if (!creation.IsDone)
                {
                    return false;
                }
            }
            return true;
        }
        private void checkComplete()
        {
            if (isComplete())
            {
                foreach (var creation in RoveBundleCreations)
                {
                    if (creation.IsError)
                    {
                        invokeErrorAndComplete("Create bundle error: " + creation.ErrorMessage);
                        return;
                    }
                }
                getCompleteData();
            }
            else
            {
                calculateProgress();
            }
        }
        private void calculateProgress()
        {
            var p = 0f;
            foreach (var task in RoveBundleCreations)
            {
                p += task.Progress;
            }
            inheritedSetProgress(p / RoveBundleCreations.Length);
        }
        private void invokeErrorAndComplete(string errorMsg, [CallerLineNumber] int lineNumber = 0, [CallerMemberName] string memberName = "")
        {
            inheritedSetErrorAndComplete(errorMsg, memberName, lineNumber, "AddressableLoaderOperation.cs");
        }
    }
}
