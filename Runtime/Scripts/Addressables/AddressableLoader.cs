using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace Rove.RoveExporter
{
    public static class AddressableLoader
    {
        public static IEnumerator loadBundles(byte[][] allData, AddressableLoaderCallback loaderCallback)
        {
            List<RoveBundle> bundles = new List<RoveBundle>();
            try
            {
                foreach (var data in allData)
                {
                    if (data == null)
                    {
                        throw new Exception("Bundle is null");
                    }
                    //var bundle = AssetBundle.LoadFromMemory(data);
                    var roveBundle = new RoveBundle(data);
                    bundles.Add(roveBundle);
                    roveBundle.loadAll();
                    loaderCallback?.invokeProgressEvent(bundles.Count * 1f / allData.Length);
                }
                loaderCallback?.invokeSuccessEvent(bundles.ToArray());
            }
            catch (Exception e)
            {
                var error = "Failed to load: " + e;
                loaderCallback?.invokeErrorEvent(AddressableErrorCode.BUNDLE_ERROR, error);
            }
            yield return null;
        }
        public static IEnumerator loadBundles(string[] bundlePaths, AddressableLoaderCallback loaderCallback, uint version = 0u, uint crc = 0u)
        {
            AddressableDownloadCallback downloadCallback = new AddressableDownloadCallback();
            List<RoveBundle> allBundles = new List<RoveBundle>();

            downloadCallback.onSuccessEvent.AddListener((roveBundleDownloadHandles) =>
            {
                foreach (var handler in roveBundleDownloadHandles)
                {
                    try
                    {
                        var roveBundle = handler.getRoveBundle();
                        roveBundle.loadAll();
                        allBundles.Add(roveBundle);
                    }
                    catch (Exception e)
                    {
                        foreach (var b in allBundles)
                        {
                            b?.unload(true);
                        }
                        Debug.LogError("[AddressableLoader] " + e);
                        loaderCallback?.invokeErrorEvent(AddressableErrorCode.BUNDLE_ERROR, e.Message);
                        return;
                    }
                }
                loaderCallback?.invokeSuccessEvent(allBundles.ToArray());
            });
            downloadCallback.onErrorEvent.AddListener((eCode, eStr) =>
            {
                loaderCallback?.invokeErrorEvent(eCode, eStr);
            });
            downloadCallback.onProgressEvent.AddListener((progress) =>
            {
                loaderCallback?.invokeProgressEvent(progress);
            });
            yield return downloadBundles(bundlePaths, downloadCallback, version, crc);
        }
        public static IEnumerator downloadBundles(string[] bundlePaths, AddressableDownloadCallback loaderCallback, uint version = 0u, uint crc = 0u)
        {
            List<UnityWebRequest> inProgressRequests = new List<UnityWebRequest>();
            List<UnityWebRequest> doneRequests = new List<UnityWebRequest>();
            List<RoveBundleDownloadHandle> downloadHandles = new List<RoveBundleDownloadHandle>();

            int totalProgress = bundlePaths.Length;

            bool isError = false;
            Action disposeAllRequest = () =>
            {
                foreach (var request in inProgressRequests)
                {
                    request.Dispose();
                }
                foreach (var request in doneRequests)
                {
                    request?.Dispose();
                }
                inProgressRequests.Clear();
                doneRequests.Clear();
            };
            IEnumerator throwError(AddressableErrorCode errorCode, string error)
            {
                log(error);
                loaderCallback?.invokeErrorEvent(errorCode, error);
                disposeAllRequest();
                isError = true;
                yield break;
            }

            foreach (var bundlePath in bundlePaths)
            {
                var request = UnityWebRequestAssetBundle.GetAssetBundle(bundlePath, version, crc);
                request.SendWebRequest();
                inProgressRequests.Add(request);
            }

            while (inProgressRequests.Count > 0)
            {
                float progress = 0;
                if (isError)
                {
                    yield break;
                }
                for (int i = inProgressRequests.Count - 1; i >= 0; i--)
                {
                    var request = inProgressRequests[i];
                    if (request.isDone)
                    {
                        if (request.error != null)
                        {
                            var errorText = "Failed to download bundle: " + request.url;
                            yield return throwError(AddressableErrorCode.API_ERROR, errorText);
                        }
                        else
                        {
                            DownloadHandlerAssetBundle downloadedBundle = (DownloadHandlerAssetBundle)request.downloadHandler;
                            if (downloadedBundle == null)
                            {
                                var errorText = "Failed to load bundle: " + request.url;
                                yield return throwError(AddressableErrorCode.BUNDLE_ERROR, errorText);
                            }
                            else
                            {
                                inProgressRequests.RemoveAt(i);
                                doneRequests.Add(request);
                                downloadHandles.Add(new RoveBundleDownloadHandle(request.url, downloadedBundle));
                            }
                        }
                    }
                    else
                    {
                        progress += request.downloadProgress;
                    }
                }
                progress += doneRequests.Count;
                loaderCallback?.invokeProgressEvent(progress / totalProgress);
                yield return null;
            }

            if (!isError)
            {
                loaderCallback?.invokeProgressEvent(1);
                loaderCallback?.invokeSuccessEvent(downloadHandles.ToArray());
            }
            disposeAllRequest();
        }
        private static void log(string str)
        {
            Debug.Log("[Load Addressable] " + str);
        }

        //public static DownloadAddressableOperator downloadBundles(string[] bundlePaths, uint version = 0u, uint crc = 0u)
        //{
        //    return null;
        //}

        public static AddressableLoaderOperation loadBundleAsync(byte[][] bundleData)
        {
            return AddressableLoaderOperation.startLoad(bundleData);
        }
        public static AddressableLoaderOperation loadBundleAsync(string[] bundlePaths)
        {
            return AddressableLoaderOperation.startLoad(bundlePaths);
        }
        public static AddressableLoaderOperation loadFromAFile(string localPath)
        {
            try
            {
                AddressableAFile addressableAFile = AddressableAFile.fromFile(localPath);
                var currentPlatform = RoveUtils.getCurrentPlatform();
                var data = addressableAFile.getFiles(currentPlatform);
                return AddressableLoaderOperation.startLoad(data);
            }
            catch (Exception e)
            {
                var operation = AddressableLoaderOperation.createErrorOperation(e.Message);
                return operation;
            }
        }
    }

    public class AddressableAFile
    {
        [Serializable]
        public class AddressableAFilePlatform
        {
            public long startIndex;
            public long length;
            [NonSerialized] public string localPath;
        }
        public int version = 1;
        public string FilePath { get; private set; }

        [SerializeField] private List<AddressableAFilePlatform> webgl = new List<AddressableAFilePlatform>();
        [SerializeField] private List<AddressableAFilePlatform> standalonewindows64 = new List<AddressableAFilePlatform>();
        [SerializeField] private List<AddressableAFilePlatform> standaloneosx = new List<AddressableAFilePlatform>();
        [SerializeField] private List<AddressableAFilePlatform> android = new List<AddressableAFilePlatform>();
        [SerializeField] private List<AddressableAFilePlatform> ios = new List<AddressableAFilePlatform>();

        public void addFile(string platform, string localPath)
        {
            platform = platform.ToLower();
            List<AddressableAFilePlatform> list = null;
            switch (platform)
            {
                case "webgl":
                    list = webgl;
                    break;
                case "standalonewindows64":
                    list = standalonewindows64;
                    break;
                case "standaloneosx":
                    list = standaloneosx;
                    break;
                case "android":
                    list = android;
                    break;
                case "ios":
                    list = ios;
                    break;
            }

            if (list != null)
            {
                list.Add(new AddressableAFilePlatform()
                {
                    localPath = localPath,
                });
            }
        }
        public string writeToFile(string sceneName, string dir)
        {
            try
            {
                var path = Path.Combine(dir, sceneName + "_allinone");
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                using (var fs = File.Create(path))
                {
                    long startIndex = 0;
                    fs.Seek(startIndex, SeekOrigin.Begin);

                    foreach (var f in webgl)
                    {
                        var b = File.ReadAllBytes(f.localPath);
                        f.startIndex = startIndex;
                        f.length = b.Length;
                        fs.Write(b, 0, b.Length);
                        startIndex += b.Length;
                    }

                    foreach (var f in standalonewindows64)
                    {
                        var b = File.ReadAllBytes(f.localPath);
                        f.startIndex = startIndex;
                        f.length = b.Length;
                        fs.Write(b, 0, b.Length);
                        startIndex += b.Length;
                    }

                    foreach (var f in standaloneosx)
                    {
                        var b = File.ReadAllBytes(f.localPath);
                        f.startIndex = startIndex;
                        f.length = b.Length;
                        fs.Write(b, 0, b.Length);
                        startIndex += b.Length;
                    }

                    foreach (var f in android)
                    {
                        var b = File.ReadAllBytes(f.localPath);
                        f.startIndex = startIndex;
                        f.length = b.Length;
                        fs.Write(b, 0, b.Length);
                        startIndex += b.Length;
                    }

                    foreach (var f in ios)
                    {
                        var b = File.ReadAllBytes(f.localPath);
                        f.startIndex = startIndex;
                        f.length = b.Length;
                        fs.Write(b, 0, b.Length);
                        startIndex += b.Length;
                    }

                    var completeJson = JsonUtility.ToJson(this);
                    var jsonByte = Encoding.UTF8.GetBytes(completeJson);
                    fs.Write(jsonByte, 0, jsonByte.Length);
                    startIndex += jsonByte.Length;
                    fs.Write(BitConverter.GetBytes(jsonByte.Length), 0, 4);
                    startIndex += 4;
                    return path;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public byte[][] getFiles(string platform)
        {
            platform = platform.ToLower();
            List<AddressableAFilePlatform> list = null;
            switch (platform)
            {
                case "webgl":
                    list = webgl;
                    break;
                case "standalonewindows64":
                    list = standalonewindows64;
                    break;
                case "standaloneosx":
                    list = standaloneosx;
                    break;
                case "android":
                    list = android;
                    break;
                case "ios":
                    list = ios;
                    break;
            }

            if (list == null || list.Count == 0)
            {
                return new byte[0][];
            }
            List<byte[]> outList = new List<byte[]>();
            using (FileStream fs = File.OpenRead(FilePath))
            {
                foreach (var item in list)
                {
                    fs.Seek(0, SeekOrigin.Begin);
                    fs.Seek(item.startIndex, SeekOrigin.Current);
                    var bytes = new byte[item.length];
                    fs.Read(bytes, 0, bytes.Length);
                    outList.Add(bytes);
                }
            }
            return outList.ToArray();
        }

        public static AddressableAFile fromFile(string filePath)
        {
            try
            {
                using (FileStream fs = File.OpenRead(filePath))
                {
                    var jsonLengthByte = new byte[4];
                    fs.Seek(fs.Length - 4, SeekOrigin.Current);
                    fs.Read(jsonLengthByte, 0, 4);
                    var jsonLength = BitConverter.ToInt32(jsonLengthByte, 0);
                    var jsonByte = new byte[jsonLength];
                    fs.Seek(-4 - jsonLength, SeekOrigin.Current);
                    fs.Read(jsonByte, 0, jsonByte.Length);
                    var json = Encoding.UTF8.GetString(jsonByte);
                    var addrAFile = JsonUtility.FromJson<AddressableAFile>(json);
                    addrAFile.FilePath = filePath;
                    return addrAFile;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
