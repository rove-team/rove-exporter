using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Rove.RoveExporter
{
    public class AddressableDownloadCallback : APIWebReuquestCallback<RoveBundleDownloadHandle[]>
    {

    }
    public class AddressableLoaderCallback : APIWebReuquestCallback<LoadedAddressableData>
    {
        public LoadedAddressableData loadedAdressableData;
        public void invokeSuccessEvent(RoveBundle[] bundles)
        {
            loadedAdressableData = new LoadedAddressableData(bundles);
            invokeSuccessEvent(loadedAdressableData);
        }
        public void unloadBundles(bool isUnLoadedAllLoadedObjects)
        {
            loadedAdressableData?.unload(isUnLoadedAllLoadedObjects);
        }
    }
}
