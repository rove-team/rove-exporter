using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public static class RoveExporterInfo
    {
        public const string EXPORT_VERSION = "1.2.2";
    }
}
