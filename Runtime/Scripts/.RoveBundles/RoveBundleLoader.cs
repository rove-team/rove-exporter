using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Rove.RoveExporter
{
    public class RoveBundleLoader
    {
        public static RoveBundleRequest loadBundle(string url, uint version = 0u, uint crc = 0u)
        {
            var request = UnityWebRequestAssetBundle.GetAssetBundle(url, version, crc);
            RoveBundleRequest bundleRequest = new RoveBundleRequest(request);
            return bundleRequest;
        }
        public static RoveBundleRequest loadBundle(byte[] bundleData)
        {
            var bundle = AssetBundle.LoadFromMemory(bundleData);
            RoveBundleRequest bundleRequest = new RoveBundleRequest(bundle);
            return bundleRequest;
        }
    }
}
