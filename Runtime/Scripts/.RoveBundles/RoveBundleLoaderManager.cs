using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class RoveBundleLoaderManager : MonoBehaviour
    {
        private const int MAX_LOAD_A_TIME = 4;
        public List<RoveBundleRequest> LoadedBundles { get; private set; } = new List<RoveBundleRequest>();
        public List<RoveBundleRequest> LoadingBundles { get; private set; } = new List<RoveBundleRequest>();
        public List<RoveBundleRequest> PendingBundles { get; private set; } = new List<RoveBundleRequest>();

        private void Update()
        {

        }

        private void checkLoading()
        {
            foreach (var request in LoadingBundles)
            {

            }
        }
        private void checkPending()
        {
            while (PendingBundles.Count > 0 && LoadingBundles.Count < MAX_LOAD_A_TIME)
            {
                var nextRequest = getNextRequest();
                if (nextRequest != null)
                {
                    nextRequest.sendRequest();
                    LoadingBundles.Add(nextRequest);
                }
            }
        }

        private RoveBundleRequest getNextRequest(bool isRemoveFromList = true)
        {
            int index = 0;
            RoveBundleRequest request = null;
            if (PendingBundles.Count > 0)
            {
                request = PendingBundles[index];
                if (isRemoveFromList)
                {
                    PendingBundles.RemoveAt(index);
                }
            }
            return request;
        }


        public RoveBundleRequest loadBundle(string url)
        {
            foreach (var r in LoadedBundles)
            {
                if (r.Url == url)
                {
                    return r;
                }
            }
            foreach (var r in LoadingBundles)
            {
                if (r.Url == url)
                {
                    return r;
                }
            }
            foreach (var r in PendingBundles)
            {
                if (r.Url == url)
                {
                    return r;
                }
            }

            var request = RoveBundleLoader.loadBundle(url);
            PendingBundles.Add(request);
            return request;
        }
        public void unloadBundle(string url)
        {
            foreach (var bundle in LoadedBundles)
            {
                if (bundle.Url == url)
                {
                    LoadedBundles.Remove(bundle);
                    bundle.RoveBundle.unload(true);
                }
            }
        }
        public void unloadBundle(RoveBundleRequest roveBundle)
        {
            roveBundle.RoveBundle.unload(true);
            if (LoadedBundles.Contains(roveBundle))
            {
                LoadedBundles.Remove(roveBundle);
            }
        }

    }
}
