using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace Rove.RoveExporter
{
    public abstract class RoveRequest : CustomYieldInstruction
    {
        public bool IsDone { get; protected set; }
        public bool IsError => !string.IsNullOrEmpty(ErrorMessage) && IsDone;
        public string ErrorMessage { get; protected set; }
        public abstract float Progress { get; }

        public override bool keepWaiting => !IsDone;
        public event Action onCompleted;
        public event Action<float> onProgress;
        internal void invokeCompleteEvent()
        {
            onCompleted?.Invoke();
        }
        internal void invokeProgress()
        {
            onProgress?.Invoke(Progress);
        }
    }

    public class RoveBundleData
    {
        public string Url { get; protected set; }
        public AssetBundle RoveBundle { get; protected set; }
        public RoveBundleData(string url, AssetBundle bundle)
        {
            Url = url;
            RoveBundle = bundle;
        }
        public void unload(bool unloadAllLoadedObjects)
        {
            RoveBundle.Unload(unloadAllLoadedObjects);
        }
    }

    public class RoveBundleRequest : RoveRequest
    {
        public UnityWebRequest WebRequest { get; protected set; }
        public RoveBundleData RoveBundle { get; protected set; }
        public string Url { get; protected set; }
        public override float Progress => WebRequest != null ? WebRequest.downloadProgress : 1;
        public RoveBundleRequest(AssetBundle assetBundle)
        {
            RoveBundle = new RoveBundleData("", assetBundle);
            IsDone = true;
        }
        public RoveBundleRequest(UnityWebRequest webRequest)
        {
            Url = webRequest.url;
        }
        public void sendRequest()
        {
            if (!IsDone && WebRequest != null)
            {
                var operation = WebRequest.SendWebRequest();
                operation.completed += RequestOperation_completed;
            }
        }
        private void RequestOperation_completed(AsyncOperation operation)
        {
            if (WebRequest.isDone)
            {
                IsDone = true;
                ErrorMessage = WebRequest.error;
                if (string.IsNullOrEmpty(WebRequest.error))
                {
                    DownloadHandlerAssetBundle downloadedBundle = (DownloadHandlerAssetBundle)WebRequest.downloadHandler;
                    RoveBundle = new RoveBundleData(WebRequest.url, downloadedBundle.assetBundle);
                }
                invokeCompleteEvent();
                WebRequest.Dispose();
            }
        }

    }
}
