using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Rove.RoveExporter
{
    public static class RoveDebug
    {
        public static Action<LogInfo> onLogEvent;
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void log(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.Log($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);

            LogInfo logInfo = new LogInfo(logData, LogType.Log, memberName, lineNumber, fileName);
            invokeLogEvent(logInfo);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void logWarning(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.LogWarning($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);

            LogInfo logInfo = new LogInfo(logData, LogType.Warning, memberName, lineNumber, fileName);
            invokeLogEvent(logInfo);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void logError(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.LogError($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);

            LogInfo logInfo = new LogInfo(logData, LogType.Error, memberName, lineNumber, fileName);
            invokeLogEvent(logInfo);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void logException(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.LogError($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);

            LogInfo logInfo = new LogInfo(logData, LogType.Exception, memberName, lineNumber, fileName);
            invokeLogEvent(logInfo);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void invokeLogEvent(LogInfo logInfo)
        {
            try
            {
                onLogEvent?.Invoke(logInfo);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void logOnly(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.Log($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void logWarningOnly(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.LogWarning($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);
        }
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void logErrorOnly(object logData, Object context = null, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var fileName = Path.GetFileNameWithoutExtension(file);
            Debug.LogError($"[{fileName} - {lineNumber} - {memberName}()] {logData}", context);
        }
    }

    public class LogInfo
    {
        public LogInfo(object message, LogType logType, string callerName, int lineNumber, string fileName)
        {
            this.message = message;
            this.logType = logType;
            this.callerMemberName = callerName;
            this.lineNumber = lineNumber;
            this.fileName = fileName;
        }
        public object message;
        public LogType logType;
        public string callerMemberName;
        public int lineNumber;
        public string fileName;
    }
}
