using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Rove.RoveExporter
{
    public static partial class RoveUtils
    {
        public const string PLATFORM_WIN64 = "standalonewindows64";
        public const string PLATFORM_OSX = "standaloneosx";
        public const string PLATFORM_WEBGL = "webgl";
        public const string PLATFORM_ANDROID = "android";
        public const string PLATFORM_IOS = "ios";


        public static string createMD5(this string input)
        {
            if (input == null)
            {
                return "";
            }
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            return createMD5(inputBytes);
        }
        public static string createMD5(byte[] inData)
        {
            if (inData == null || inData.Length == 0)
            {
                return "";
            }
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] hashBytes = md5.ComputeHash(inData);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
        internal static int generateNextUid(Type type)
        {
            var arrays = GameObject.FindObjectsOfType(type, true);
            List<RoveUid> list = new List<RoveUid>();
            foreach (var a in arrays)
            {
                var go = a as RoveUid;
                if (go != null)
                {
                    var rUid = go.GetComponent<RoveUid>();
                    if (rUid != null)
                    {
                        list.Add(rUid);
                    }
                }
            }
            var nId = getNextId(list);
            return nId;
        }
        private static int getNextId(List<RoveUid> list)
        {
            List<int> idUseds = new List<int>();
            foreach (var a in list)
            {
                if (!idUseds.Contains(a.Uid))
                {
                    idUseds.Add(a.Uid);
                }
            }

            int nId = 1;

            while (idUseds.Contains(nId))
            {
                nId++;
            }
            return nId;
        }
        public static bool isInViewPort(this Transform inTransform, Camera inCamera)
        {
            var cam = inCamera;
            var viewport = cam.WorldToViewportPoint(inTransform.position);
            var isInViewport = viewport.x < 1 && viewport.x > 0 && viewport.y > 0 && viewport.y < 1;
            return isInViewport;
        }
        public static bool isInViewPortWithZ(this Transform inTransform, Camera inCamera, float farestDistance = float.PositiveInfinity)
        {
            var cam = inCamera;
            var viewport = cam.WorldToViewportPoint(inTransform.position);
            var isInViewport = viewport.x < 1 && viewport.x > 0 && viewport.y > 0 && viewport.y < 1 && viewport.z >= 0 && viewport.z <= farestDistance;
            return isInViewport;
        }
        /// <summary>
        /// Only clone serializable or cloneable object
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <returns></returns>
        public static T clone<T>(T source)
        {
            if (source == null)
            {
                return default(T);
            }
            if (typeof(T).IsSerializable)
            {
                var json = JsonUtility.ToJson(source);
                return JsonUtility.FromJson<T>(json);
            }
            else
            {
                var cloneable = source as ICloneable;
                if (cloneable != null)
                {
                    return (T)cloneable.Clone();
                }
            }
            return default(T);
        }
        public static T copyComponent<T>(T original, GameObject destination) where T : Component
        {
            System.Type type = original.GetType();
            var dst = destination.GetComponent(type) as T;
            if (!dst) dst = destination.AddComponent(type) as T;
            var fields = type.GetFields();
            foreach (var field in fields)
            {
                if (field.IsStatic) continue;
                field.SetValue(dst, field.GetValue(original));
            }
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name") continue;
                prop.SetValue(dst, prop.GetValue(original, null), null);
            }
            return dst as T;
        }
        public static void copyComponent(Component original, Component destination)
        {
            Type type = original.GetType();
         
            if (type != destination.GetType())
            {
                throw new InvalidOperationException("Type is not match");
            }
            
            var fields = type.GetFields();

            foreach (var field in fields)
            {
                if (field.IsStatic) continue;
                field.SetValue(destination, field.GetValue(original));
            }
            var props = type.GetProperties();
            foreach (var prop in props)
            {
                if (!prop.CanWrite || !prop.CanWrite || prop.Name == "name") continue;
                prop.SetValue(destination, prop.GetValue(original, null), null);
            }
        }
        public static void copyAllComponents(GameObject original, GameObject destination, params Type[] copyTypes)
        {
            Component[] components = original.GetComponents<Component>();

            Dictionary<Type, List<Component>> comTable = new Dictionary<Type, List<Component>>();
            foreach (var component in components)
            {
                var type = component.GetType();
                if (!copyTypes.Contains(type))
                {
                    continue;
                }
                if (comTable.ContainsKey(type))
                {
                    comTable[type].Add(component);
                }
                else
                {
                    comTable.Add(type, new List<Component>() { component });
                }
            }

            foreach (var com in comTable)
            {
                var countInOrigin = original.GetComponents(com.Key).Length;
                var countInDst = destination.GetComponents(com.Key).Length;
                while (countInDst < countInOrigin)
                {
                    destination.AddComponent(com.Key);
                    countInDst = destination.GetComponents(com.Key).Length;
                }
                var comInDst = destination.GetComponents(com.Key);
                for (int i = 0; i < com.Value.Count; i++)
                {
                    copyComponent(com.Value[i], comInDst[i]);
                }
            }
        }
        public static T getOrAddComponent<T>(GameObject target) where T : Component
        {
            var com = target.GetComponent<T>();
            if (!com)
            {
                com = target.AddComponent<T>();
            }
            return com;
        }

        public static void destroy(UnityEngine.Object component, bool isImmediate = false)
        {
            if (!component)
            {
                return;
            }
            if (!Application.isPlaying || isImmediate)
            {
                UnityEngine.Object.DestroyImmediate(component);
            }
            else
            {
                UnityEngine.Object.Destroy(component);
            }
        }

        public static bool isChildOf(this Transform target, Transform parent)
        {
            if (target.parent == null)
            {
                return false;
            }
            if (target.parent == parent)
            {
                return true;
            }
            return target.parent.isChildOf(parent);
        }
        public static Transform[] getTransformInPattern(this Transform root)
        {
            var trans = new List<Transform>(root.GetComponentsInChildren<Transform>(true));
            for (int i = trans.Count - 1; i >= 0; i--)
            {
                if (trans[i].GetComponent<IgnorePattern>())
                {
                    trans.RemoveAt(i);
                }
            }
            return trans.ToArray();
        }
        public static T[] getComponentInPattern<T>(this GameObject go) where T : Component 
        {
            var coms = new List<T>(go.GetComponentsInChildren<T>(true));
            for (int i = coms.Count - 1; i >= 0; i--)
            {
                if (coms[i].GetComponent<IgnorePattern>())
                {
                    coms.RemoveAt(i);
                }
            }
            return coms.ToArray();
        }
        public static GameObject findObjectPattern(GameObject inSrc, GameObject pattern, bool isChildOnly)
        {
            var trans = inSrc.transform.getTransformInPattern();
            for (int i = 0; i < trans.Length; i++)
            {
                var tran = trans[i];
                if (isEqualPattern(tran.gameObject, pattern, isChildOnly))
                {
                    return tran.gameObject;
                }
            }
            return null;
        }
        public static GameObject[] findObjectWithName(GameObject inSrc, string name)
        {
            HashSet<GameObject> result = new HashSet<GameObject>();
            var trans = inSrc.GetComponentsInChildren<Transform>(true);
            foreach (var tran in trans)
            {
                result.Add(tran.gameObject);
            }
            return result.ToArray();
        }
        public static bool isEqualPattern(GameObject lhs, GameObject rhs, bool isChildOnly)
        {
            var lTrans = lhs.transform.getTransformInPattern();
            var rTrans = rhs.transform.getTransformInPattern();

            if (lTrans.Length != rTrans.Length)
            {
                return false;
            }
            int startIndex = isChildOnly ? 1 : 0;
            for (int i = startIndex; i < lTrans.Length; i++)
            {
                if (lTrans[i].name != rTrans[i].name)
                {
                    return false;
                }
            }
            return true;
        }
        public static bool isInScene(this GameObject go)
        {
            return go.scene != null && !string.IsNullOrEmpty(go.gameObject.scene.path);
        }
        public static bool isInScene(this Component component)
        {
            return isInScene(component.gameObject);
        }
        public static string getCurrentPlatform()
        {
#if UNITY_EDITOR && UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            return PLATFORM_WIN64;
#elif UNITY_EDITOR && UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
            return PLATFORM_OSX;
#elif UNITY_WEBGL
            return PLATFORM_WEBGL;
#elif UNITY_ANDROID
            return PLATFORM_ANDROID;
#elif UNITY_IOS
            return PLATFORM_IOS;
#endif
        }
    }

    public class RInRangeChecker
    {
        private enum BoolLogic
        {
            AND,
            OR
        } 
        public struct Result
        {
            public Vector3 viewportPosition;
        }
        public bool? IsInRange { get; private set; }
        public Camera Camera { get; private set; }

        private Transform _Target;
        private Vector3 _TempViewport;

        List<Func<bool>> _LogicList = new List<Func<bool>>();

        public static RInRangeChecker DefaultConfig => new RInRangeChecker().setCamera(Camera.main).andInX(0, 1).andInY(0, 1).andInZ(0, float.PositiveInfinity);
        public RInRangeChecker setCamera(Camera inCamera)
        {
            Camera = inCamera;
            return this;
        }
        private Func<bool> getLogic(Func<bool> func, BoolLogic boolLogic)
        {
            Func<bool> logic1 = () =>
            {
                if (IsInRange.HasValue)
                {
                    switch (boolLogic)
                    {
                        case BoolLogic.AND:
                            IsInRange = IsInRange.Value & func();
                            break;
                        case BoolLogic.OR:
                            IsInRange = IsInRange.Value | func();
                            break;
                    }
                    return IsInRange.Value;
                }
                IsInRange = func();
                return IsInRange.Value;
            };
            return logic1;
        }
        
        public RInRangeChecker andInX(float from, float to)
        {
            var logic = getLogic(() =>
            {
                return _TempViewport.x >= from && _TempViewport.x <= to;
            }, BoolLogic.AND);
            
            _LogicList.Add(logic);
            return this;
        }
        public RInRangeChecker andInY(float from, float to)
        {
            var logic = getLogic(() =>
            {
                return _TempViewport.y >= from && _TempViewport.y <= to;
            }, BoolLogic.AND);

            _LogicList.Add(logic);
            return this;
        }
        public RInRangeChecker andInZ(float from, float to)
        {
            var logic = getLogic(() =>
            {
                return _TempViewport.z >= from && _TempViewport.z <= to;
            }, BoolLogic.AND);

            _LogicList.Add(logic);
            return this;
        }
        public RInRangeChecker andInAngle(float from, float to)
        {
            var logic = getLogic(() =>
            {
                var angles = Quaternion.Angle(Camera.transform.rotation, _Target.rotation);
                return angles >= from && angles <= to;
            }, BoolLogic.AND);

            _LogicList.Add(logic);
            return this;
        }
        public RInRangeChecker orInRange(float from, float to)
        {
            var logic = getLogic(() =>
            {
                var distance = Vector3.Distance(Camera.transform.position, _Target.position);
                return distance >= from && distance <= to;
            }, BoolLogic.OR);

            _LogicList.Add(logic);
            return this;
        }
        public RInRangeChecker andInRange(float from, float to)
        {
            var logic = getLogic(() =>
            {
                var distance = Vector3.Distance(Camera.transform.position, _Target.position);
                return distance >= from && distance <= to;
            }, BoolLogic.AND);

            _LogicList.Add(logic);
            return this;
        }
        public bool check(Transform target)
        {
            _Target = target;
            if (Camera == null)
            {
                return false;
            }
            if (_Target == null)
            {
                return false;
            }
            if (_LogicList.Count == 0)
            {
                return false;
            }

            _TempViewport = Camera.WorldToViewportPoint(_Target.position);

            IsInRange = null;
            for (int i = 0; i < _LogicList.Count; i++)
            {
                _LogicList[i].Invoke();
            }
            return IsInRange ?? false;
        }
        public bool check(Transform target, out Result result)
        {
            var isInRange = check(target);
            result = new Result();
            result.viewportPosition = _TempViewport;
            return isInRange;
        }
    }
}
