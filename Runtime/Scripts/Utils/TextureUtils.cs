using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace Rove.RoveExporter
{
    public static class TextureUtils
    {
        const int DEPTH_BUFFER = 24;
        public static Texture2D getFromTexture(Texture texture)
        {
            Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
            RenderTexture currentRT = RenderTexture.active;
            RenderTexture renderTexture = RenderTexture.GetTemporary(texture.width, texture.height, DEPTH_BUFFER);
            Graphics.Blit(texture, renderTexture);

            RenderTexture.active = renderTexture;
            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);

            RenderTexture.active = currentRT;
            RenderTexture.ReleaseTemporary(renderTexture);
            return texture2D;
        }

        public static Texture2D getFromTexture(Texture texture, int w, int h)
        {
            Texture2D texture2D = new Texture2D(w, h, TextureFormat.RGBA32, false);
            RenderTexture currentRT = RenderTexture.active;
            RenderTexture renderTexture = RenderTexture.GetTemporary(texture2D.width, texture2D.height, DEPTH_BUFFER);
            Graphics.Blit(texture, renderTexture);

            RenderTexture.active = renderTexture;
            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);

            RenderTexture.active = currentRT;
            RenderTexture.ReleaseTemporary(renderTexture);
            return texture2D;
        }

        public static Texture2D getFromRenderTexture(RenderTexture renderTexture, int w, int h)
        {
            Texture2D texture2D = new Texture2D(w, h, TextureFormat.RGBA32, false);
            RenderTexture currentRT = RenderTexture.active;
            RenderTexture.active = renderTexture;
            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            RenderTexture.active = currentRT;
            return texture2D;
        }

        public static Texture2D getFromCamera(Camera camera, int w, int h, bool forceRender = false)
        {
            Texture2D texture2D;
            if (camera.targetTexture == null)
            {
                camera.targetTexture = RenderTexture.GetTemporary(w, h, DEPTH_BUFFER);
                camera.Render();
                texture2D = getFromRenderTexture(camera.targetTexture, w, h);
                RenderTexture.ReleaseTemporary(camera.targetTexture);
                camera.targetTexture = null;
            }
            else
            {
                if (forceRender)
                {
                    camera.Render();
                }
                texture2D = getFromRenderTexture(camera.targetTexture, w, h);
            }
            return texture2D;
        }

        public static bool checkIsImage(byte[] data)
        {
            var stream = new MemoryStream(data);
            stream.Seek(0, SeekOrigin.Begin);

            List<string> jpg = new List<string> { "FF", "D8" };
            List<string> bmp = new List<string> { "42", "4D" };
            List<string> gif = new List<string> { "47", "49", "46" };
            List<string> png = new List<string> { "89", "50", "4E", "47", "0D", "0A", "1A", "0A" };
            List<List<string>> imgTypes = new List<List<string>> { jpg, bmp, /*gif,*/ png };
            //TODO: implement GIF render

            List<string> bytesIterated = new List<string>();

            for (int i = 0; i < 8; i++)
            {
                string bit = stream.ReadByte().ToString("X2");
                bytesIterated.Add(bit);

                bool isImage = imgTypes.Any(img => !img.Except(bytesIterated).Any());
                if (isImage)
                {
                    return true;
                }
            }

            return false;
        }
        public static string getImageExtension(byte[] data)
        {
            var stream = new MemoryStream(data);
            stream.Seek(0, SeekOrigin.Begin);

            List<string> jpg = new List<string> { "FF", "D8" };
            List<string> bmp = new List<string> { "42", "4D" };
            List<string> gif = new List<string> { "47", "49", "46" };
            List<string> png = new List<string> { "89", "50", "4E", "47", "0D", "0A", "1A", "0A" };
            Dictionary<string,List<string>> imgTypes = new Dictionary<string, List<string>> {
                { "jpg", jpg },
                { "bmp", bmp },
                {"gif", gif },
                {"png", png }};
            //TODO: implement GIF render

            List<string> bytesIterated = new List<string>();

            for (int i = 0; i < 8; i++)
            {
                string bit = stream.ReadByte().ToString("X2");
                bytesIterated.Add(bit);

                foreach (var type in imgTypes)
                {
                    if (!type.Value.Except(bytesIterated).Any())
                    {
                        return type.Key;
                    }
                }
            }

            return "";
        }
    }
}