using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class DataObserver<T> where T : class
    {
        public DataObserver()
        {
            IsSupported = isSupported();
            if (!IsSupported)
            {
                warningNotSupport();
            }
        }

        protected T _Source;
        protected T _PersistentSource;
        protected List<DataObserverWatcher<T>> _Watchers = new List<DataObserverWatcher<T>>();

        internal T Source => _Source;
        public bool IsSupported { get; private set; }

        public void setData(T inData, bool isNotify = true)
        {
            if (!IsSupported)
            {
                warningNotSupport();
                return;
            }
            if (inData == _Source)
            {
                return;
            }
            if (isNotify)
            {
                runWatchFuncs(_Source, inData);
            }
            _Source = inData;
            _PersistentSource = null;
        }
        public T getData()
        {
            if (_PersistentSource == null && _Source != null)
            {
                _PersistentSource = RoveUtils.clone(_Source);
            }
            return _PersistentSource;
        }
        public void clearPersistent()
        {
            _PersistentSource = null;
        }
        public DataObserverWatcher<T> watch(Func<T, T, bool> func)
        {
            var watcher = new DataObserverWatcher<T>();
            watcher.setWatchFunc(func, this);
            _Watchers.Add(watcher);
            return watcher;
        }
        public void unwatch(DataObserverWatcher<T> watcher)
        {
            var idx = _Watchers.IndexOf(watcher);
            if (idx >= 0)
            {
                _Watchers.RemoveAt(idx);
            }
        }
        private void runWatchFuncs(T inData0, T inData1)
        {
            foreach (var f in _Watchers)
            {
                if (f != null)
                {
                    if (f.IsWatching)
                    {
                        f.trackDiff(inData0, inData1);
                    }
                }
            }
        }
        private bool isSupported()
        {
            if (typeof(T).IsSerializable)
            {
                return true;
            }

            var interfaces = typeof(T).GetInterfaces();
            var cloneableType = typeof(ICloneable);
            foreach (var i in interfaces)
            {
                if (i == cloneableType)
                {
                    return true;
                }
            }
            return false;
        }
        private void warningNotSupport()
        {
            Debug.Log($"[DataObserver] {typeof(T).Name} is not supported, data always return null");
        }

    }
    public class DataObserverWatcher<T> where T : class
    {
        protected Func<T, T, bool> _WatchFunc;
        protected event Action onValueChangedEvent;
        protected DataObserver<T> _Observer;
        internal DataObserverWatcher<T> setWatchFunc(Func<T, T, bool> func, DataObserver<T> observer)
        {
            _WatchFunc = func;
            _Observer = observer;
            return this;
        }
        internal void trackDiff(T inData0, T inData1)
        {
            if (_WatchFunc != null)
            {
                bool haveChanged = false;
                if (inData0 != inData1)
                {
                    if (inData0 == null || inData1 == null)
                    {
                        haveChanged = true;
                    }
                    else
                    {
                        haveChanged = _WatchFunc.Invoke(inData0, inData1);
                    }
                    if (haveChanged)
                    {
                        onValueChangedEvent?.Invoke();
                    }
                }
            }
        }
        public DataObserverWatcher<T> addChangedEvent(Action onChanged)
        {
            onValueChangedEvent += onChanged;
            return this;
        }
        public bool IsWatching { get; protected set; } = true;
        public void pause()
        {
            IsWatching = false;
        }
        public void resume()
        {
            IsWatching = true;
        }
    }

    [Serializable]
    internal class SampleSupportedData : SampleData
    {
        public int abc;
    }
    internal class SampleNotSupportedData : SampleData
    {

    }
    internal class SampleCloneableData : SampleData, ICloneable
    {
        public object Clone()
        {
            return new SampleCloneableData()
            {
                testData = testData,
                subData = new SubSampleData()
                {
                    subData = subData.subData
                }
            };
        }
    }
    internal class SubSampleData
    {
        public int subData;
    }

    [Serializable]
    internal abstract class SampleData
    {
        public int testData;
        public SubSampleData subData = new SubSampleData();
    }
    internal class DataObserverTestRunner
    {
        public static void test()
        {
            DataObserver<SampleSupportedData> observer0 = new DataObserver<SampleSupportedData>();
            DataObserver<SampleNotSupportedData> observer1 = new DataObserver<SampleNotSupportedData>();
            DataObserver<SampleCloneableData> observer2 = new DataObserver<SampleCloneableData>();

            var watcher0 = observer0.watch((a, b) =>
            {
                return !a.testData.Equals(b.testData);
            }).addChangedEvent(() =>
            {
                Debug.Log("[DataObserverTestRunner] observer0.testData was changed");
            });

            var watcher1 = observer0.watch((a, b) =>
            {
                return !a.subData.Equals(b.subData);
            }).addChangedEvent(() =>
            {
                Debug.Log("[DataObserverTestRunner] observer0.testData was changed");
            });

            watcher0.pause();
            watcher1.pause();

            var supportedData = observer0.getData();
            if (supportedData == null)
            {
                supportedData = new SampleSupportedData();
            }
            observer0.setData(supportedData);

            //watcher0.resume();
            watcher1.resume();

            //supportedData = observer0.getData();
            //if (supportedData != null)
            //{
            //    supportedData.testData++;
            //    Debug.Log($"RootData.testData: {observer0.Source.testData}, CloneData.testData: {supportedData.testData}");
            //    supportedData.subData.subData++;
            //    Debug.Log($"RootData.subData: {observer0.Source.subData.subData}, CloneData.subData: {supportedData.subData.subData}");
            //    observer0.setData(supportedData);
            //    Debug.Log($"RootData.testData: {observer0.Source.testData}, CloneData.testData: {supportedData.testData}");
            //    Debug.Log($"RootData.subData: {observer0.Source.subData.subData}, CloneData.subData: {supportedData.subData.subData}");
            //}

            //supportedData = observer0.getData();
            //if (supportedData != null)
            //{
            //    supportedData.subData = new SubSampleData();
            //    observer0.setData(supportedData);
            //}

            supportedData = observer0.getData();
            supportedData = null;
            observer0.setData(supportedData);
            observer0.setData(supportedData);
            observer0.setData(supportedData);
            observer0.setData(supportedData);
            observer0.setData(supportedData);
            observer0.setData(supportedData);
            observer0.setData(new SampleSupportedData());
            observer0.setData(new SampleSupportedData());
            observer0.setData(new SampleSupportedData());
            observer0.setData(new SampleSupportedData());
            observer0.setData(new SampleSupportedData());
            observer0.setData(new SampleSupportedData());
            observer0.setData(new SampleSupportedData());
        }
    }
}
