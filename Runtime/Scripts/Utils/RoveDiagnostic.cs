using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Rove.Dainogstic
{
    public class RoveStopwatch
    {
        public static RoveStopwatch startNew(string trackName = "newTrack")
        {
            var stopwatch = new RoveStopwatch(trackName);
            return stopwatch;
        }
        private RoveStopwatch(string trackName)
        {
            TrackName = trackName;
            Laps = new List<LapInfo>();
            _stopwatch = Stopwatch.StartNew();
        }

        private Stopwatch _stopwatch;
        public string TrackName { get; private set; }
        public List<LapInfo> Laps { get; private set; }
        public bool IsRunning => _stopwatch.IsRunning;

        public LapInfo lap(string message = "", GameObject context = null, bool isLog = true, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var ticks = _stopwatch.ElapsedTicks;
            var lapInfo = new LapInfo()
            {
                lapIndex = Laps.Count + 1,
                message = message,
                elapsedTicks = ticks,
            };
            if (isLog)
            {
                RoveDebug.log($"[LAP-{lapInfo.lapIndex}] - {lapInfo.ElapsedTicks} ticks: \"{message}\"", context, memberName, lineNumber, file);
            }
            Laps.Add(lapInfo);
            _stopwatch.Restart();
            return lapInfo;
        }
        public LapInfo stop(string message = "", GameObject context = null, bool isLog = true, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            var l = lap(message, context, isLog, memberName, lineNumber, file);
            _stopwatch.Reset();

            return l;
        }
        public void restart()
        {
            Laps.Clear();
            _stopwatch.Restart();
        }

        /// <summary>
        /// get elapsed ticks from start to lasted lap
        /// </summary>
        public long ElapsedTicks
        {
            get
            {
                long total = 0;
                foreach (var lap in Laps)
                {
                    total += lap.elapsedTicks;
                }
                return total;
            }
        }
        /// <summary>
        /// get elapsed milliseconds from start to lasted lap
        /// </summary>
        public long ElapsedMs => ElapsedTicks / 10000;
    }
    public struct LapInfo
    {
        internal int lapIndex;
        internal string message;
        internal long elapsedTicks;

        public int LapIndex => lapIndex;
        public string Message => message;
        public long ElapsedTicks => elapsedTicks;
        public long ElapsedMs => elapsedTicks / 10000;
    }
}
