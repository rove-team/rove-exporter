using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Rove.RoveExporter
{
    public abstract class RoveOperation : CustomYieldInstruction
    {
        private Action<float> onProgressEvent;
        private Action onCompleteEvent;

        public override bool keepWaiting => !IsDone;
        public bool IsDone {get; private set;}
        public string ErrorMessage {get; private set;}
        public bool IsError => HasError && IsDone;
        public float Progress {get; private set;}

        private bool HasError => !string.IsNullOrEmpty(ErrorMessage);
        private void setError(string errMsg)
        {
            if (IsDone)
            {
                Debug.LogWarning("Can't set error when operation has complete");
                return;
            }
            ErrorMessage = errMsg;
            Debug.Log($"[{GetType().Name}] has error: {ErrorMessage}");
        }
        private void setProgress(float progress)
        {
            if (IsDone)
            {
                Debug.LogWarning("Can't set progress when operation has complete");
                return;
            }
            if (Progress != progress)
            {
                Progress = progress;
                try
                {
                    onProgressEvent?.Invoke(Progress);
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
        }
        private void invokeCompleteEvent()
        {
            if (IsDone)
            {
                Debug.LogWarning("Can't invoke complete when operation has complete");
                return;
            }
	        setProgress(1);
            IsDone = true;
            try
            {
                onCompleteEvent?.Invoke();
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
            onCompleteEvent = null;
        }

        protected void inheritedSetError(string errorMsg, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            setError($"{errorMsg} at {file}, method={memberName}(), line={lineNumber}");
        }
        protected void inheritedSetErrorAndComplete(string errorMsg, [CallerMemberName] string memberName = "", [CallerLineNumber] int lineNumber = 0, [CallerFilePath] string file = "")
        {
            inheritedSetError(errorMsg, memberName, lineNumber, file);
            inheritedInvokeComplete();
        }
        protected void inheritedSetProgress(float progress)
        {
            setProgress(progress);
        }
        protected void inheritedInvokeComplete()
        {
            invokeCompleteEvent();
        }

        public void addCompleteCallback(Action completeCallback)
        {
            if (IsDone)
            {
                try
                {
                    completeCallback?.Invoke();
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            else
            {
                onCompleteEvent += completeCallback;
            }
        }
        public void addProgressCallback(Action<float> progressCallback)
        {
            onProgressEvent += progressCallback;
        }
        public void removeCompleteCallback(Action completeCallback)
        {
            onCompleteEvent -= completeCallback;
        }
        public void removeProgressCallback(Action<float> progressCallback)
        {
            onProgressEvent -= progressCallback;
        }
    }
}
