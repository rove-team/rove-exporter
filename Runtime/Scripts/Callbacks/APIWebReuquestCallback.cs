using System;
using UnityEngine;
using UnityEngine.Events;

namespace Rove.RoveExporter
{
    public enum AddressableErrorCode
    {
        NONE,
        API_ERROR,
        BUNDLE_ERROR,
        NOT_SCENE
    }
    public class APIWebReuquestCallback<T>
    {
        public UnityEvent<float> onProgressEvent = new UnityEvent<float>();
        public UnityEvent<T> onSuccessEvent = new UnityEvent<T>();
        public UnityEvent<AddressableErrorCode, string> onErrorEvent = new UnityEvent<AddressableErrorCode, string>();

        public bool IsDone { get; private set; }
        public bool IsError => IsDone && !string.IsNullOrEmpty(ErrorMessage);
        public string ErrorMessage { get; private set; }
        public AddressableErrorCode ErrorCode { get; private set; }

        public virtual void invokeProgressEvent(float progress)
        {
            try
            {
                onProgressEvent?.Invoke(progress);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        public virtual void invokeSuccessEvent(T result)
        {
            if (IsDone)
            {
                Debug.LogWarning("Can't invokeSuccessEvent when operation has complete");
                return;
            }
            IsDone = true;
            ErrorMessage = "";
            ErrorCode = AddressableErrorCode.NONE;
            try
            {
                onSuccessEvent?.Invoke(result);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        public virtual void invokeErrorEvent(AddressableErrorCode errorCode, string errorMsg)
        {
            if (IsDone)
            {
                Debug.LogWarning("Can't invokeErrorEvent when operation has complete");
                return;
            }
            IsDone = true;
            ErrorMessage = errorMsg;
            ErrorCode = errorCode;
            try
            {
                onErrorEvent?.Invoke(errorCode, errorMsg);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
    }
}
