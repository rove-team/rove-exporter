using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public interface ILocalOnlyChecker
    {
        public bool IsLocalOnly { get; }
    }
}
