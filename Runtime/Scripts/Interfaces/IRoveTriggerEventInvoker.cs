using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public enum RoveTriggerEventType
    {
        OnEnter,
        OnExit,
        OnInteracted
        //OnStay,
    }
    public interface IRoveTriggerEventInvoker
    {
        public RoveTriggerEventType EventType { get; set; }
        public void invoke(RoveTriggerEventType eventType);
    }
}
