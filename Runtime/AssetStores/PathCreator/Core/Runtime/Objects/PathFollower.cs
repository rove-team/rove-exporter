﻿using UnityEngine;

namespace PathCreation
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public PathCreator pathCreator;
        public EndOfPathInstruction endOfPathInstruction;
        public float speed = 5;
        float distanceTravelled;
        public bool isAuto = false;

        private Vector3? _targetPosition;
        private Quaternion _targetRotation;

        void Start() {
            if (pathCreator != null)
            {
                // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
                pathCreator.pathUpdated += OnPathChanged;
            }
        }

#if !ROVE_DEVELOPER
        void Update()
        {
            if (pathCreator != null)
            {
                var scrollValue = Input.mouseScrollDelta.y;
                if (scrollValue != 0 || isAuto)
                {
                    var v = scrollValue > 0 ? 1 : -1;
                    if (isAuto)
                    {
                        v = 1;
                    }
                    distanceTravelled += speed * Time.deltaTime;
                    _targetPosition = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction);
                    _targetRotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction);
                }
            }

            if (_targetPosition.HasValue)
            {
                transform.position = Vector3.Lerp(transform.position, _targetPosition.Value, Time.deltaTime * 2);
                transform.rotation = Quaternion.Lerp(transform.rotation, _targetRotation, Time.deltaTime * 2);
            }
        }
#endif

        // If the path changes during the game, update the distance travelled so that the follower's position on the new path
        // is as close as possible to its position on the old path
        void OnPathChanged() {
            distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
        }
    }
}