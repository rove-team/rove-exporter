﻿using UnityEngine;

namespace PathCreation {
    public class MinMax3D {

        private Vector3 _min;
        private Vector3 _max;
        public Vector3 Min => _min;
        public Vector3 Max => _max;

        public MinMax3D()
        {
            _min = Vector3.positiveInfinity;
            _max = Vector3.negativeInfinity;
        }

        public void AddValue(Vector3 v)
        {
            addMin(v);
            addMax(v);
        }
        public void addMin(Vector3 v)
        {
            _min.x = Mathf.Min(Min.x, v.x);
            _min.y = Mathf.Min(Min.y, v.y);
            _min.z = Mathf.Min(Min.z, v.z);
        }
        public void addMax(Vector3 v)
        {
            _max.x = Mathf.Max(Max.x, v.x);
            _max.y = Mathf.Max(Max.y, v.y);
            _max.z = Mathf.Max(Max.z, v.z);
        }
    }
}