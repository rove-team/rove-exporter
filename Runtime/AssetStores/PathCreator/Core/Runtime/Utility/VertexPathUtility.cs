﻿using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace PathCreation.Utility
{
    public static class VertexPathUtility
    {

        public static PathSplitData SplitBezierPathByAngleError(BezierPath bezierPath, float maxAngleError, float minVertexDst, float accuracy)
        {
            return splitBezierPathByAngleError_UseJob(bezierPath, maxAngleError, minVertexDst, accuracy);
            //PathSplitData splitData = new PathSplitData();

            //splitData.vertices.Add(bezierPath[0]);
            //splitData.tangents.Add(CubicBezierUtility.EvaluateCurveDerivative(bezierPath.GetPointsInSegment(0), 0).normalized);
            //splitData.cumulativeLength.Add(0);
            //splitData.anchorVertexMap.Add(0);
            //splitData.minMax.AddValue(bezierPath[0]);

            //Vector3 prevPointOnPath = bezierPath[0];
            //Vector3 lastAddedPoint = bezierPath[0];

            //float currentPathLength = 0;
            //float dstSinceLastVertex = 0;

            //// Go through all segments and split up into vertices
            //for (int segmentIndex = 0; segmentIndex < bezierPath.NumSegments; segmentIndex++)
            //{
            //    Vector3[] segmentPoints = bezierPath.GetPointsInSegment(segmentIndex);
            //    float estimatedSegmentLength = CubicBezierUtility.EstimateCurveLength(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3]);
            //    int divisions = Mathf.CeilToInt(estimatedSegmentLength * accuracy);
            //    float increment = 1f / divisions;

            //    Vector3 pointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints, 0);
            //    for (float t = increment; t <= 1; t += increment)
            //    {
            //        bool isLastPointOnPath = (t + increment > 1 && segmentIndex == bezierPath.NumSegments - 1);
            //        if (isLastPointOnPath)
            //        {
            //            t = 1;
            //            pointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints, t);
            //        }
            //        Vector3 nextPointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints, t + increment);

            //        var prevSubPoint = prevPointOnPath - pointOnPath;
            //        var nextSubPoint = nextPointOnPath - pointOnPath;
            //        var lastSubPoint = lastAddedPoint - pointOnPath;

            //        // angle at current point on path
            //        float localAngle = 180 - Vector3.Angle(prevSubPoint, nextSubPoint);
            //        // angle between the last added vertex, the current point on the path, and the next point on the path
            //        float angleFromPrevVertex = 180 - Vector3.Angle(lastSubPoint, nextSubPoint);
            //        float angleError = Mathf.Max(localAngle, angleFromPrevVertex);


            //        if ((angleError > maxAngleError && dstSinceLastVertex >= minVertexDst) || isLastPointOnPath)
            //        {

            //            currentPathLength += lastSubPoint.magnitude;
            //            splitData.cumulativeLength.Add(currentPathLength);
            //            splitData.vertices.Add(pointOnPath);
            //            splitData.tangents.Add(CubicBezierUtility.EvaluateCurveDerivative(segmentPoints, t).normalized);
            //            splitData.minMax.AddValue(pointOnPath);
            //            dstSinceLastVertex = 0;
            //            lastAddedPoint = pointOnPath;
            //        }
            //        else
            //        {
            //            dstSinceLastVertex += prevSubPoint.magnitude;
            //        }
            //        prevPointOnPath = pointOnPath;
            //        pointOnPath = nextPointOnPath;
            //    }
            //    splitData.anchorVertexMap.Add(splitData.vertices.Count - 1);
            //}
            //return splitData;
        }

        public static PathSplitData SplitBezierPathEvenly(BezierPath bezierPath, float spacing, float accuracy)
        {
            PathSplitData splitData = new PathSplitData();

            splitData.vertices.Add(bezierPath[0]);
            splitData.tangents.Add(CubicBezierUtility.EvaluateCurveDerivative(bezierPath.GetPointsInSegment(0), 0).normalized);
            splitData.cumulativeLength.Add(0);
            splitData.anchorVertexMap.Add(0);
            splitData.minMax.AddValue(bezierPath[0]);

            Vector3 prevPointOnPath = bezierPath[0];
            Vector3 lastAddedPoint = bezierPath[0];

            float currentPathLength = 0;
            float dstSinceLastVertex = 0;

            // Go through all segments and split up into vertices
            for (int segmentIndex = 0; segmentIndex < bezierPath.NumSegments; segmentIndex++)
            {
                Vector3[] segmentPoints = bezierPath.GetPointsInSegment(segmentIndex);
                float estimatedSegmentLength = CubicBezierUtility.EstimateCurveLength(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3]);
                int divisions = Mathf.CeilToInt(estimatedSegmentLength * accuracy);
                float increment = 1f / divisions;

                for (float t = increment; t <= 1; t += increment)
                {
                    bool isLastPointOnPath = (t + increment > 1 && segmentIndex == bezierPath.NumSegments - 1);
                    if (isLastPointOnPath)
                    {
                        t = 1;
                    }
                    Vector3 pointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints, t);
                    dstSinceLastVertex += (pointOnPath - prevPointOnPath).magnitude;

                    // If vertices are now too far apart, go back by amount we overshot by
                    if (dstSinceLastVertex > spacing)
                    {
                        float overshootDst = dstSinceLastVertex - spacing;
                        pointOnPath += (prevPointOnPath - pointOnPath).normalized * overshootDst;
                        t -= increment;
                    }

                    if (dstSinceLastVertex >= spacing || isLastPointOnPath)
                    {
                        currentPathLength += (lastAddedPoint - pointOnPath).magnitude;
                        splitData.cumulativeLength.Add(currentPathLength);
                        splitData.vertices.Add(pointOnPath);
                        splitData.tangents.Add(CubicBezierUtility.EvaluateCurveDerivative(segmentPoints, t).normalized);
                        splitData.minMax.AddValue(pointOnPath);
                        dstSinceLastVertex = 0;
                        lastAddedPoint = pointOnPath;
                    }
                    prevPointOnPath = pointOnPath;
                }
                splitData.anchorVertexMap.Add(splitData.vertices.Count - 1);
            }
            return splitData;
        }


        public class PathSplitData
        {
            public List<Vector3> vertices = new List<Vector3>();
            public List<Vector3> tangents = new List<Vector3>();
            public List<float> cumulativeLength = new List<float>();
            public List<int> anchorVertexMap = new List<int>();
            public MinMax3D minMax = new MinMax3D();
        }

        [BurstCompile]
        public struct SplitBezierPathByAngleError_Job : IJob
        {
            public SplitBezierPathByAngleError_Job(Vector3[] segmentPoints, Vector3 pointOnPath, Vector3 prevPointOnPath, Vector3 lastAddedPoint, float maxAngleError, float minVertexDst, bool isLastNumSegment, float increment)
            {
                this.segmentPoints = new NativeArray<Vector3>(segmentPoints, Allocator.TempJob);
                this.pointOnPath = pointOnPath;
                this.lastAddedPoint = lastAddedPoint;
                this.maxAngleError = maxAngleError;
                this.minVertexDst = minVertexDst;
                this.isLastNumSegment = isLastNumSegment;
                this.prevPointOnPath = prevPointOnPath;
                this.increment = increment;

                cumulativeLength = new NativeList<float>(AllocatorManager.TempJob);
                vertices = new NativeList<Vector3>(AllocatorManager.TempJob);
                tangents = new NativeList<Vector3>(AllocatorManager.TempJob);

                min = Vector3.positiveInfinity;
                max = Vector3.negativeInfinity;
            }

            public void dispose()
            {
                cumulativeLength.Dispose();
                segmentPoints.Dispose();
                vertices.Dispose();
                tangents.Dispose();
            }

            NativeArray<Vector3> segmentPoints;
            Vector3 pointOnPath;
            Vector3 prevPointOnPath;
            Vector3 lastAddedPoint;
            float maxAngleError;
            float minVertexDst;
            float increment;
            bool isLastNumSegment;

            [WriteOnly] public NativeList<float> cumulativeLength;
            [WriteOnly] public NativeList<Vector3> vertices;
            [WriteOnly] public NativeList<Vector3> tangents;

            public Vector3 min;
            public Vector3 max;

            public void Execute()
            {
                float dstSinceLastVertex = 0;
                float pathLength;
                for (float t = increment; t <= 1; t += increment)
                {
                    bool isLastPointOnPath = (t + increment > 1 && isLastNumSegment);
                    if (isLastPointOnPath)
                    {
                        t = 1;
                        pointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3], t);
                    }
                    Vector3 nextPointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3], t + increment);

                    var prevSubPoint = prevPointOnPath - pointOnPath;
                    var nextSubPoint = nextPointOnPath - pointOnPath;
                    var lastSubPoint = lastAddedPoint - pointOnPath;

                    // angle at current point on path
                    float localAngle = 180 - Vector3.Angle(prevSubPoint, nextSubPoint);
                    // angle between the last added vertex, the current point on the path, and the next point on the path
                    float angleFromPrevVertex = 180 - Vector3.Angle(lastSubPoint, nextSubPoint);
                    float angleError = Mathf.Max(localAngle, angleFromPrevVertex);


                    if ((angleError > maxAngleError && dstSinceLastVertex >= minVertexDst) || isLastPointOnPath)
                    {

                        pathLength = lastSubPoint.magnitude;
                        cumulativeLength.Add(pathLength);
                        vertices.Add(pointOnPath);
                        tangents.Add(CubicBezierUtility.EvaluateCurveDerivative(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3], t).normalized);
                        updateMinMax(pointOnPath);
                        dstSinceLastVertex = 0;
                        lastAddedPoint = pointOnPath;
                    }
                    else
                    {
                        dstSinceLastVertex += prevSubPoint.magnitude;
                    }
                    prevPointOnPath = pointOnPath;
                    pointOnPath = nextPointOnPath;
                }
            }

            private void updateMinMax(Vector3 v)
            {
                min.x = Mathf.Min(min.x, v.x);
                min.y = Mathf.Min(min.y, v.y);
                min.z = Mathf.Min(min.z, v.z);

                max.x = Mathf.Max(max.x, v.x);
                max.y = Mathf.Max(max.y, v.y);
                max.z = Mathf.Max(max.z, v.z);
            }
        }

        public static PathSplitData splitBezierPathByAngleError_UseJob(BezierPath bezierPath, float maxAngleError, float minVertexDst, float accuracy)
        {
            Vector3 prevPointOnPath = bezierPath[0];
            Vector3 lastAddedPoint = bezierPath[0];

            List<SplitBezierPathByAngleError_Job> allJobs = new List<SplitBezierPathByAngleError_Job>();
            NativeArray<JobHandle> jobHandles = new NativeArray<JobHandle>(bezierPath.NumSegments, Allocator.Persistent);


            // Go through all segments and split up into vertices
            for (int segmentIndex = 0; segmentIndex < bezierPath.NumSegments; segmentIndex++)
            {
                Vector3[] segmentPoints = bezierPath.GetPointsInSegment(segmentIndex);
                float estimatedSegmentLength = CubicBezierUtility.EstimateCurveLength(segmentPoints[0], segmentPoints[1], segmentPoints[2], segmentPoints[3]);
                int divisions = Mathf.CeilToInt(estimatedSegmentLength * accuracy);
                float increment = 1f / divisions;

                Vector3 pointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints, 0);

                SplitBezierPathByAngleError_Job job = new SplitBezierPathByAngleError_Job(segmentPoints, pointOnPath, prevPointOnPath, lastAddedPoint, maxAngleError, minVertexDst, segmentIndex == bezierPath.NumSegments - 1, increment);
                allJobs.Add(job);
                jobHandles[segmentIndex] = job.Schedule();

                prevPointOnPath = CubicBezierUtility.EvaluateCurve(segmentPoints, 1);
                lastAddedPoint = prevPointOnPath;               
            }

            var combineJob = JobHandle.CombineDependencies(jobHandles);
            combineJob.Complete();

            PathSplitData splitData = new PathSplitData();

            splitData.vertices.Add(bezierPath[0]);
            splitData.tangents.Add(CubicBezierUtility.EvaluateCurveDerivative(bezierPath.GetPointsInSegment(0), 0).normalized);
            splitData.cumulativeLength.Add(0);
            splitData.anchorVertexMap.Add(0);
            splitData.minMax.AddValue(bezierPath[0]);

            float currentPathLength = 0;

            for (int i = 0; i < allJobs.Count; i++)
            {
                var job = allJobs[i];
                for (int i2 = 0; i2 < job.cumulativeLength.Length; i2++)
                {
                    currentPathLength += job.cumulativeLength[i2];
                    splitData.cumulativeLength.Add(currentPathLength);
                }
                splitData.vertices.AddRange(job.vertices.ToArray());
                splitData.tangents.AddRange(job.tangents.ToArray());
                splitData.minMax.addMin(job.min);
                splitData.minMax.addMax(job.max);
                splitData.anchorVertexMap.Add(splitData.vertices.Count - 1);
                job.dispose();
            }
            jobHandles.Dispose();
            return splitData;
        }
    }
}