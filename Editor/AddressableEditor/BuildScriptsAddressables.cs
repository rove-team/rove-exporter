#if UNITY_EDITOR
using Rove.RoveExporter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
using UnityEditor.Build;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

namespace Rove.Editor
{
    public class BuildScriptsAddressables
    {
        private const string ROVE_BUILD_GROUP_NAME = "__RoveOpenningGroup__";
        private const string OUTPUT_FOLDER = "__RoveOutput";
        public static string TEMP_FOLDER = Path.Combine("Temp", "RoveExport");
        public const string BUILDIN_SHADER_NAME = "_unitybuiltinshaders_";
        public const string NO_BUILTIN_SHADER_NAME = "_NoBuiltinShader_";
        public const string BUILD_ID = "BuildScriptsAddressables";
        public const string PREFIX = "";
        public const string EXTENSION = "rovescene";

#if ROVE_DESIGNER
        [MenuItem("Rove Tools/Addressables/Build Openning Scene/Build WebGL")]
        public static void buildOpenningSceneWebGL()
        {
            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets =  new BuildTarget[]
            {
                BuildTarget.WebGL
            };
            buildSetting.outputFolder = AddressableBuildPath.WEBGL_ONLY;
            setupBuildOpenning(buildSetting);
        }
        [MenuItem("Rove Tools/Addressables/Build Openning Scene/Build All")]
        public static void buildOpenningSceneALL()
        {
            var isContinue = EditorUtility.DisplayDialog("Warning...", "", "Ok", "Cancel");
            if (!isContinue) return;

            var targets = new List<BuildTarget>();
            targets.Add(EditorUserBuildSettings.activeBuildTarget);

            var tempTargets = new BuildTarget[]
            {
                BuildTarget.StandaloneWindows64,
                BuildTarget.StandaloneOSX,
                BuildTarget.WebGL,
                BuildTarget.Android,
                BuildTarget.iOS
            };
            foreach (var t in tempTargets)
            {
                if (!targets.Contains(t))
                {
                    targets.Insert(0, t);
                }
            }

            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = targets.ToArray();
            buildSetting.outputFolder = AddressableBuildPath.ALL;
            setupBuildOpenning(buildSetting);
        }
        public static void buildAddressableWebGL()
        {
            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = new BuildTarget[]
            {
                BuildTarget.WebGL
            };
            buildSetting.outputFolder = AddressableBuildPath.WEBGL_ONLY;
            buildSetting.isRequireScene = false;

            setupBuildNormal(buildSetting);
        }
        [MenuItem("Rove Tools/Addressables/Build WebGL (without extension)")]
        public static void buildWebGLWithoutExtension()
        {
            var targets = new List<BuildTarget>();
            targets.Add(BuildTarget.WebGL);

            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = targets.ToArray();
            buildSetting.outputFolder = AddressableBuildPath.WEBGL_ONLY;
            buildSetting.isBuildWithoutExtension = true;
            buildSetting.isRequireScene = false;

            setupBuildNormal(buildSetting);
        }
        [MenuItem("Rove Tools/Addressables/Build Multiplatform (without extension)")]
        public static void buildAllWithoutExtension()
        {
            var isContinue = EditorUtility.DisplayDialog("Warning...", "It would take a long time. Are you sure?", "Ok", "Cancel");
            if (!isContinue) return;

            var targets = new List<BuildTarget>();
            targets.Add(EditorUserBuildSettings.activeBuildTarget);

            var tempTargets = new BuildTarget[]
            {
                BuildTarget.StandaloneWindows64,
                BuildTarget.StandaloneOSX,
                BuildTarget.WebGL,
                BuildTarget.Android,
                BuildTarget.iOS
            };
            foreach (var t in tempTargets)
            {
                if (!targets.Contains(t))
                {
                    targets.Insert(0, t);
                }
            }

            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = targets.ToArray();
            buildSetting.outputFolder = AddressableBuildPath.ALL;
            buildSetting.isBuildWithoutExtension = true;

            setupBuildNormal(buildSetting);
        }
        [MenuItem("Rove Tools/Addressables/Build Multiplatform (one file)")]
        public static void buildAllOneFile()
        {
            var isContinue = EditorUtility.DisplayDialog("Warning...", "It would take a long time. Are you sure?", "Ok", "Cancel");
            if (!isContinue) return;

            var targets = new List<BuildTarget>();
            targets.Add(EditorUserBuildSettings.activeBuildTarget);

            var tempTargets = new BuildTarget[]
            {
                BuildTarget.StandaloneWindows64,
                BuildTarget.StandaloneOSX,
                BuildTarget.WebGL,
                BuildTarget.Android,
                BuildTarget.iOS
            };
            foreach (var t in tempTargets)
            {
                if (!targets.Contains(t))
                {
                    targets.Insert(0, t);
                }
            }

            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = targets.ToArray();
            buildSetting.outputFolder = AddressableBuildPath.ALL;
            buildSetting.isBuildWithoutExtension = true;
            buildSetting.isOneFile = true;
            buildSetting.isRequireScene = false;

            setupBuildNormal(buildSetting);
        }
#endif
        [MenuItem("Rove Tools/Addressables/Build WebGL")]
        public static void buildWebGL()
        {
            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = new BuildTarget[]
            {
                BuildTarget.WebGL
            };
            buildSetting.outputFolder = AddressableBuildPath.WEBGL_ONLY;

            setupBuildNormal(buildSetting);
        }
        [MenuItem("Rove Tools/Addressables/Build Multiplatform")]
        public static void buildAll()
        {
            var isContinue = EditorUtility.DisplayDialog("Warning...", "It would take a long time. Are you sure?", "Ok", "Cancel");
            if (!isContinue) return;

            var targets = new List<BuildTarget>();
            targets.Add(EditorUserBuildSettings.activeBuildTarget);

            var tempTargets = new BuildTarget[]
            {
                BuildTarget.StandaloneWindows64,
                BuildTarget.StandaloneOSX,
                BuildTarget.WebGL,
                BuildTarget.Android,
                BuildTarget.iOS
            };
            foreach (var t in tempTargets)
            {
                if (!targets.Contains(t))
                {
                    targets.Insert(0, t);
                }
            }

            var buildSetting = new AddressableBuildSetting();
            buildSetting.buildTargets = targets.ToArray();
            buildSetting.outputFolder = AddressableBuildPath.ALL;

            setupBuildNormal(buildSetting);
        }

        private static void setupBuildOpenning(AddressableBuildSetting buildSetting)
        {
            EditorUtility.DisplayProgressBar("Building Addressables...", $"Initalize", 0);
            var scene = EditorSceneManager.GetActiveScene();
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            if (settings == null)
            {
                Debug.Log("[Rove tools] Require initialize Addressables Settings.\nSelect menu <b>Window > Asset Management > Addressables > Groups</b>");
                EditorUtility.ClearProgressBar();
                return;
            }
            Debug.Log("[Rove tools] Start build openning scene");

            var roveGroup = settings.FindGroup(ROVE_BUILD_GROUP_NAME);
            if (!roveGroup)
            {
                roveGroup = settings.CreateGroup(ROVE_BUILD_GROUP_NAME, false, false, true, null, typeof(ContentUpdateGroupSchema), typeof(BundledAssetGroupSchema));
            }
            
            var sceneGuid = AssetDatabase.AssetPathToGUID(scene.path);
            var oldGroup = settings.FindAssetEntry(sceneGuid);
            var oldGroupName = oldGroup != null ? oldGroup.parentGroup.name : "";

            settings.CreateOrMoveEntry(sceneGuid, roveGroup, false, true);

            buildSetting.buildMode = BuildMode.OPENNING;
            buildSetting.oldGroupOfOpenningSceneMode = oldGroupName;

            startBuild(buildSetting);
        }
        private static void setupBuildNormal(AddressableBuildSetting buildSetting)
        {
            EditorUtility.DisplayProgressBar("Building Addressables...", $"Initalize", 0);

            var settings = AddressableAssetSettingsDefaultObject.Settings;

            if (settings == null)
            {
                Debug.Log("[Rove tools] Require initialize Addressables Settings.\nSelect menu <b>Window > Asset Management > Addressables > Groups</b>");
                EditorUtility.ClearProgressBar();
                return;
            }
            Debug.Log("[Rove tools] Start build Addressables");

            buildSetting.buildMode = BuildMode.NORMAL;

            startBuild(buildSetting);
        }

        private async static void startBuild(AddressableBuildSetting buildSetting)
        {
            var isError = RoveEditorUtils.haveErrorRoveUid();
            if (isError)
            {
                var iisContinue = EditorUtility.DisplayDialog("Warning...", "Have issues with RoveUid, Fix it now?", "Ok", "Cancel");
                if (!iisContinue) return;
                RoveEditorUtils.fixRoveUidToRoveMonoBehaviour();
            }

            EditorSceneManager.MarkAllScenesDirty();
            EditorSceneManager.SaveOpenScenes();


            if (GraphicsSettings.renderPipelineAsset == null)
            {
                Debug.LogError("[Build Addressable] <color=white>Please install the Universal Render Pipeline to project</color>");
                postBuildProcess();
                return;
            }
            var nNPOTCount = RoveTextureUtils.findNonNPOTScaleTextures();
            var nCrunchedCount = RoveTextureUtils.findNonCrunchTextures();
            //var estData = RoveStatistic.estimateTrisVerMesh();

            if (nNPOTCount > 0)
            {
                Debug.Log($"<color=yellow>[Build Addressable] Found {nNPOTCount} texture(s) not NPOT scale</color>");
            }
            if (nCrunchedCount > 0)
            {
                Debug.Log($"<color=yellow>[Build Addressable] Found {nCrunchedCount} texture(s) not crunched</color>");
            }

            var settings = AddressableAssetSettingsDefaultObject.Settings;
            var groups = settings.groups;
            settings.ShaderBundleNaming = ShaderBundleNaming.Custom;
            settings.MonoScriptBundleNaming = MonoScriptBundleNaming.Disabled;

            var guid = Guid.NewGuid().ToString("n");
            settings.ShaderBundleCustomNaming = guid + BUILDIN_SHADER_NAME;
            settings.UniqueBundleIds = true;

            var projectPath = Path.GetDirectoryName(Application.dataPath);
            var rootBuildPath = Path.Combine(projectPath, TEMP_FOLDER);
            if (buildSetting.isBuildWithoutExtension)
            {
                buildSetting.outputFolder += " (No extension)";
            }
            var outputPath = Path.Combine(projectPath, OUTPUT_FOLDER, buildSetting.outputFolder);
            if (Directory.Exists(rootBuildPath))
            {
                Directory.Delete(rootBuildPath, true);
            }
            Directory.CreateDirectory(rootBuildPath);

            AddressableTargetBuild addressableTarget = new AddressableTargetBuild();
            addressableTarget.buildRootPath = rootBuildPath;
            addressableTarget.outputPath = outputPath;
            addressableTarget.buildMode = buildSetting.buildMode;
            addressableTarget.groupIncludeBuildValues = new bool[groups.Count];
            addressableTarget.buildGuid = guid;

            for (int i = 0; i < groups.Count; i++)
            {
                BundledAssetGroupSchema schema = groups[i].GetSchema<BundledAssetGroupSchema>();
                addressableTarget.groupIncludeBuildValues[i] = schema && schema.IncludeInBuild;
            }
            addressableTarget.startTarget = EditorUserBuildSettings.activeBuildTarget;
            addressableTarget.targets.AddRange(buildSetting.buildTargets);
            addressableTarget.targetCount = buildSetting.buildTargets.Count();
            addressableTarget.isBuildWithExtension = buildSetting.isBuildWithoutExtension;
            addressableTarget.isOneFile = buildSetting.isOneFile;

            if (buildSetting.buildMode == BuildMode.OPENNING)
            {
                addressableTarget.oldGroupOfOpenningSceneMode = buildSetting.oldGroupOfOpenningSceneMode;
            }

            var json = JsonUtility.ToJson(addressableTarget);
            EditorPrefs.SetString(BUILD_ID, json);

            string[] openedScenes = new string[EditorSceneManager.sceneCount];
            for (int i = 0; i < openedScenes.Length; i++)
            {
                openedScenes[i] = EditorSceneManager.GetSceneAt(i).path;
            }

            int gIndex = 0;
            bool isContinue = true;
            bool isGroupBuild = false;
            bool isHasSceneInAGroup = false;
            int buildGroupCount = 0;
            foreach (var group in groups)
            {
                BundledAssetGroupSchema schema = group.GetSchema<BundledAssetGroupSchema>();

                if (schema)
                {
                    if (buildSetting.buildMode == BuildMode.OPENNING)
                    {
                        schema.IncludeInBuild = group.name == ROVE_BUILD_GROUP_NAME;
                    }
                    else
                    {
                        schema.IncludeInBuild &= group.name != ROVE_BUILD_GROUP_NAME;
                    }
                    isGroupBuild |= schema.IncludeInBuild;
                }

                if (schema && !schema.IncludeInBuild)
                {
                    continue;
                }

                List<AddressableAssetEntry> entries = group.entries.ToList();
                bool hasScene = false;

                foreach (var e in entries)
                {
                    if (e.AssetPath.EndsWith(".unity"))
                    {
                        isHasSceneInAGroup |= true;
                        if (settings.DefaultGroup == null)
                        {
                            settings.DefaultGroup = group;
                        }
                        if (hasScene)
                        {
                            Debug.LogError("[Build Addressable] Do not support build one more scene in a group!");
                            postBuildProcess();
                            return;
                        }
                        buildGroupCount++;
                        hasScene = true;
                        var sceneName = Path.GetFileNameWithoutExtension(e.AssetPath).Trim();
                        settings.ShaderBundleCustomNaming = e.guid + "__" + sceneName;
                        addressableTarget.buildGuid = e.guid;

                        sceneName = sceneName.Replace(BUILDIN_SHADER_NAME, "_builtin_shaders_");

                        var pathBuiledName = $"Rove_{gIndex++}";
                        var groupPath = Path.Combine(TEMP_FOLDER, sceneName + "-" + e.guid);
                        var pathInDisk = Path.Combine(groupPath, "[UnityEditor.EditorUserBuildSettings.activeBuildTarget]");

                        settings.profileSettings.CreateValue(pathBuiledName, pathInDisk);
                        var profileId = settings.profileSettings.GetProfileId("Default");
                        settings.profileSettings.SetValue(profileId, pathBuiledName, pathInDisk);

                        schema.BuildPath.SetVariableByName(settings, pathBuiledName);
                        schema.LoadPath.SetVariableByName(settings, pathBuiledName);
                        
                        if (gIndex == 1)
                        {
                            BundledAssetGroupSchema defaultSchema = settings.DefaultGroup.GetSchema<BundledAssetGroupSchema>();
                            defaultSchema.BuildPath.SetVariableByName(settings, pathBuiledName);
                            defaultSchema.LoadPath.SetVariableByName(settings, pathBuiledName);
                        }

                        var scene = EditorSceneManager.OpenScene(e.AssetPath, OpenSceneMode.Single);
                        await Task.Delay(2000);
                        int camCount = 0;
                        var cameras = GameObject.FindObjectsOfType<Camera>();
                        foreach (var cam in cameras)
                        {
                            if (!cam.GetComponent<ThumbnailCapturer>())
                            {
                                camCount++;
                            }
                        }

                        if (camCount > 0)
                        {
                            Debug.LogError($"[Build Addressable] Too many [Camera] in scene <color=white>{e.AssetPath}</color>, please remove or disable them");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }

                        var audioListener = GameObject.FindObjectsOfType<AudioListener>();
                        if (audioListener.Length > 0)
                        {
                            Debug.LogError($"[Build Addressable] Too many [AudioListener] in scene <color=white>{e.AssetPath}</color>, please remove or disable them");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }

                        var eventSystem = GameObject.FindObjectsOfType<EventSystem>();
                        if (eventSystem.Length > 0)
                        {
                            Debug.LogError($"[Build Addressable] Too many [EventSystem] in scene <color=white>{e.AssetPath}</color>, please remove or disable them");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }

                        var isCanExportRoveFrame = true;//RovePhotoFrameEditor.isCanExport();
                        if (!isCanExportRoveFrame)
                        {
                            Debug.LogError($"[Build Addressable] Have error(s)");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }
                        var playerSpawnPoint = GameObject.FindObjectOfType<PlayerSpawnPoint>(true);
                        if (playerSpawnPoint == null)
                        {
                            Debug.LogError($"[Build Addressable] Require <color=cyan>[PlayerSpawnPoint]</color> in scene <color=white>{e.AssetPath}</color>");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }
                        else if (!playerSpawnPoint.isCanBeOnGround())
                        {
                            Debug.LogError($"[Build Addressable] <color=cyan>[PlayerSpawnPoint]</color> in scene <color=white>{e.AssetPath}</color> must be on ground");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }
                        else if (buildSetting.isOneFile && isHasSceneInAGroup)
                        {
                            Debug.LogError($"[Build Addressable] Not support build environment in one file mode");
                            isContinue = false;
                            goto SHOW_ERROR;
                        }

                        var roveObjectManager = GameObject.FindObjectOfType<RoveObjectManager>(true);
                        if (roveObjectManager)
                        {
                            roveObjectManager.buildJson();
                            EditorSceneManager.SaveScene(scene);
                        }

                        var isDone = createThumbnail(groupPath, e.AssetPath);
                        isContinue &= isDone;
                    }
                }

                if (!hasScene && schema)
                {
                    foreach (var e in entries)
                    {
                        var pathBuiledName = $"RoveNoScene_{gIndex++}";
                        var groupPath = Path.Combine(TEMP_FOLDER, NO_BUILTIN_SHADER_NAME + Path.GetFileNameWithoutExtension(e.AssetPath) + "-" + e.guid);
                        var pathInDisk = Path.Combine(groupPath, "[UnityEditor.EditorUserBuildSettings.activeBuildTarget]");
                        settings.profileSettings.CreateValue(pathBuiledName, pathInDisk);
                        var profileId = settings.profileSettings.GetProfileId("Default");
                        settings.profileSettings.SetValue(profileId, pathBuiledName, pathInDisk);

                        schema.BuildPath.SetVariableByName(settings, pathBuiledName);
                        schema.LoadPath.SetVariableByName(settings, pathBuiledName);
                        buildGroupCount++;
                        break;
                    }
                }
            }

            if (buildGroupCount == 0)
            {
                goto SHOW_ERROR;
            }

            for (int i = 0; i < openedScenes.Length; i++)
            {
                EditorSceneManager.OpenScene(openedScenes[i], i == 0 ? OpenSceneMode.Single : OpenSceneMode.Additive);
            }


            SHOW_ERROR:
            if (!isContinue)
            {
                await Task.Delay(500);
                postBuildProcess();
                return;
            }
            if (buildGroupCount == 0)
            {
                Debug.LogError($"[Build Addressable] No resource to build");
                await Task.Delay(500);
                postBuildProcess();
                return;
            }
            else if (!isHasSceneInAGroup && buildSetting.isRequireScene)
            {
                Debug.LogError($"[Build Addressable] No groups have a scene");
                await Task.Delay(500);
                postBuildProcess();
                return;
            }
            else if (!isGroupBuild)
            {
                Debug.LogError($"[Build Addressable] No groups IncludeInBuild");
                await Task.Delay(500);
                postBuildProcess();
                return;
            }

            json = JsonUtility.ToJson(addressableTarget);
            EditorPrefs.SetString(BUILD_ID, json);
            nextBuild();
        }
        private static bool createThumbnail(string groupPath, string sceneAssetPath)
        {
            try
            {
                var sceneName = Path.GetFileNameWithoutExtension(sceneAssetPath);
                var captureCamera = GameObject.FindObjectOfType<ThumbnailCapturer>(true);
                if (!Directory.Exists(groupPath))
                {
                    Directory.CreateDirectory(groupPath);
                }

                if (captureCamera != null)
                {
                    var imagePath = Path.Combine(groupPath, $"{sceneName.ToLower()}.png");
                    if (File.Exists(imagePath))
                    {
                        File.Delete(imagePath);
                    }
                    if (!string.IsNullOrEmpty(captureCamera._UrlPath) && File.Exists(captureCamera._UrlPath))
                    {
                        File.Copy(captureCamera._UrlPath, imagePath, true);
                    }
                    else
                    {
                        captureCamera.captureNow();
                        File.Copy(captureCamera._UrlPath, imagePath, true);
                    }
                }
                else
                {
                    var scene = EditorSceneManager.GetActiveScene();
                    captureCamera = UnityEngine.Object.FindObjectOfType<ThumbnailCapturer>(true);
                    Debug.Log($"[Building Addressables] <color=red>Need <color=cyan>[ThumbnailCapturer]</color> in the scene to create thumbnail: {sceneName}</color>");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError($"[Build Addressables] " + e);
                return false;
            }
        }
        private static void nextBuild()
        {
            AddressableActiveBuild a = new AddressableActiveBuild();
            a.startBuild();
        }

        public static void BuildAddressables()
        {
            try
            {
                //Debug.Log("[Rove tools][Building Addressables - START] target: " + EditorUserBuildSettings.activeBuildTarget);

                AddressableAssetSettings.CleanPlayerContent();
                var r = BuildPlayerContentImpl();
                if (!string.IsNullOrEmpty(r.Error))
                {
                    throw new Exception(r.Error);
                }
                else
                {
                    Debug.Log("[Build Addressables - DONE] target: " + EditorUserBuildSettings.activeBuildTarget + $" (duration : {TimeSpan.FromSeconds(r.Duration).ToString("g")})");
                }

                nextBuild();
            }
            catch (Exception e)
            {
                BuildScriptsAddressables.postBuildProcess();
                Debug.LogError("[Build Addressables - ERROR] target: " + EditorUserBuildSettings.activeBuildTarget + " --- Error: " + e);
            }
        }
        internal static AddressablesPlayerBuildResult BuildPlayerContentImpl()
        {
            var settings = AddressableAssetSettingsDefaultObject.Settings;

            var buildContext = new AddressablesDataBuilderInput(settings);
            var result = settings.ActivePlayerDataBuilder.BuildData<AddressablesPlayerBuildResult>(buildContext);

            if (BuildScript.buildCompleted != null)
                BuildScript.buildCompleted(result);
            AssetDatabase.Refresh();
            return result;
        }
        public static void postBuildProcess()
        {
            try
            {
                var json = EditorPrefs.GetString(BuildScriptsAddressables.BUILD_ID);
                var targetBuild = JsonUtility.FromJson<AddressableTargetBuild>(json);
                if (targetBuild != null)
                {
                    var settings = AddressableAssetSettingsDefaultObject.Settings;
                    var groups = settings.groups;
                    for (int i = 0; i < groups.Count; i++)
                    {
                        BundledAssetGroupSchema schema = groups[i].GetSchema<BundledAssetGroupSchema>();
                        if (schema)
                        {
                            schema.IncludeInBuild = targetBuild.groupIncludeBuildValues[i];
                        }
                        if (groups[i].name == ROVE_BUILD_GROUP_NAME)
                        {
                            var entries = groups[i].entries.ToArray();
                            foreach (var e in entries)
                            {
                                groups[i].RemoveAssetEntry(e, true);
                                if (!string.IsNullOrEmpty(targetBuild.oldGroupOfOpenningSceneMode))
                                {
                                    var group = settings.FindGroup(targetBuild.oldGroupOfOpenningSceneMode);
                                    if (group)
                                    {
                                        settings.CreateOrMoveEntry(e.guid, group, false, true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
            EditorPrefs.DeleteKey(BuildScriptsAddressables.BUILD_ID);
            EditorUtility.ClearProgressBar();
        }
    }

    public class AddressableActiveBuild : IActiveBuildTargetChanged
    {
        public int callbackOrder => 0;

        public void startBuild()
        {
            var json = EditorPrefs.GetString(BuildScriptsAddressables.BUILD_ID, "");
            AddressableTargetBuild targetBuild = JsonUtility.FromJson<AddressableTargetBuild>(json);

            if (targetBuild.targets.Count > 0 && targetBuild.targetCount > 0)
            {
                var target = targetBuild.targets[0];
                var progress = (targetBuild.targetCount - targetBuild.targets.Count) * 1f / targetBuild.targetCount;
                EditorUtility.DisplayProgressBar("Building Addressables...", $"Building {target}", progress);
                targetBuild.targets.RemoveAt(0);
                EditorPrefs.SetString(BuildScriptsAddressables.BUILD_ID, targetBuild.toJson());
                switchPlatform(target);
            }
            else
            {
                EditorUtility.ClearProgressBar();
                generateJsonFile(targetBuild.isBuildWithExtension);
                if (!Directory.Exists(targetBuild.outputPath))
                {
                    Directory.CreateDirectory(targetBuild.outputPath);
                }
                
                var allSceneDir = Directory.GetDirectories(targetBuild.buildRootPath);
                List<string> sceneBuiltPaths = new List<string>();
                foreach (var sceneDir in allSceneDir)
                {
                    var sceneName = Path.GetFileName(sceneDir);
                    var nDir = Path.Combine(targetBuild.outputPath, sceneName);
                    if (Directory.Exists(nDir))
                    {
                        Directory.Delete(nDir, true);
                    }
                    Directory.Move(sceneDir, nDir);
                    sceneBuiltPaths.Add(nDir);
                }

                BuildScriptsAddressables.postBuildProcess();

                foreach (var path in sceneBuiltPaths)
                {
                    var name = Path.GetFileName(path);
                    Debug.Log($"[Rove tools] <b><color=blue>Complete Exported: <color=white>{name}</color></color></b>: <color=lime>\"" + path + "</color>\"");
                }

                BuildAddrressableResultWindow.show(sceneBuiltPaths.ToArray());
            }
        }

        private void updateAllBuiltInShaderFile(string[] allRoveDir)
        {
            Dictionary<string, string> allShader = new Dictionary<string, string>();
            foreach (var roveSceneDir in allRoveDir)
            {
                var allPlatformDir = Directory.GetDirectories(roveSceneDir);
                foreach (var platformDir in allPlatformDir)
                {
                    var files = Directory.GetFiles(platformDir);
                    foreach (var f in files)
                    {
                        if (f.Contains(BuildScriptsAddressables.BUILDIN_SHADER_NAME))
                        {
                            var dirName = Path.GetFileName(platformDir);
                            allShader.Add(dirName, f);
                        }
                    }
                }
            }
            foreach (var roveSceneDir in allRoveDir)
            {
                var allPlatformDir = Directory.GetDirectories(roveSceneDir);
                var sceneDirName = Path.GetFileNameWithoutExtension(roveSceneDir);
                if (sceneDirName.StartsWith(BuildScriptsAddressables.NO_BUILTIN_SHADER_NAME))
                {
                    continue;
                }
                foreach (var platformDir in allPlatformDir)
                {
                    var dirName = Path.GetFileName(platformDir);
                    if (allShader.ContainsKey(dirName))
                    {
                        var shaderFile = allShader[dirName];
                        var shaderName = Path.GetFileName(shaderFile);
                        var shaderDst = Path.Combine(platformDir, shaderName);
                        if (shaderDst != shaderFile)
                        {
                            File.Copy(shaderFile, shaderDst, true);
                        }
                    }
                }
            }
        }
        private void fixName(string[] allRoveDir, bool isWithoutExtension)
        {
            foreach (var roveSceneDir in allRoveDir)
            {
                var allPlatformDir = Directory.GetDirectories(roveSceneDir);
                var sceneName = Path.GetFileName(roveSceneDir);
                foreach (var platformDir in allPlatformDir)
                {
                    var files = Directory.GetFiles(platformDir);
                    var platformName = Path.GetFileName(platformDir);
                    int count = 0;
                    foreach (var f in files)
                    {
                        var fName = Path.GetFileName(f);
                        string nName = "";
                        if (fName.Contains(BuildScriptsAddressables.BUILDIN_SHADER_NAME))
                        {
                            nName += sceneName + "_" + platformName + "_builtinshaders";
                        }
                        else
                        {
                            nName += sceneName + "_" + platformName;
                            if (nName.StartsWith(BuildScriptsAddressables.NO_BUILTIN_SHADER_NAME))
                            {
                                nName = nName.Replace("_NoBuiltinShader_", "");
                            }
                            else
                            {
                                nName += "_scene";
                            }
                            if (count > 0)
                            {
                                nName += count;
                            }
                            count++;
                        }
                        if (!string.IsNullOrEmpty(BuildScriptsAddressables.PREFIX))
                        {
                            nName = BuildScriptsAddressables.PREFIX + "_";
                        }
                        if (!string.IsNullOrEmpty(BuildScriptsAddressables.EXTENSION) && !isWithoutExtension)
                        {
                            nName += "." + BuildScriptsAddressables.EXTENSION;
                        }
                        try
                        {
                            var parts = nName.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                            if (parts.Length > 1)
                            {
                                var guid = "-" + parts.Last();
                                var index0fGuid = nName.IndexOf(guid);
                                nName = nName.Remove(index0fGuid, 33);
                            }
                        }
                        catch (Exception ex)
                        {
                            Debug.LogWarning("Failed to remove GUID from name of bundles: " + ex.Message);
                        }
                        var nPath = Path.Combine(platformDir, nName.ToLower());
                        File.Move(f, nPath);
                    }
                }
            }
        }

        private void generateJsonFile(bool isWithoutExtension)
        {
            var json = EditorPrefs.GetString(BuildScriptsAddressables.BUILD_ID, "");
            AddressableTargetBuild targetBuild = JsonUtility.FromJson<AddressableTargetBuild>(json);

            var allRoveDir = Directory.GetDirectories(targetBuild.buildRootPath);
            
            updateAllBuiltInShaderFile(allRoveDir);
            fixName(allRoveDir, targetBuild.isBuildWithExtension);

            foreach (var roveSceneDir in allRoveDir) 
            {
                var allPlatformDir = Directory.GetDirectories(roveSceneDir);
                AddressableBuiltData builtData = new AddressableBuiltData();
                AddressableBuildInfo buildInfo = new AddressableBuildInfo();
                buildInfo.buildGuid = targetBuild.buildGuid;
                buildInfo.exportVersion = RoveExporterInfo.EXPORT_VERSION;


                builtData.name = Path.GetFileName(roveSceneDir);
                long totalSum = 0;
                foreach (var f in Directory.GetFiles(roveSceneDir))
                {
                    if (f.EndsWith(".png"))
                    {
                        builtData.alias = Path.GetFileNameWithoutExtension(f);
                        break; 
                    }
                }
                foreach (var platformDir in allPlatformDir)
                {
                    AddressableBuiltData.AddressablePlatform addressableFiles = new AddressableBuiltData.AddressablePlatform();

                    var platform = Path.GetFileName(platformDir);
                    var files = Directory.GetFiles(platformDir);
                    List<string> roveFiles = new List<string>();
                    long sum = 0;
                    for (int i = 0; i < files.Length; i++)
                    {
                        var fName = Path.GetFileName(files[i]);
                        if (fName.StartsWith(BuildScriptsAddressables.PREFIX) && (fName.EndsWith(BuildScriptsAddressables.EXTENSION) || isWithoutExtension))
                        {
                            roveFiles.Add(Path.GetFileName(files[i]));
                            FileInfo fileInfo = new FileInfo(files[i]);
                            sum += fileInfo.Length;
                            totalSum += sum;
                        }
                    }
                    var sumStr = Convert.ToBase64String(BitConverter.GetBytes(sum), 0);
                    builtData.add(platform, roveFiles.ToArray()).checksum = string.Join("", sumStr.Reverse());
                }
                builtData.checksum = Convert.ToBase64String(BitConverter.GetBytes(totalSum), 0);
                builtData.unity_version = Application.unityVersion;
                var jsonPath = Path.Combine(roveSceneDir, "catalog.json");
                var buildInfoPath = Path.Combine(roveSceneDir, "buildInfo.json");
                File.WriteAllText(jsonPath, builtData.toJson());
                File.WriteAllText(buildInfoPath, buildInfo.toJson());
            }

            moveToOneFolder(allRoveDir);
            if (targetBuild.isOneFile)
            {
                mergeToOneFile(allRoveDir);
            }
        }
        private void moveToOneFolder(string[] allBuiltpaths)
        {
            foreach (var sceneBuilt in allBuiltpaths)
            {
                var dirs = Directory.GetDirectories(sceneBuilt);
                var sceneName = Path.GetFileName(sceneBuilt);
                foreach (var dir in dirs)
                {
                    var files = Directory.GetFiles(dir);
                    foreach (var f in files)
                    {
                        var fName = Path.GetFileName(f);
                        var nPath = Path.Combine(sceneBuilt, fName.ToLower());
                        File.Move(f, nPath);
                    }
                    Directory.Delete(dir);
                }
            }
        }
        private void mergeToOneFile(string[] allBuiltpaths)
        {
            foreach (var sceneBuilt in allBuiltpaths)
            {
                AddressableAFile addressableAFile = new AddressableAFile();
                var sceneName = Path.GetFileName(sceneBuilt);
                var files = Directory.GetFiles(sceneBuilt);
                AddressableBuiltData builtData = null;
                foreach (var f in files)
                {
                    if (f.Contains("catalog.json"))
                    {
                        var json = File.ReadAllText(f);
                        builtData = JsonUtility.FromJson<AddressableBuiltData>(json);
                        break;
                    }
                }
                if (builtData == null)
                {
                    continue;
                }
                foreach (var f in files)
                {
                    var fileName = Path.GetFileName(f);
                    foreach (var p in builtData.allPlatforms)
                    {
                        if (p.all_files.Contains(fileName))
                        {
                            addressableAFile.addFile(p.platform, f);
                            break;
                        }
                    }
                }
                var path = addressableAFile.writeToFile(sceneName, sceneBuilt);
            }
        }

        private void switchPlatform(BuildTarget target)
        {
            try
            {
                switch (target)
                {
                    case BuildTarget.StandaloneWindows:
                        switchPlatform(BuildTarget.StandaloneWindows, BuildTargetGroup.Standalone);
                        break;
                    case BuildTarget.WebGL:
                        switchPlatform(BuildTarget.WebGL, BuildTargetGroup.WebGL);
                        break;
                    case BuildTarget.StandaloneWindows64:
                        switchPlatform(BuildTarget.StandaloneWindows64, BuildTargetGroup.Standalone);
                        break;
                    case BuildTarget.iOS:
                        switchPlatform(BuildTarget.iOS, BuildTargetGroup.iOS);
                        break;
                    case BuildTarget.Android:
                        switchPlatform(BuildTarget.Android, BuildTargetGroup.Android);
                        break;
                    case BuildTarget.StandaloneOSX:
                        switchPlatform(BuildTarget.StandaloneOSX, BuildTargetGroup.Standalone);
                        break;
                    default:
                        throw new Exception("Not support build for " + target);
                }
            }
            catch (Exception e)
            {
                BuildScriptsAddressables.postBuildProcess();
                Debug.LogError("[Build Addressable] " + e);
            }
        }

        private async void switchPlatform(BuildTarget target, BuildTargetGroup group)
        {
            if (EditorUserBuildSettings.activeBuildTarget == target && BuildPipeline.GetBuildTargetGroup(EditorUserBuildSettings.activeBuildTarget) == group)
            {
                OnActiveBuildTargetChanged(target, target);
            }
            else
            {
                await Task.Delay(100);
                var b = EditorUserBuildSettings.SwitchActiveBuildTargetAsync(group, target);
                if (!b)
                {
                    startBuild();
                    Debug.LogWarning("[Building Addressables] Can't not switch platform: Target - " + target + " --- Group - " + group);
                }
            }
        }

        public void OnActiveBuildTargetChanged(BuildTarget previousTarget, BuildTarget newTarget)
        {
            var json = EditorPrefs.GetString(BuildScriptsAddressables.BUILD_ID, "");
            if (!string.IsNullOrEmpty(json))
            {
                BuildScriptsAddressables.BuildAddressables();
            }
        }
    }

    [Serializable]
    public class AddressableTargetBuild : AbsJsonable
    {
        public string buildRootPath;
        public string outputPath;
        public int targetCount;
        public BuildTarget startTarget;
        public List<BuildTarget> targets = new List<BuildTarget>();
        public BuildMode buildMode;
        public bool[] groupIncludeBuildValues;
        public string oldGroupOfOpenningSceneMode;
        public bool isBuildWithExtension;
        public string buildGuid;
        public bool isOneFile;
    }

    public static class AddressableBuildPath
    {
        public const string ALL = "All";
        public const string WEBGL_ONLY = "WebGL Only";
    }

    public class AddressableBuildSetting
    {
        public BuildTarget[] buildTargets;
        public string outputFolder;
        public BuildMode buildMode;
        public string oldGroupOfOpenningSceneMode;
        public bool isBuildWithoutExtension;
        public bool isRequireScene = true;
        public bool isOneFile;
    }

    public enum BuildMode
    {
        NORMAL,
        OPENNING
    }
}
#endif