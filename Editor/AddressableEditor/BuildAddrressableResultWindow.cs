#if UNITY_EDITOR
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    internal class BuildAddrressableResultWindow : EditorWindow
    {
        private string[] resultPaths;
        public static EditorWindow show(params string[] buildPaths)
        {
            BuildAddrressableResultWindow window = BuildAddrressableResultWindow.GetWindow(typeof(BuildAddrressableResultWindow)) as BuildAddrressableResultWindow;
            window.titleContent = new GUIContent("Results");
            window.resultPaths = buildPaths;
            return window;
        }
        private void OnGUI()
        {
            if (resultPaths == null || resultPaths.Length == 0)
            {
                return;
            }
            this.minSize = new Vector2(350, 100);
            GUILayout.Space(5);
            for (int i = 0; i < resultPaths.Length; i++)
            {
                var name = Path.GetFileNameWithoutExtension(resultPaths[i]);
                var path = resultPaths[i];
                GUILayout.BeginHorizontal(GUILayout.ExpandHeight(false));
                GUILayout.Label(name, EditorStyles.boldLabel);
                GUILayout.FlexibleSpace();
                if (GUILayout.Button($"Open folder"))
                {
                    OpenInFileBrowser.Open(path);
                }
                if (GUILayout.Button($"Copy path"))
                {
                    GUIUtility.systemCopyBuffer = path;
                    Debug.Log($"[Rove tools] <b><color=blue>Copied <color=white>{name}</color></color></b>: <color=lime>\"" + path + "</color>\"");
                    this.ShowNotification(new GUIContent("Copied"));
                }
                GUILayout.EndHorizontal();
            }
        }
    }
}
#endif