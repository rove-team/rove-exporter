using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomPropertyDrawer(typeof(RoveParameter))]
    public class RoveParameterDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
            contentPosition.height /= 3;

            var keyLable = new GUIContent("Key");
            var typeLable = new GUIContent("Type");
            var valueLable = new GUIContent("Value");
            var namePosition = contentPosition;
            EditorGUI.LabelField(namePosition, keyLable);
            namePosition.position = new Vector2(namePosition.x, namePosition.y + namePosition.height);
            EditorGUI.LabelField(namePosition, typeLable);
            namePosition.position = new Vector2(namePosition.x, namePosition.y + namePosition.height);
            EditorGUI.LabelField(namePosition, valueLable);

            contentPosition.position = new Vector2(contentPosition.x + 50, contentPosition.y);
            contentPosition.width -= 50;
            EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("key"), GUIContent.none);
            contentPosition.position = new Vector2(contentPosition.position.x, contentPosition.position.y + contentPosition.height);
            EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("type"), GUIContent.none);
            contentPosition.position = new Vector2(contentPosition.position.x, contentPosition.position.y + contentPosition.height);

            var type = (RoveParameterType)property.FindPropertyRelative("type").intValue;
            switch (type)
            {
                case RoveParameterType.Bool:
                    EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("boolValue"), GUIContent.none);
                    break;
                case RoveParameterType.Integer:
                    EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("intValue"), GUIContent.none);
                    break;
                case RoveParameterType.Float:
                    EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("floatValue"), GUIContent.none);
                    break;
                case RoveParameterType.String:
                    EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("stringValue"), GUIContent.none);
                    break;
                case RoveParameterType.Component:
                    EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("componentValue"), GUIContent.none);
                    break;
            }
        }
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * 3;
        }
    }
}
