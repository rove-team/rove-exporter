#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RovePoint)), CanEditMultipleObjects]
    public class RovePointEditor : UnityEditor.Editor
    {
        SerializedProperty _roveIdProperty;
        SerializedProperty _tagProperty;
        SerializedProperty _roveParamsProperty;

        private void OnEnable()
        {
            _roveIdProperty = serializedObject.FindProperty("_RoveId");
            _tagProperty = serializedObject.FindProperty("_roveTags");
            _roveParamsProperty = serializedObject.FindProperty("_roveParameters");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            for (int i = 0; i < _tagProperty.arraySize; i++)
            {
                _tagProperty.GetArrayElementAtIndex(i).stringValue = _tagProperty.GetArrayElementAtIndex(i).stringValue.Trim().ToLower().Replace(' ', '_').Replace('\t', '_').Replace('\n', '_');
            }
            EditorGUILayout.LabelField("RoveId: " + _roveIdProperty.stringValue);
            EditorGUILayout.PropertyField(_tagProperty);
            EditorGUILayout.PropertyField(_roveParamsProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif