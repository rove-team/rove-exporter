#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RoveInteractable)), CanEditMultipleObjects]
    public class RoveInteractableEditor : UnityEditor.Editor
    {
        SerializedProperty _interactTypeProperty;
        SerializedProperty _IsLocalOnlyProperty;
        SerializedProperty _keyCodeProperty;
        SerializedProperty _onInteractEventProperty;

        private bool _isInputKeyBoard;

        private void OnEnable()
        {
            _IsLocalOnlyProperty = serializedObject.FindProperty("_isLocalOnly");
            _interactTypeProperty = serializedObject.FindProperty("_interactType");
            _keyCodeProperty = serializedObject.FindProperty("_keyCode");
            _onInteractEventProperty = serializedObject.FindProperty("_onInteractedEvent");
        }

        public override void OnInspectorGUI()
        {
            EditorGUILayout.Space();
            serializedObject.Update();
            EditorGUILayout.PropertyField(_IsLocalOnlyProperty);
            EditorGUILayout.PropertyField(_interactTypeProperty);
            if (_interactTypeProperty.intValue == (int)RoveInteractType.KEYBOARD)
            {
                EditorGUILayout.PropertyField(_keyCodeProperty);
                if (!_isInputKeyBoard)
                {
                    if (GUILayout.Button("Enter Keycode"))
                    {
                        _isInputKeyBoard = true;
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("Press a key....", MessageType.Info);
                    Event e = Event.current;
                    if (e.isKey)
                    {
                        _isInputKeyBoard = false;
                        _keyCodeProperty.intValue = (int)e.keyCode;
                    }
                }
            }
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_onInteractEventProperty);

            var roveInteractable = (target as RoveInteractable);
            if (GUILayout.Button("Add animator setter"))
            {
                roveInteractable.gameObject.AddComponent<RoveAnimatorSetter>();
            }
            foreach (var invoker in roveInteractable.GetComponents<IRoveTriggerEventInvoker>())
            {
                invoker.EventType = RoveTriggerEventType.OnInteracted;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif