#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RoveProjector))]
    public class RoveProjectorEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}
#endif
