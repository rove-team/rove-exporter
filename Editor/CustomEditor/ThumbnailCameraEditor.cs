#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(ThumbnailCapturer))]
    public class ThumbnailCameraEditor : UnityEditor.Editor
    {
        SerializedProperty _UidProprerty;
        SerializedProperty _ResolutionProperty;
        SerializedProperty _IsDestroyOnRunTimeProperty;
        SerializedProperty _IsHideAllCanvasWhenCaptureProperty;


        private string _ThumbnailRenderUrl;
        private Texture2D _ThumbnailTexture;

        private void OnEnable()
        {
            _IsDestroyOnRunTimeProperty = serializedObject.FindProperty("_IsDestroyOnRuntime");
            _ResolutionProperty = serializedObject.FindProperty("_Resolution");
            _IsHideAllCanvasWhenCaptureProperty = serializedObject.FindProperty("_IsHideAllCanvasWhenCapture");
        }
        public override void OnInspectorGUI()
        {
            EditorGUILayout.Space();
            serializedObject.Update();

            EditorGUILayout.PropertyField(_ResolutionProperty);
            EditorGUILayout.PropertyField(_IsDestroyOnRunTimeProperty);
            EditorGUILayout.PropertyField(_IsHideAllCanvasWhenCaptureProperty);

            EditorGUILayout.Space();
            ThumbnailCapturer thumbnailCamera = (ThumbnailCapturer)target;

            if (thumbnailCamera._IsMarkClearCache)
            {
                if (_ThumbnailTexture)
                {
                    DestroyImmediate(_ThumbnailTexture);
                }
                _ThumbnailRenderUrl = null;
                thumbnailCamera._IsMarkClearCache = false;
                thumbnailCamera.saveScene();
            }
            else
            {
                var generatePath = thumbnailCamera.generatePath();
                if (File.Exists(generatePath))
                {
                    thumbnailCamera._UrlPath = generatePath;
                }
                if (!File.Exists(thumbnailCamera._UrlPath) && !string.IsNullOrEmpty(thumbnailCamera._UrlPath))
                {
                    if (_ThumbnailTexture)
                    {
                        DestroyImmediate(_ThumbnailTexture);
                    }
                    thumbnailCamera._UrlPath = null;
                    thumbnailCamera.saveScene();
                }
            }
            if (!string.IsNullOrEmpty(thumbnailCamera._UrlPath))
            {
                EditorGUILayout.HelpBox(thumbnailCamera._UrlPath, MessageType.None);
                EditorGUILayout.Space();
            }

            GUILayout.BeginHorizontal(GUILayout.ExpandHeight(false));
            if (GUILayout.Button("Capture"))
            {
                thumbnailCamera.captureNow();
                thumbnailCamera.saveScene();
            }

            if (File.Exists(thumbnailCamera._UrlPath))
            {
                if (GUILayout.Button("Open folder"))
                {
                    OpenInFileBrowser.Open(thumbnailCamera._UrlPath);
                }
                if (GUILayout.Button("Delete"))
                {
                    File.Delete(thumbnailCamera._UrlPath);
                    DestroyImmediate(_ThumbnailTexture);
                    _ThumbnailRenderUrl = null;
                    thumbnailCamera._UrlPath = null;
                    thumbnailCamera.saveScene();
                }
            }
            GUILayout.EndHorizontal();
            serializedObject.ApplyModifiedProperties();

            if (_ThumbnailRenderUrl != thumbnailCamera._UrlPath)
            {
                _ThumbnailRenderUrl = thumbnailCamera._UrlPath;
                if (_ThumbnailTexture)
                {
                    GameObject.DestroyImmediate(_ThumbnailTexture);
                }
                var previewUrl = _ThumbnailRenderUrl += ".preview";
                if (File.Exists(previewUrl))
                {
                    _ThumbnailTexture = new Texture2D(1, 1, TextureFormat.ARGB32, true);
                    var data = File.ReadAllBytes(previewUrl);
                    _ThumbnailTexture.LoadImage(data);
                }
            }

            if (_ThumbnailTexture)
            {
                GUILayout.BeginHorizontal(GUILayout.ExpandHeight(false));
                GUILayout.FlexibleSpace();
                GUILayout.Box(_ThumbnailTexture);
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();
            }
        }
        private void OnDestroy()
        {
            DestroyImmediate(_ThumbnailTexture);
        }
    }
}
#endif