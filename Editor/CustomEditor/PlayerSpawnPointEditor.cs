#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(PlayerSpawnPoint))]
    public class PlayerSpawnPointEditor: UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }
    }
}
#endif