#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RoveUid))]
    public class RoveUidEditor : UnityEditor.Editor
    {
        SerializedProperty _uidProperty;
        SerializedProperty _codeProperty;

        void OnEnable()
        {
            _uidProperty = serializedObject.FindProperty("_uid");
            _codeProperty = serializedObject.FindProperty("_uniqueCode");

            var roveUid = target as RoveUid;
            if (!roveUid.isActiveAndEnabled)
            {
                roveUid.invokeNewRoveUidEvent();
            }
        }

        public override void OnInspectorGUI()
        {
            var roveUid = target as RoveUid;
            if (!roveUid.isInScene())
            {
                EditorGUILayout.Space();
                EditorGUILayout.HelpBox("Only update on scene", MessageType.Info);
                return;
            }
            serializedObject.Update();
            GUILayout.Label("Rove Uid: " + _uidProperty.intValue);
            GUILayout.Label("Rove Code: " + _codeProperty.stringValue);

            if (roveUid.Uid == 0)
            {
                roveUid.regenerateUid();
                _uidProperty.intValue = roveUid.Uid;
                _codeProperty.stringValue = roveUid.UniqueCode;
            }

            //var go = (target as MonoBehaviour).gameObject;
            //if (go.scene != null && !string.IsNullOrEmpty(go.scene.path))
            //{
            //    GUILayout.Label("Path: " + _pathProperty.stringValue);
            //}
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif