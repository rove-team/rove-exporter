#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.UIElements;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RovePhotoFrame))]
    public class RovePhotoFrameEditor : UnityEditor.Editor
    {
        SerializedProperty defaultModeProperty;
        SerializedProperty urlProperty;
        SerializedProperty textureProperty;
        SerializedProperty photoRendererProperty;
        SerializedProperty loadingOverlayProperty;
        SerializedProperty emptyTextureProperty;
        SerializedProperty sortIndexProperty;

        private void OnEnable()
        {
            defaultModeProperty = serializedObject.FindProperty("_defaultMode");
            urlProperty = serializedObject.FindProperty("_defaultUrl");
            textureProperty = serializedObject.FindProperty("_defaultTexture");
            photoRendererProperty = serializedObject.FindProperty("_photoRenderer");
            loadingOverlayProperty = serializedObject.FindProperty("_loadingOverlay");
            emptyTextureProperty = serializedObject.FindProperty("_emptyTexture");
            sortIndexProperty = serializedObject.FindProperty("_sortIndex");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            //EditorGUILayout.PropertyField(defaultModeProperty);
            if (defaultModeProperty.intValue == (int)RovePhotoFrame.DefaultMode.URL)
            {
                EditorGUILayout.PropertyField(urlProperty);
            }
            else
            {
                EditorGUILayout.PropertyField(textureProperty);
            }
            
            EditorGUILayout.PropertyField(sortIndexProperty);
            //EditorGUILayout.PropertyField(photoRendererProperty);
            //EditorGUILayout.PropertyField(loadingOverlayProperty);
            //EditorGUILayout.PropertyField(emptyTextureProperty);
            serializedObject.ApplyModifiedProperties();
        }
        public static bool isCanExport()
        {
            RovePhotoFrame[] frames = FindObjectsOfType<RovePhotoFrame>(true);
            List<int> useList = new List<int>();

            foreach (RovePhotoFrame frame in frames)
            {
                if (frame.Uid == 0)
                {
                    Debug.LogError($"Uid can't be 0: {frame.gameObject.name}", frame);
                    return false;
                }
                else if (useList.Contains(frame.Uid))
                {
                    Debug.LogError($"Another Rove photo frame use same UID : {frame.gameObject.name}", frame);
                    return false;
                }
                else
                {
                    useList.Add(frame.Uid);
                }
            }
            return true;
        }
    }
}
#endif