#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace Rove.Editor
{
    [UnityEditor.CustomEditor(typeof(RoveAnimatorSetter)), CanEditMultipleObjects]
    public class RoveAnimatorSetterEditor : UnityEditor.Editor
    {
        SerializedProperty _EventTypeProperty;
        SerializedProperty _SetterTypeProperty;
        SerializedProperty _AnimatorProperty;
        SerializedProperty _ParamNameProperty;
        SerializedProperty _ValueProperty;

        private void OnEnable()
        {
            _AnimatorProperty = serializedObject.FindProperty("animator");
            _EventTypeProperty = serializedObject.FindProperty("eventType");
            _SetterTypeProperty = serializedObject.FindProperty("setterType");
            _ParamNameProperty = serializedObject.FindProperty("parameterName");
        }

        public override void OnInspectorGUI()
        {
            RoveAnimatorSetter roveSetter = (RoveAnimatorSetter)target;

            EditorGUILayout.Space();
            serializedObject.Update();

            switch ((RoveAnimatorSetter.RoveSetterType)_SetterTypeProperty.intValue)
            {
                case RoveAnimatorSetter.RoveSetterType.Integer:
                    _ValueProperty = serializedObject.FindProperty("intValue");
                    break;
                case RoveAnimatorSetter.RoveSetterType.Float:
                    _ValueProperty = serializedObject.FindProperty("floatValue");
                    break;
                case RoveAnimatorSetter.RoveSetterType.Boolean:
                    _ValueProperty = serializedObject.FindProperty("boolValue");
                    break;
            }
            //EditorGUILayout.PropertyField(_isLocalOnlyProperty);
            EditorGUILayout.PropertyField(_EventTypeProperty);
            EditorGUILayout.PropertyField(_AnimatorProperty);

            if (_AnimatorProperty.objectReferenceValue != null)
            {
                var animator = _AnimatorProperty.objectReferenceValue as Animator;
                var animatorController = animator.runtimeAnimatorController as AnimatorController;
                var parameters = new AnimatorControllerParameter[0];
                List<string> options = new List<string>();

                if (animatorController != null)
                {
                    parameters = animatorController.parameters;
                    if (parameters.Length == 0)
                    {
                        parameters = animator.parameters;
                    }
                    foreach (var p in parameters)
                    {
                        if (p.type != AnimatorControllerParameterType.Trigger)
                        {
                            options.Add(p.name);
                        }
                    }
                }
                else
                {
                    EditorGUILayout.HelpBox("Missing runtime animation controller on Animator", MessageType.Error);
                }

                if (options.Count > 0)
                {
                    int selected = options.IndexOf(_ParamNameProperty.stringValue);
                    if (selected == -1)
                    {
                        if (!Application.isPlaying)
                        {
                            _ParamNameProperty.stringValue = "";
                        }
                    }
                    var afterSelected = EditorGUILayout.Popup("ParamName", selected, options.ToArray());

                    if (!Application.isPlaying)
                    {
                        if (selected != afterSelected)
                        {
                            _ParamNameProperty.stringValue = options[afterSelected];
                            foreach (var p in parameters)
                            {
                                if (p.name == _ParamNameProperty.stringValue)
                                {
                                    switch (p.type)
                                    {
                                        case AnimatorControllerParameterType.Bool:
                                            _SetterTypeProperty.intValue = (int)RoveAnimatorSetter.RoveSetterType.Boolean;
                                            break;
                                        case AnimatorControllerParameterType.Int:
                                            _SetterTypeProperty.intValue = (int)RoveAnimatorSetter.RoveSetterType.Integer;
                                            break;
                                        case AnimatorControllerParameterType.Float:
                                            _SetterTypeProperty.intValue = (int)RoveAnimatorSetter.RoveSetterType.Float;
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                EditorGUILayout.HelpBox("Missing Animator reference", MessageType.Error);
            }
            if (roveSetter.GetComponent<ILocalOnlyChecker>() == null)
            {
                EditorGUILayout.HelpBox("Missing RoveTrigger or RoveInteractable reference", MessageType.Error);
            }
            //EditorGUILayout.PropertyField(_SetterTypeProperty);
            //EditorGUILayout.PropertyField(_ParamNameProperty);
            if (!string.IsNullOrEmpty(_ParamNameProperty.stringValue))
            {
                EditorGUILayout.PropertyField(_ValueProperty);
            }
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif