#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RoveTrigger)), CanEditMultipleObjects]
    public class RoveTriggerEditor : UnityEditor.Editor
    {        
        SerializedProperty _IsLocalOnlyProperty;
        SerializedProperty _TriggerTypeProperty;
        SerializedProperty _NumberToTriggerProperty;
        SerializedProperty _LimitTriggerCountProperty;
        SerializedProperty _MinTimeToTriggerProperty;
        SerializedProperty _BoxColliderProperty;
        SerializedProperty _SphereColliderProperty;

        SerializedProperty _TriggerTargetProperty;
        SerializedProperty _TriggererTypeProperty;
        SerializedProperty _TriggerMaskProperty;
        SerializedProperty _TriggerersProperty;

        SerializedProperty _TriggerEventProperty;
        SerializedProperty _LogFlagProperty;
        SerializedProperty _GizmosFlagProperty;

        private void OnEnable()
        {
            _IsLocalOnlyProperty = serializedObject.FindProperty("_isLocalOnly");
            _NumberToTriggerProperty = serializedObject.FindProperty("_NumberToTrigger");
            _LimitTriggerCountProperty = serializedObject.FindProperty("_LimitTriggerCount");
            _MinTimeToTriggerProperty = serializedObject.FindProperty("_minTimeToTrigger");
            _TriggerTypeProperty = serializedObject.FindProperty("_TriggerType");
            _TriggererTypeProperty = serializedObject.FindProperty("_TriggererType");
            _BoxColliderProperty = serializedObject.FindProperty("_BoxCollider");
            _SphereColliderProperty = serializedObject.FindProperty("_SphereCollider");

            _TriggerTargetProperty = serializedObject.FindProperty("_TriggerTarget");
            _TriggerMaskProperty = serializedObject.FindProperty("_triggerMask");
            _TriggerersProperty = serializedObject.FindProperty("_Triggerers");

            _TriggerEventProperty = serializedObject.FindProperty("_TriggerEvents");
            _LogFlagProperty = serializedObject.FindProperty("_IsEnableLog");
            _GizmosFlagProperty = serializedObject.FindProperty("_IsEnableGizmos");
        }

        public override void OnInspectorGUI()
        {
            RoveTrigger roveTrigger = (RoveTrigger)target;

            EditorGUILayout.Space();
            serializedObject.Update();

            EditorGUILayout.PropertyField(_IsLocalOnlyProperty);
            EditorGUILayout.PropertyField(_NumberToTriggerProperty);
            EditorGUILayout.PropertyField(_LimitTriggerCountProperty);
            EditorGUILayout.PropertyField(_MinTimeToTriggerProperty);

            EditorGUILayout.PropertyField(_TriggerTypeProperty);
            guiCollider(roveTrigger);

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_TriggerTargetProperty);
            switch ((RoveTriggerTarget)_TriggerTargetProperty.intValue)
            {
                case RoveTriggerTarget.LAYER_MASK:
                    EditorGUILayout.PropertyField(_TriggerMaskProperty);
                    break;
                case RoveTriggerTarget.GAMEOBJECTS:
                    EditorGUILayout.PropertyField(_TriggerersProperty);
                    break;
                case RoveTriggerTarget.ROVE_TRIGGERER:
                    EditorGUILayout.PropertyField(_TriggererTypeProperty);
                    EditorGUILayout.PropertyField(_TriggerMaskProperty);
                    break;
            }

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_TriggerEventProperty);
            //EditorGUILayout.PropertyField(_OnExitEventProperty);
            //EditorGUILayout.PropertyField(_OnStayEventProperty);

            foreach (var invoker in roveTrigger.GetComponents<IRoveTriggerEventInvoker>())
            {
                if (invoker.EventType == RoveTriggerEventType.OnInteracted)
                {
                    invoker.EventType = RoveTriggerEventType.OnEnter;
                };
            }

            if (GUILayout.Button("Add animator setter"))
            {
                roveTrigger.gameObject.AddComponent<RoveAnimatorSetter>();
            }

            EditorGUILayout.PropertyField(_LogFlagProperty);
            EditorGUILayout.PropertyField(_GizmosFlagProperty);
            serializedObject.ApplyModifiedProperties();
        }

        void guiCollider(RoveTrigger roveTrigger)
        {
            if (_TriggerTypeProperty.intValue == (int)RoveTriggerType.OVERLAP_BOX)
            {
                EditorGUILayout.PropertyField(_BoxColliderProperty);
                if (_BoxColliderProperty.objectReferenceValue == null)
                {
                    EditorGUILayout.Space();
                    if (GUILayout.Button("Create collider"))
                    {
                        var collier = roveTrigger.GetComponent<BoxCollider>();
                        if (collier == null)
                        {
                            collier = roveTrigger.gameObject.AddComponent<BoxCollider>();
                        }
                        _BoxColliderProperty.objectReferenceValue = collier;
                        SceneView.lastActiveSceneView.FrameSelected();
                        UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                    }
                }
            }
            else if (_TriggerTypeProperty.intValue == (int)RoveTriggerType.OVERLAP_SPHERE)
            {
                EditorGUILayout.PropertyField(_SphereColliderProperty);

                if (_SphereColliderProperty.objectReferenceValue == null)
                {
                    EditorGUILayout.Space();
                    if (GUILayout.Button("Create collider"))
                    {
                        var collier = roveTrigger.GetComponent<SphereCollider>();
                        if (collier == null)
                        {
                            collier = roveTrigger.gameObject.AddComponent<SphereCollider>();
                        }
                        _SphereColliderProperty.objectReferenceValue = collier;
                        SceneView.lastActiveSceneView.FrameSelected();
                        UnityEditor.SceneManagement.EditorSceneManager.MarkAllScenesDirty();
                    }
                }
            }
        }
    }
}
#endif