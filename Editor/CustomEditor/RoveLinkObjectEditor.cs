#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    [CustomEditor(typeof(RoveLinkObject))]
    public class RoveLinkObjectEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            var roveLinkObject = (RoveLinkObject)target;
            if (!roveLinkObject.isInScene())
            {
                EditorGUILayout.Space();
                EditorGUILayout.HelpBox("Only update on scene", MessageType.Info);
                return;
            }
            base.OnInspectorGUI();

            if (!roveLinkObject.CompleteObject)
            {
                EditorGUILayout.HelpBox("CompleteObject is null", MessageType.Error);
            }

            bool isCanCreateData = true;

            if (!roveLinkObject.OriginalObject)
            {
                EditorGUILayout.HelpBox("OriginalObject is null", MessageType.Error);
                isCanCreateData = false;
            }
            else if (roveLinkObject.OriginalObject.isInScene())
            {
                EditorGUILayout.HelpBox("OriginalObject must be prefab", MessageType.Error);
                isCanCreateData = false;
            }
            
            if (roveLinkObject.LinkedData == null || !roveLinkObject.LinkedPattern)
            {
                EditorGUILayout.HelpBox("LinkData is null", MessageType.Warning);
            }

            if (isCanCreateData)
            {
                if (GUILayout.Button("Create data"))
                {
                    roveLinkObject.createLinkData(false);
                }
                if (GUILayout.Button("Create data and save as prefab"))
                {
                    roveLinkObject.createLinkData(true);
                }
            }

            if (roveLinkObject.PrefabInProject)
            {
                if (GUILayout.Button("Locate in Asset"))
                {
                    EditorGUIUtility.PingObject(roveLinkObject.PrefabInProject);
                }
            }

            if (roveLinkObject.EditorImportedObject && roveLinkObject.LinkedData != null)
            {
                EditorGUILayout.Space();
                EditorGUILayout.Space();
                if (GUILayout.Button("Apply data"))
                {
                    roveLinkObject.applyLinkedObjectEditor();
                }
            }
        }
    }
}
#endif