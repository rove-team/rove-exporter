#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;

namespace Rove.Editor
{
    internal class RoveSettingEditor
    {
#if ROVE_DESIGNER
        [MenuItem("Edit/Rove/Deactive Rove designer")]
        private static void setRoveDev()
        {
            string symbolName = "ROVE_DESIGNER";
            var tempTargets = new BuildTargetGroup[]
            {
                BuildTargetGroup.Standalone,
                BuildTargetGroup.WebGL,
                BuildTargetGroup.Android,
                BuildTargetGroup.iOS
            };
            foreach (var target in tempTargets)
            {
                var str = PlayerSettings.GetScriptingDefineSymbolsForGroup(target);
                var symbols = new List<string>(str.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                if (!symbols.Contains(symbolName))
                {
                    continue;
                }
                symbols.Remove(symbolName);
                str = String.Join(";", symbols);

                PlayerSettings.SetScriptingDefineSymbolsForGroup(target, str);
            }
        }
#else
        [MenuItem("Edit/Rove/Active Rove designer")]
        private static void setRoveDev()
        {
            string symbolName = "ROVE_DESIGNER";
            var tempTargets = new BuildTargetGroup[]
            {
                BuildTargetGroup.Standalone,
                BuildTargetGroup.WebGL,
                BuildTargetGroup.Android,
                BuildTargetGroup.iOS
            };
            foreach (var target in tempTargets)
            {
                var str = PlayerSettings.GetScriptingDefineSymbolsForGroup(target);
                var symbols = new List<string>(str.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries));
                if (symbols.Contains(symbolName))
                {
                    continue;
                }
                symbols.Add(symbolName);
                str = String.Join(";", symbols);

                PlayerSettings.SetScriptingDefineSymbolsForGroup(target, str);
            }
        }
#endif
    }

}
#endif