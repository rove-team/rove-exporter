#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace Rove.Editor
{
    internal class RoveTextureUtils
    {
        private static int findTextureInAsset(Action<TextureImporter> callback)
        {
            var totalTextures = AssetDatabase.FindAssets("t:texture", new string[] { "Assets" });
            foreach (string guid in totalTextures)
            {
                var path = AssetDatabase.GUIDToAssetPath(guid);
                if (path.Contains("/Editor/"))
                {
                    continue;
                }
                TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                if (textureImporter)
                {
                    callback?.Invoke(textureImporter);
                }
            }
            return totalTextures.Length;
        }

        [MenuItem("Rove Tools/Textures/Find non-crunched textures")]
        internal static int findNonCrunchTextures()
        {
            int count = 0;
            findTextureInAsset((textureImporter) =>
            {
                if (!textureImporter.crunchedCompression)
                {
                    var texture = AssetDatabase.LoadAssetAtPath<Texture>(textureImporter.assetPath);
                    Debug.Log(textureImporter.assetPath, texture);
                    count++;
                }
            });
            Debug.Log($"[Rove tools] Found {count} non-crunched texture(s)");
            return count;
        }
        [MenuItem("Rove Tools/Textures/Find non-NPOTScale textures")]
        internal static int findNonNPOTScaleTextures()
        {
            int count = 0;
            findTextureInAsset((textureImporter) =>
            {
                if (textureImporter.npotScale == TextureImporterNPOTScale.None)
                {
                    var texture = AssetDatabase.LoadAssetAtPath<Texture>(textureImporter.assetPath);
                    // check NPOT
                    if ((texture.width & (texture.width - 1)) != 0 || (texture.width & (texture.width - 1)) != 0)
                    {
                        Debug.Log(textureImporter.assetPath, texture);
                        count++;
                    }
                }
            });
            Debug.Log($"[Rove tools] Found {count} Non-NPOTScale texture(s)");
            return count;
        }

        [MenuItem("Rove Tools/Textures/Crunch all textures")]
        private static void crunchAllTextures()
        {
            int quality = 50, count = 0;
            var textureCount = findTextureInAsset((textureImporter) =>
            {
                if (!textureImporter.crunchedCompression || textureImporter.compressionQuality != quality)
                {
                    textureImporter.crunchedCompression = true;
                    textureImporter.compressionQuality = quality;
                    textureImporter.SaveAndReimport();
                    var texture = AssetDatabase.LoadAssetAtPath<Texture>(textureImporter.assetPath);
                    Debug.Log("Crunched " + textureImporter.assetPath, texture);
                    count++;
                }
            });
            Debug.Log($"[Rove tools] Crunched {count}/{textureCount} texture(s)");
        }
        #region Change texture format

        #region iOS
        [MenuItem("Rove Tools/Textures/Change texture format/iOS/ETC-ETC2 (small size)")]
        private static void changeTextureFormat_iOS_ETC()
        {
            TextureImporterFormat haveAlphaFormat = TextureImporterFormat.ETC2_RGBA8Crunched;
            TextureImporterFormat noAlphaFormat = TextureImporterFormat.ETC_RGB4Crunched;
            BuildTargetGroup platform = BuildTargetGroup.iOS;
            int count = changeAllTexturesFormat(platform, haveAlphaFormat, noAlphaFormat);
            Debug.Log($"[Rove tools] Changed {count} texture(s)");
        }
        [MenuItem("Rove Tools/Textures/Change texture format/iOS/PVRTC")]
        private static void changeTextureFormat_iOS_PVRTC()
        {
            TextureImporterFormat haveAlphaFormat = TextureImporterFormat.PVRTC_RGBA4;
            TextureImporterFormat noAlphaFormat = TextureImporterFormat.PVRTC_RGB4;
            BuildTargetGroup platform = BuildTargetGroup.iOS;
            int count = changeAllTexturesFormat(platform, haveAlphaFormat, noAlphaFormat);
            Debug.Log($"[Rove tools] Changed {count} texture(s)");
        }
        [MenuItem("Rove Tools/Textures/Change texture format/iOS/ASTC12")]
        private static void changeTextureFormat_iOS_ASTC12()
        {
            TextureImporterFormat haveAlphaFormat = TextureImporterFormat.ASTC_12x12;
            TextureImporterFormat noAlphaFormat = TextureImporterFormat.ASTC_12x12;
            BuildTargetGroup platform = BuildTargetGroup.iOS;
            int count = changeAllTexturesFormat(platform, haveAlphaFormat, noAlphaFormat);
            Debug.Log($"[Rove tools] Changed {count} texture(s)");
        }
        #endregion

        #region Win/Mac
        [MenuItem("Rove Tools/Textures/Change texture format/Win-Mac/DXT (small size)")]
        private static void changeTextureFormat_WinMac_DXT()
        {
            TextureImporterFormat haveAlphaFormat = TextureImporterFormat.DXT5Crunched;
            TextureImporterFormat noAlphaFormat = TextureImporterFormat.DXT1Crunched;
            BuildTargetGroup platform = BuildTargetGroup.Standalone;
            int count = changeAllTexturesFormat(platform, haveAlphaFormat, noAlphaFormat);
            Debug.Log($"[Rove tools] Changed {count} texture(s)");
        }
        [MenuItem("Rove Tools/Textures/Change texture format/Win-Mac/BC7")]
        private static void changeTextureFormat_WinMac_BC7()
        {
            TextureImporterFormat haveAlphaFormat = TextureImporterFormat.BC7;
            TextureImporterFormat noAlphaFormat = TextureImporterFormat.BC7;
            BuildTargetGroup platform = BuildTargetGroup.Standalone;
            int count = changeAllTexturesFormat(platform, haveAlphaFormat, noAlphaFormat);
            Debug.Log($"[Rove tools] Changed {count} texture(s)");
        }
        #endregion

        private static int changeAllTexturesFormat(BuildTargetGroup platform, TextureImporterFormat haveAlphaFormat, TextureImporterFormat noAlphaFormat)
        {
            int count = 0;
            var textureCount = findTextureInAsset((textureImporter) =>
            {
                TextureImporterPlatformSettings settings = textureImporter.GetPlatformTextureSettings("iOS");
                if (changeTextureFormat(textureImporter, platform, haveAlphaFormat, noAlphaFormat))
                {
                    count++;
                }
            });
            return count;
        }
        private static bool changeTextureFormat(TextureImporter textureImporter, BuildTargetGroup target, TextureImporterFormat haveAlphaFormat, TextureImporterFormat noAlphaFormat)
        {
            TextureImporterPlatformSettings settings = textureImporter.GetPlatformTextureSettings(target.ToString());
            bool isChange = false;
            if (textureImporter.DoesSourceTextureHaveAlpha() || textureImporter.textureType == TextureImporterType.NormalMap)
            {
                if (settings.format != haveAlphaFormat)
                {
                    settings.format = haveAlphaFormat;
                    settings.overridden = true;
                    isChange = true;
                }
            }
            else
            {
                if (settings.format != noAlphaFormat)
                {
                    settings.format = noAlphaFormat;
                    settings.overridden = true;
                    isChange = true;
                }
            }
            if (isChange)
            {
                textureImporter.SetPlatformTextureSettings(settings);
                textureImporter.SaveAndReimport();
            }
            return isChange;
        }
        #endregion
    }
}
#endif