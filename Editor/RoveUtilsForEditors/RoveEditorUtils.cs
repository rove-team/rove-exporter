#if UNITY_EDITOR
using Rove.RoveExporter;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Rove.Editor
{
    internal class RoveEditorUtils
    {
#if ROVE_DESIGNER
        [MenuItem("Rove Tools/Convert/Convert all frames")]
        private static void convertAllFrames()
        {
            var allCanvases = GameObject.FindObjectsOfType<Canvas>(true);
            var oldFrames = new List<Canvas>();
            foreach (var canvas in allCanvases)
            {
                var frameObject = canvas.transform.Find("PhotoFrameObject");
                if (frameObject != null)
                {
                    var imageObject = frameObject.Find("Image");
                    if (imageObject != null)
                    {
                        oldFrames.Add(canvas);
                    }
                }
            }

            var framePrefab = Resources.Load<RovePhotoFrame>("RovePhotoFrame");

            foreach (var oldFrame in oldFrames)
            {
                var frame = GameObject.Instantiate(framePrefab);
                frame.name = "RovePhotoFrame_" + frame.Uid;
                frame.transform.position = oldFrame.transform.position;
                frame.transform.rotation = oldFrame.transform.rotation;
                var w = oldFrame.GetComponent<RectTransform>().sizeDelta.x * oldFrame.transform.lossyScale.x;
                var h = oldFrame.GetComponent<RectTransform>().sizeDelta.y * oldFrame.transform.lossyScale.y;
                frame.transform.localScale = new Vector3(w, h, 1);
                var img = oldFrame.transform.Find("PhotoFrameObject").Find("Image");
                if (img)
                {
                    var img2 = img.GetComponent<Image>();
                    if (img2.sprite != null)
                    {
                        frame.setDefaultTexture(img2.sprite.texture);
                    }
                }
            }
        }
#endif
        [MenuItem("Rove Tools/Fix Tools/Fix RoveUid")]
        public static void fixRoveUidToRoveMonoBehaviour()
        {
            var monos = GameObject.FindObjectsOfType<RoveMonoBehaviour>(true);
            foreach (var r in monos)
            {
                if (r.GetComponent<RoveUid>() == null)
                {
                    r.gameObject.AddComponent<RoveUid>();
                }
            }

            var rUids = GameObject.FindObjectsOfType<RoveUid>(true);
            foreach (var r in rUids)
            {
                r.clearUid();
            }
            foreach (var r in rUids)
            {
                r.regenerateUid();
            }
        }
        public static bool haveErrorRoveUid()
        {
            var monos = GameObject.FindObjectsOfType<RoveMonoBehaviour>(true);
            foreach (var r in monos)
            {
                if (r.GetComponent<RoveUid>() == null)
                {
                    return true;
                }
            }

            var rUids = GameObject.FindObjectsOfType<RoveUid>(true);
            foreach (var r in rUids)
            {
                if (r.Uid == 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
    //internal partial class RoveStatistic
    //{
    //    [MenuItem("Rove Tools/Statistics/Estimate triangles and vertices (mesh only)")]
    //    private static void estimateTrisAndVertsMeshOnly()
    //    {
    //        var data = estimateTrisVerMesh();
    //        var warningColor = "lime";
    //        if (data.vertsMax > 500000)
    //        {
    //            warningColor = "red";
    //        }
    //        else if (data.vertsMax > 200000)
    //        {
    //            warningColor = "#FF7F50"; // coral color
    //        }
    //        var text = $"<color={warningColor}>[Rove statistics] Tris: {data.trisMin}-{data.trisMax}, Verts: {data.vertsMin}-{data.vertsMax}</color>";
    //        Debug.Log(text);
    //    }
    //}
}
#endif