#if UNITY_EDITOR
using UnityEditor.SceneManagement;

namespace Rove.Editor
{
    internal class RoveSceneEditor
    {
        public static void saveActiveScene()
        {
            if (!string.IsNullOrEmpty(EditorSceneManager.GetActiveScene().name))
            {
                EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
                EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());
            }
        }
    }
}
#endif