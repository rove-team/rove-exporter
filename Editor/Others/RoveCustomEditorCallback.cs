#if UNITY_EDITOR
using Rove.RoveExporter;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Rove.Editor
{
#if !ROVE_DEVELOPER
    public class RoveCustomEditorCallback
    {
        [UnityEditor.Callbacks.DidReloadScripts]
        private static void onReloadScripts()
        {
            EditorSceneManager.sceneSaving -= onEditorSceneManager_sceneSaving;
            EditorSceneManager.sceneSaving += onEditorSceneManager_sceneSaving;

            EditorSceneManager.sceneOpened += onEditorSceneManager_sceneOpened;

            onEditorSceneManager_sceneSaving(default, default);
        }

        private static void onEditorSceneManager_sceneOpened(Scene scene, OpenSceneMode mode)
        {
            createRoveObjectManager();
        }

        private static void onEditorSceneManager_sceneSaving(Scene scene, string path)
        {
            createRoveObjectManager();
        }

        public static void createRoveObjectManager()
        {
            var roveManager = GameObject.FindObjectOfType<RoveObjectManager>();
            var name = "[Don't delete] RoveObjectManager";
            if (!roveManager)
            {
                var go = new GameObject(name);
                roveManager = go.AddComponent<RoveObjectManager>();
                if (!Application.isPlaying)
                {
                    RoveSceneEditor.saveActiveScene();
                }
            }
            else
            {

                if (roveManager.name != name)
                {
                    roveManager.name = name;
                    RoveSceneEditor.saveActiveScene();
                }
            }
            roveManager.addEvents();
            roveManager.findReferences();
        }
    }
#endif
}
#endif