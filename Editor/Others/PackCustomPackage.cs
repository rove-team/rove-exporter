#if UNITY_EDITOR
using Rove.RoveExporter;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEngine;

namespace Rove.Editor
{
#if ROVE_DESIGNER
    internal class PackCustomPackage
    {
        [MenuItem("Rove Tools/Others/Pack Custom package")]
        public static void packCustomPackage()
        {
            var path = EditorUtility.OpenFolderPanel("Select folder to package", "", "");
            if (!string.IsNullOrEmpty(path))
            {
                var jsonPath = Path.Combine(path, "package.json");
                var ignorePath = Path.Combine(path, ".gitignore");
                var outputPath = Path.Combine(path, ".output");
                if (File.Exists(jsonPath))
                {
                    if (File.Exists(ignorePath))
                    {
                        var reader = File.OpenText(ignorePath);
                        var txt = reader.ReadToEnd();
                        reader.Close();
                        reader.Dispose();

                        if (!txt.Contains(@"/.output"))
                            txt += @"/.output";
                        Task.Delay(100);
                        File.WriteAllText(ignorePath, txt);
                    }
                    else
                    {
                        var f = File.CreateText(ignorePath);
                        f.WriteLine(@"/.output");
                        f.Close();
                        f.Dispose();
                    }
                }
                EditorUtility.DisplayProgressBar("Pack custom package", "Packing...", 0);
                var request = Client.Pack(path, outputPath);
                while (!request.IsCompleted) Task.Delay(100);
                EditorUtility.ClearProgressBar();
                if (request.Error != null)
                {
                    Debug.Log($"[Rove toos] <b><color=red>Pack error</color></b>: {request.Error.message}");
                }
                else
                {
                    Debug.Log($"[Rove toos] <b><color=blue>Pack complete</color></b>: <color=lime>{outputPath}</color>");
                    BuildAddrressableResultWindow.show(outputPath);
                }
            }
        }
    }
#endif
}
#endif