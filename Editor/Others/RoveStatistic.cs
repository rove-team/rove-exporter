using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.Editor
{
    internal partial class RoveStatistic
    {
        public static RoveTrisVerData estimateTrisVerMesh()
        {
            var obj = GameObject.FindObjectsOfType<Renderer>(true);

            Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);
            foreach (var r in obj)
            {
                bounds.Encapsulate(r.bounds);
            }

            var d0 = estimateTrisVerMesh(bounds.center, obj);
            return d0;
            //var d1 = estimateTrisVerMesh(bounds.min, obj);
            //var d2 = estimateTrisVerMesh(bounds.max, obj);

            //return new RoveTrisVerData()
            //{
            //    trisMin = Mathf.Min(d0.trisMin, d1.trisMin, d2.trisMin),
            //    trisMax = Mathf.Max(d0.trisMax, d1.trisMax, d2.trisMax),
            //    vertsMin = Mathf.Min(d0.vertsMin, d1.vertsMin, d2.vertsMin),
            //    vertsMax = Mathf.Max(d0.vertsMax, d1.vertsMax, d2.vertsMax),
            //};
        }
        public static RoveTrisVerData estimateTrisVerMesh(Vector3 pos, Renderer[] obj = null)
        {
            if (obj == null)
            {
                obj = GameObject.FindObjectsOfType<Renderer>(true);
            }

            int trisMax = 0;
            int trisMin = int.MaxValue;
            int vertsMax = 0;
            int vertsMin = int.MaxValue;

            Camera cam = new GameObject("AutoCreate-Camera").AddComponent<Camera>();
            cam.transform.position = pos;
            cam.transform.rotation = Quaternion.identity;

            int Yrotated = 0;
            int Zrotated = 0;
            int angleStep = 5;

            while (Yrotated < 360)
            {
                var tRotation = cam.transform.rotation;
                while (Zrotated < 360)
                {
                    int tris = 0;
                    int verts = 0;

                    foreach (var r in obj)
                    {
                        if (isInViewport(cam, r))
                        {
                            var (t, v) = checkTrisVer(r);
                            tris += t;
                            verts += v;
                        }
                    }

                    tris /= 3;
                    trisMax = Mathf.Max(trisMax, tris);
                    trisMin = Mathf.Min(trisMin, tris);
                    vertsMax = Mathf.Max(vertsMax, verts);
                    vertsMin = Mathf.Min(vertsMin, verts);
                    cam.transform.Rotate(Vector3.forward, angleStep);
                    Zrotated += angleStep;
                }
                cam.transform.rotation = tRotation;
                cam.transform.Rotate(Vector3.up, angleStep);
                Yrotated += angleStep;
                Zrotated = 0;
            }

            GameObject.DestroyImmediate(cam.gameObject);

            return new RoveTrisVerData()
            {
                trisMax = trisMax,
                trisMin = trisMin,
                vertsMax = vertsMax,
                vertsMin = vertsMin,
            };
        }

        public static (int, int) checkTrisVer(Renderer r)
        {
            var skinMesh = r as SkinnedMeshRenderer;
            if (skinMesh != null)
            {
                return checkTrisVer(skinMesh);
            }
            else
            {
                Component[] filters;
                filters = r.GetComponents<MeshFilter>();
                int tris = 0;
                int verts = 0;
                foreach (MeshFilter f in filters)
                {
                    tris += f.sharedMesh.triangles.Length;
                    verts += f.sharedMesh.vertexCount;
                }
                return (tris, verts);
            }
        }
        public static (int, int) checkTrisVer(SkinnedMeshRenderer r)
        {
            var m = r.sharedMesh;
            return (m.triangles.Length, m.vertexCount);
        }

        public static bool isInViewport(Camera cam, Renderer renderer)
        {
            var upper = 1.02f;
            var lower = -0.02f;
            var bounds = renderer.bounds;
            var viewport = cam.WorldToViewportPoint(bounds.max);
            if (viewport.x < upper && viewport.x > lower && viewport.y > lower && viewport.y < upper && viewport.z > 0)
            {
                return true;
            }
            viewport = cam.WorldToViewportPoint(bounds.min);
            if (viewport.x < upper && viewport.x > lower && viewport.y > lower && viewport.y < upper && viewport.z > 0)
            {
                return true;
            }
            return false;
        }
    }
    public struct RoveTrisVerData
    {
        public int trisMax;
        public int trisMin;
        public int vertsMax;
        public int vertsMin;
    }
}