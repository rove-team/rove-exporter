using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rove.RoveExporter
{
    public class RovePlayerTriggerer : MonoBehaviour, IPlayerTriggerer
    {
        [SerializeField] private bool _isLocalPlayer;
        public bool IsLocalPlayer => _isLocalPlayer;
    }
}
